Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :administrators
  devise_for :auth_users, controllers: { registrations: 'auth_users/registrations' }

  patch "/update_password" => "templates#update_password", as: "update_password"
  get "/auth_users/edit_password_signed" => "templates#edit_password", as: "edit_password_signed"

  get "/admin/auth_users/:id/edit_password" => "templates#edit_password_admin", as: "edit_password_admin"
  patch "/update_password_admin/:id" => "templates#update_password_admin", as: "update_password_admin"
  get "/admin/latamusers_usergroups" => "admin/latamusers_usergroups#index", as:"index_admin_usergroup"
  get "/admin/latamusers_usergroups/:id/edit" => "admin/latamusers_usergroups#edit", as:"edit_admin_usergroup", :constraints => { :id => /[^\/]+/ } 
  get "/admin/usergroups/:id" => "admin/latamusers_usergroups#show", as:"show_admin_usergroup", :constraints => { :id => /[^\/]+/ } 
  patch "/admin/latamusers_usergroups/:id" => "admin/latamusers_usergroups#update", as: "update_admin_usergroup", :constraints => { :id => /[^\/]+/ } 
  delete "/admin/usergroups/:id" => "admin/latamusers_usergroups#destroy", as: "delete_admin_usergroup", :constraints => { :id => /[^\/]+/ } 
  get "/vacio" => "templates#vacio", as: "edit_admin_administrator"

  match '/admin/administrators' => redirect('/admin/auth_users'), via: [:get]
  match '/admin/signin' =>redirect('administrators/sign_in'), via: [:get]

  # URLs para los tutoriales
  match '/static/videostv/TutorialComprimido.mp4' => redirect('https://s3-us-west-2.amazonaws.com/elpost/videostv/TutorialComprimido.mp4'), via: [:get]
  match '/static/videostv/ManulaDeUsuarioIntruduccion.mp4' => redirect('https://s3-us-west-2.amazonaws.com/elpost/videostv/ManulaDeUsuarioIntruduccion.mp4'), via: [:get]
  match '/static/videostv/manualdelprofe2.mp4' => redirect('https://s3-us-west-2.amazonaws.com/elpost/videostv/manualdelprofe2.mp4'), via: [:get]

  # Temporal para efectos de presentación
  get '/admin_statistics' => "templates#admin_statistics", as: "admin_statistics"

  Tolaria.draw_routes(self)

  get "/activities" => "templates#all", as: "activities"
  get "/activities/media" => "templates#media", as: "activities_media"
  get "/activities/reading" => "templates#reading", as: "activities_reading"
  get "/activities/vocabulary" => "templates#vocabulary", as: "activities_vocabulary"

  get "/caracterization_view/" => "templates#caracterization", as: "caracterization_profile"
  post "/caracterization_view/" => "templates#create_caracterization", as: "caracterization_profile_create"

  get "/profile" => "templates#profile", as: "profile"
  get "/articles" => "templates#articles", as: "articles"
  get "/activity/:id" => "templates#activity", as: "activity"
  get "/preview/:id" => "templates#preview", as: "preview"
  get "/generate_report" => "templates#reports", as: "reports"
  get "/phrases" => "templates#phrases", as: "phrases"
  get "/parts" => "templates#parts", as: "parts"
  get "/guided_content" => "templates#guided_content", as: "guided_content"
  get "/my_subscription" => "templates#my_subscription", as: "my_subscription"
  get "/create-group" => "templates#create_group", as: "create_group"
  get "/create-activity" => "templates#create_activity", as: "create_activity"
  get "/create-sequence" => "templates#create_sequence", as: "create_sequence"

  get "/edit-activity/:id" => "templates#edit_activity", as: "edit_activity"
  get "/edit-sequence/:id" => "templates#edit_sequence", as: "edit_sequence"

  get "/students_in_group/:code" => "templates#students_in_group", as: "students_in_group", :constraints => { :code => /[^\/]+/ }
  get "/student_activities/:id" => "templates#student_activities", as: "student_activities"
  get "/students_assignation" => "templates#students_assignation", as: "students_assignation"

  get "/students_with_activity/:id" => "templates#students_with_activity", as: "students_with_activity"
  get "/students_with_sequence/:id" => "templates#students_with_sequence", as: "students_with_sequence"

  get "/all_assigned_activities" => "templates#all_assigned_activities", as: "all_assigned_activities"
  get "/elearning" => "templates#elearning", as: "elearning"
  get "/activities_active" => "templates#activities_active", as: "activities_active"
  get "/sequences_active" => "templates#sequences_active", as: "sequences_active"

  get "/current_groups" => "templates#current_groups", as: "current_groups"
  get "/current_groups_by_id/:id" => "templates#current_groups_by_id", as: "current_groups_by_id"
  
  get "/current_students" => "templates#current_students", as: "current_students"

  get "/current_students_in_activity/:id" => "templates#current_students_in_activity", as: "current_students_in_activity"
  get "/current_students_in_sequence/:id" => "templates#current_students_in_sequence", as: "current_students_in_sequence"
  get "/preview_sequence/:id" => "templates#preview_sequence", as: "preview_sequence"

  get "/current_students_in_group/:code" => "templates#current_students_in_group", as: "current_students_in_group", :constraints => { :code => /[^\/]+/ }
  get "/current_activities_in_student/:id" => "templates#current_activities_in_student", as: "current_activities_in_student"
  get "/assigned_activities" => "templates#assigned_activities", as: "assigned_activities"
  get "/admin/auth_users/import/csv" => "templates#import", as: "import_users"
  get "/admin/media_media/utility/generate_audio" => "templates#generate_audio_layout", as: "generate_audio_layout"
  get "/about/:page" => "templates#pages", as: "pages"

  get "/open-domain-questions" => "templates#open_domain_questions", as: "open_domain_questions"
  get "/open-questions/:id" => "templates#open_questions_show", as: "open_question_show"
  get "/open-questions-api" => "templates#open_questions_api", as: "open_questions_api"

  get "/login_users_path" => "templates#view_as_user", as: "login_users_path"

  post "/get_group_students/:code" => "templates#get_students_html", as: "get_students_html", :constraints => { :code => /[^\/]+/ }
  post "/get_activities/:id_level/:id_section/:start_date/:end_date/:active/:tries" => "templates#get_activities_html", as: "get_activities_html"
  post "/create_assign_activity" => "templates#create_assign_activity", as: "create_assign_activity"
  post "/create_assign_sequence" => "templates#create_assign_sequence", as: "create_assign_sequence"
  post "/assign_activity" => "templates#assign_activity", as: "assign_activity"
  post "/assign_sequence" => "templates#assign_sequence", as: "assign_sequence"
  post "/create_student_group" => "templates#create_student_group", as: "create_student_group"

  post "/edit_activity_post" => "templates#edit_activity_post", as: "edit_activity_post"
  post "/edit_sequence_post" => "templates#edit_sequence_post", as: "edit_sequence_post"

  post "/send_contact_mail" => "templates#send_contact_mail", as: "send_contact_mail"
  post "/lesscount/:id" => "templates#lesscount", as: "lesscount"
  post "/import/auth_users" => "templates#import_users", as: "import_users_db"
  post "/generate_audio/media_media" => "templates#generate_audio", as: "generate_audio"
  post "/save_audio" => "templates#save_audio", as: "save_audio"
  post "/correct_answer" => "templates#correct_answer", as: "correct_answer"
  post "/get_words" => "templates#update_lms_words", as:"lms_words_refresh"
  post "/get_articles" => "templates#update_articles", as:"lms_articles_refresh"
  post "/get_media/:media" => "templates#update_media", as:"media_refresh"
  post "/api/userquestions/" => "templates#questions", as: "questions"
  post "/api/openquestions/" => "templates#openquestions", as: "openquestionsadd"
  post "/postActivity" => "templates#postActivity", as: "postActivity"

  post "/create_review" => "templates#open_question_create", as: "review_open_questions"

  delete "/remove_activity/:id" => "templates#remove_activity", as:"remove_activity"
  delete "/remove_secuence/:id" => "templates#remove_secuence", as:"remove_secuence"

  devise_scope :auth_user do
    authenticated :auth_user do
      root 'templates#articles', as: :authenticated_root
    end

    unauthenticated do
      root 'auth_users/sessions#new', as: :unauthenticated_root
    end
  end

  devise_scope :administrator do
    authenticated :administrator do
      root 'templates#articles', as: :authenticated_admin
    end

    unauthenticated do
      root 'auth_users/sessions#new', as: :unauthenticated_admin
    end
  end

end
