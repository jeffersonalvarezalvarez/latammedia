class ApplicationController < ActionController::Base
	before_action :set_global_variables
  before_action :getUserData
	before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception

    private
  	
	    def set_global_variables
	    	@positions_templates = ['top', 'left1', 'left2', 'left3', 'left4', 'left5', 'right1', 'right2', 'right3', 'right4', 'right5', 'bottom']
        @email_templates = ["Bienvenida", "Vencimiento"]
	    end

      def getUserData
        @config = CmsSiteconfig.where("id=2").first
      end

      # Método que define redirección después de autenticado
      def after_sign_in_path_for(resource)
        if resource.class.name == "AuthUser"
          if resource.auth_groups.where("group_id = '4'").present?
            profile_path
          else
            articles_url
          end
        elsif resource.class.name == "Administrator"
            cookies.encrypted[:admin_auth_token] = {
              value: current_administrator.auth_token,
              expires: params[:remember_me].eql?("1") ? 1.year.from_now : nil,
              httponly: true, # JavaScript should not read this cookie
            }
            admin_auth_users_path
        end
      end

      # Método para permitir atributos en el inicio de sesión 
      def configure_permitted_parameters
        sign_up = [:email, :password, :username, :first_name, :last_name, :date_joined, :terms, latamusers_userprofiles_attributes: [:education, :english_level, :ocupation, :type, :id, :date_subscription, :date_end, :subscripted]]
        update_account = [:email, :password, :first_name, :last_name, latamusers_userprofiles_attributes: [:city, :country, :thumbnail, :education, :english_level, :ocupation, :gender, :birthday, :type, :id]]
        devise_parameter_sanitizer.permit :sign_up, keys: sign_up
        devise_parameter_sanitizer.permit :account_update, keys: update_account
      end

end
