class TemplatesController < ApplicationController

	skip_before_action :verify_authenticity_token

	before_action :validate_caracterization, only: [:articles, :all, :assigned_activities, :guided_content]

	before_action :authenticate_auth_user!, only: [:all, :media, :reading, :vocabulary, :caracterization, :create_caracterization, 
		:profile, :articles, :activity, :create_group, :create_activity, :create_sequence, :students_in_group, :students_with_sequence, :student_activities, :elearning, 
		:my_subscription, :edit_password, :update_password, :assigned_activities, :guided_content, :reports, :preview_sequence]
	
	before_action :authenticate_administrator!, only: [:import, :import_users, :preview, :generate_audio_layout, :view_as_user]
	
	before_action :load_managed_class!, only: [:edit_password_admin, :update_password_admin]
	
	before_action :load_page_reports, only: [:reports]

	def load_page_reports
		if !(current_auth_user.auth_permissions.where('"auth_permission"."codename" = ?', "generate_report").present? || current_auth_user.is_superuser == true)
			flash[:danger] = "You can't access to this page"
			redirect_to profile_path
		end
	end

	def load_managed_class!
    	@managed_class ||= Tolaria.managed_classes.find do |managed_class|
      		self.class.to_s == "Admin::#{managed_class.controller_name}"
    	end
  	end

  	def rejected_activities
  		all_reject = ""
		counter = 0 
		sequences = Sequence.all
		sequences.each do |sequence|
			sequence.lms_activities.each do |activity|

				if activity.sequence_lms_activities.where("type_activity = ?", '5').count > 0
					next
				end

				counter = counter + 1
				if counter == 1 
					all_reject = all_reject + "'"+activity.id.to_s+"'"
				else 
					all_reject = all_reject + ",'"+activity.id.to_s+"'"
				end 
			end
		end

		if all_reject.blank?
			all_reject = "'0'"
		end

		return all_reject
  	end

  	def guided_content
  		@latamusers_students = current_auth_user.latamusers_students
  		@assigned_sequences_user = SequenceUser.joins(:latamusers_assignedsequence).where('auth_user_id = ? AND assigned = true AND active = true AND start_date <= ? AND end_date >= ?', current_auth_user.id, Date.today, Date.today)
  	end

  	def pages
  		page = params[:page]

  		if page == "about-us"
  			@content = CmsPage.find(5).content
  		elsif page == "terminos"
  			@content = CmsPage.find(3).content
  		end 
  	end

	#Funciones para las actividades
	
	def index
		@disable_nav = true
		@disable_foot = true
	end

	def vacio
	end

	def all

		@id_page = CmsPage.find_by_title('Activities').id
		@categories_menu = LmsCategory.all
		@levels = LmsLevel.all.order(:difficulty_level)
		@sections = NewsSection.all.joins(:django_sites).where('domain = ?', 'elpost.co')
        if !LatamusersStudent.where('user_id = ?',current_auth_user.id).first.nil?
			institution = LatamusersStudent.where('user_id = ?',current_auth_user.id).first.institution_id
	        if CategoryInstitution.where('institution_id = ?',institution).count > 0
	        	@sections = CategoryInstitution.select('section_id as id').where('institution_id = ?',institution).all
	        end
        end
        @iconos = "all"

    	all_sections = "" 
		counter = 0 
		@sections.each do |section| 
		counter = counter + 1 
			if counter == 1 
				all_sections = all_sections + "'"+section.id.to_s+"'"
			else 
				all_sections = all_sections + ",'"+section.id.to_s+"'"
			end 
		end

		all_levels = "" 
		counter = 0 
		@levels.each do |level| 
		counter = counter + 1 
			if counter == 1 
				all_levels = all_levels + "'"+level.id.to_s+"'"
			else 
				all_levels = all_levels + ",'"+level.id.to_s+"'"
			end 
		end

		if params[:keyword] and params[:level]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND lower(name) like ? AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ?', '%'+params[:keyword].downcase+'%', 'true').order('id DESC').page(params[:p]).per(10)
		elsif params[:keyword]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND lower(name) like ? AND published = ?', '%'+params[:keyword].downcase+'%','true').order('id DESC').page(params[:p]).per(10)
		elsif params[:level]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ?', 'true').order('id DESC').page(params[:p]).per(10)
		else
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND published = ?', 'true').order('id DESC').page(params[:p]).per(10)
		end

	end

	def media
		@id_page = CmsPage.find_by_title('Activities').id
		@categories_menu = LmsCategory.all
		@levels = LmsLevel.all.order(:difficulty_level)
		@sections = NewsSection.all.joins(:django_sites).where('domain = ?', 'elpost.co')
		@category = LmsCategory.find(3)

    	all_sections = "" 
		counter = 0 
		@sections.each do |section| 
		counter = counter + 1 
			if counter == 1 
				all_sections = all_sections + "'"+section.id.to_s+"'"
			else 
				all_sections = all_sections + ",'"+section.id.to_s+"'"
			end 
		end

		all_levels = "" 
		counter = 0 
		@levels.each do |level| 
		counter = counter + 1 
			if counter == 1 
				all_levels = all_levels + "'"+level.id.to_s+"'"
			else 
				all_levels = all_levels + ",'"+level.id.to_s+"'"
			end 
		end

		if params[:keyword] and params[:level]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND lower(name) like ? AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ? AND category_id = ?', '%'+params[:keyword].downcase+'%', 'true', '3').group(:id).order('id DESC').page(params[:p]).per(10)
		elsif params[:keyword]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND lower(name) like ? AND published = ? AND category_id = ?', '%'+params[:keyword].downcase+'%','true', '3').group(:id).order('id DESC').page(params[:p]).per(10)
		elsif params[:level]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ? AND category_id = ?', 'true', '3').group(:id).order('id DESC').page(params[:p]).per(10)
		else
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND published = ? AND category_id = ?', 'true', '3').group(:id).order('id DESC').page(params[:p]).per(10)
		end
	end

	def reading
		@id_page = CmsPage.find_by_title('Activities').id
		@categories_menu = LmsCategory.all
		@levels = LmsLevel.all.order(:difficulty_level)
		@sections = NewsSection.all.joins(:django_sites).where('domain = ?', 'elpost.co')
		@category = LmsCategory.find(1)
		
    	all_sections = "" 
		counter = 0 
		@sections.each do |section| 
		counter = counter + 1 
			if counter == 1 
				all_sections = all_sections + "'"+section.id.to_s+"'"
			else 
				all_sections = all_sections + ",'"+section.id.to_s+"'"
			end 
		end

		all_levels = "" 
		counter = 0 
		@levels.each do |level| 
		counter = counter + 1 
			if counter == 1 
				all_levels = all_levels + "'"+level.id.to_s+"'"
			else 
				all_levels = all_levels + ",'"+level.id.to_s+"'"
			end 
		end

		if params[:keyword] and params[:level]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND lower(name) like ? AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ? AND category_id = ?', '%'+params[:keyword].downcase+'%', 'true', '1').group(:id).order('id DESC').page(params[:p]).per(10)
		elsif params[:keyword]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND lower(name) like ? AND published = ? AND category_id = ?', '%'+params[:keyword].downcase+'%','true', '1').group(:id).order('id DESC').page(params[:p]).per(10)
		elsif params[:level]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ? AND category_id = ?', 'true', '1').group(:id).order('id DESC').page(params[:p]).per(10)
		else
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND published = ? AND category_id = ?', 'true', '1').group(:id).order('id DESC').page(params[:p]).per(10)
		end
	end

	def vocabulary
		@id_page = CmsPage.find_by_title('Activities').id
		@categories_menu = LmsCategory.all
		@levels = LmsLevel.all.order(:difficulty_level)
		@sections = NewsSection.all.joins(:django_sites).where('domain = ?', 'elpost.co')
		@category = LmsCategory.find(2)
		
    	all_sections = "" 
		counter = 0 
		@sections.each do |section| 
		counter = counter + 1 
			if counter == 1 
				all_sections = all_sections + "'"+section.id.to_s+"'"
			else 
				all_sections = all_sections + ",'"+section.id.to_s+"'"
			end 
		end

		all_levels = "" 
		counter = 0 
		@levels.each do |level| 
		counter = counter + 1 
			if counter == 1 
				all_levels = all_levels + "'"+level.id.to_s+"'"
			else 
				all_levels = all_levels + ",'"+level.id.to_s+"'"
			end 
		end

		if params[:keyword] and params[:level]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND lower(name) like ? AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ? AND category_id = ?', '%'+params[:keyword].downcase+'%', 'true', '2').group(:id).order('id DESC').page(params[:p]).per(10)
		elsif params[:keyword]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND lower(name) like ? AND published = ? AND category_id = ?', '%'+params[:keyword].downcase+'%','true', '2').group(:id).order('id DESC').page(params[:p]).per(10)
		elsif params[:level]
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ? AND category_id = ?', 'true', '2').group(:id).order('id DESC').page(params[:p]).per(10)
		else
			@activities = LmsActivity.all.joins(:lms_level, :news_article_sections, :lms_questions).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND published = ? AND category_id = ?', 'true', '2').group(:id).order('id DESC').page(params[:p]).per(10)
		end
	end

	# Funciones para agregar u actualizar caracterizaciones
	def caracterization
		@user = current_auth_user
	end

	def create_caracterization
		@user = current_auth_user

		ids = params[:checkboxes]
		ids_rel = []

		relations = @user.caracterization_citemclientrelations
		relations.each do |rel|
			ids_rel << rel.citem_id.to_s
		end

		if !ids.present?
			current_auth_user.caracterization_citemclientrelations.destroy_all
		end

		ids_rel.each do |id|
			if ids.present?
				if !(ids.include? id)
					CaracterizationCitemclientrelation.where("citem_id = ? AND user_id = ?", id, @user.id).first.destroy
				end
			end
		end

		if ids.present?
			ids.each do |id|
				if !(ids_rel.include? id)
					car = CaracterizationCitemclientrelation.new(:citem_id => id, :user_id => @user.id, :current_state => true)
					car.save!
				end
			end
		end

		redirect_to caracterization_profile_path
		flash[:success] = "Successfully updated your caracterization"
	end

	def reports

		@institution = nil
		@teachers = nil
		current_auth_user.auth_groups.each do |auth_group|
			@institution = LatamusersIntitution.find_by_name(auth_group.name)
			if @institution.present?
				break
			end
		end

		if @institution.present?
			@teachers = AuthUser.joins(:latamusers_students_teacher, :auth_groups).where('"latamusers_student"."institution_id" = ? AND "auth_group"."id" = ?', @institution.id, 4).group('"auth_users"."id"').order('"auth_users"."date_joined" desc')
			@teacher_paginate = AuthUser.joins(:latamusers_students_teacher, :auth_groups).where('"latamusers_student"."institution_id" = ? AND "auth_group"."id" = ?', @institution.id, 4).group('"auth_users"."id"').order('"auth_users"."date_joined" desc').page(params[:page]).per(6)
		else
			@teachers = AuthUser.joins(:auth_groups).where("group_id = ?", 0) 
		end

	end

	def profile

		@categories = LmsCategory.all
		@user_activities = current_auth_user.latamusers_useractivities.joins(:lms_activity, :lms_questions)
		@levels = LmsLevel.all.order(:difficulty_level)
		@sections = NewsSection.joins(:django_sites).where('domain = ?', 'elpost.co')
		@activities_init = LmsActivity.joins(:lms_level, :news_article_sections).where("difficulty_level = '0' AND section_id = 39")
		@sequences_init = Sequence.all
		@groups = current_auth_user.latamusers_usergroups

		@arreglo_general = general_year_scores(current_auth_user, "all")
		@arreglo_category_reading = general_by_category_scores(current_auth_user, 1, "all")
		@arreglo_category_vocabulary = general_by_category_scores(current_auth_user, 2, "all")
		@arreglo_category_media = general_by_category_scores(current_auth_user, 3, "all")

		if params["stats-range"] 
			if params["stats-range"] == "mes"
				if params["assigned"]
					if params["assigned"] == "true" 
						@arreglo_general = general_month_scores(current_auth_user, true)
						@arreglo_category_reading = month_by_category_scores(current_auth_user, 1, true)
						@arreglo_category_vocabulary = month_by_category_scores(current_auth_user, 2, true)
						@arreglo_category_media = month_by_category_scores(current_auth_user, 3, true)
					elsif params["assigned"] == "false"
						@arreglo_general = general_month_scores(current_auth_user, false)
						@arreglo_category_reading = month_by_category_scores(current_auth_user, 1, false)
						@arreglo_category_vocabulary = month_by_category_scores(current_auth_user, 2, false)
						@arreglo_category_media = month_by_category_scores(current_auth_user, 3, false)
					elsif params["assigned"] == "all"
						@arreglo_general = general_month_scores(current_auth_user, "all")
						@arreglo_category_reading = month_by_category_scores(current_auth_user, 1, "all")
						@arreglo_category_vocabulary = month_by_category_scores(current_auth_user, 2, "all")
						@arreglo_category_media = month_by_category_scores(current_auth_user, 3, "all")
					end
				end
			elsif params["stats-range"] == "semana"
				if params["assigned"]
					if params["assigned"] == "true"
						@arreglo_general = general_week_scores(current_auth_user,true)
						@arreglo_category_reading = week_by_category_scores(current_auth_user, 1, true)
						@arreglo_category_vocabulary = week_by_category_scores(current_auth_user, 2, true)
						@arreglo_category_media = week_by_category_scores(current_auth_user, 3, true)
					elsif params["assigned"] == "false"
						@arreglo_general = general_week_scores(current_auth_user, false)
						@arreglo_category_reading = week_by_category_scores(current_auth_user, 1, false)
						@arreglo_category_vocabulary = week_by_category_scores(current_auth_user, 2, false)
						@arreglo_category_media = week_by_category_scores(current_auth_user, 3, false)
					elsif params["assigned"] == "all"
						@arreglo_general = general_week_scores(current_auth_user, "all")
						@arreglo_category_reading = week_by_category_scores(current_auth_user, 1, "all")
						@arreglo_category_vocabulary = week_by_category_scores(current_auth_user, 2, "all")
						@arreglo_category_media = week_by_category_scores(current_auth_user, 3, "all")
					end
				end
			elsif params["stats-range"] == "año"
				if params["assigned"] 
					if params["assigned"] == "true"
						@arreglo_general = general_year_scores(current_auth_user, true)
						@arreglo_category_reading = general_by_category_scores(current_auth_user, 1, true)
						@arreglo_category_vocabulary = general_by_category_scores(current_auth_user, 2, true)
						@arreglo_category_media = general_by_category_scores(current_auth_user, 3, true)
					elsif params["assigned"] == "false"
						@arreglo_general = general_year_scores(current_auth_user, false)
						@arreglo_category_reading = general_by_category_scores(current_auth_user, 1, false)
						@arreglo_category_vocabulary = general_by_category_scores(current_auth_user, 2, false)
						@arreglo_category_media = general_by_category_scores(current_auth_user, 3, false)
					elsif params["assigned"] == "all"
						@arreglo_general = general_year_scores(current_auth_user, "all")
						@arreglo_category_reading = general_by_category_scores(current_auth_user, 1, "all")
						@arreglo_category_vocabulary = general_by_category_scores(current_auth_user, 2, "all")
						@arreglo_category_media = general_by_category_scores(current_auth_user, 3, "all")
					end
				end
			end
		end

	end

	# Funciones para armar estadísticas
	def general_week_scores(user, assigned)

	    now = Time.now
	    year = now.year
	    weekStart = 2
	    weekNo = now.strftime("%U")
	    weekDates = []
	    date_to_convert = year.to_s + '-01-01'
	    janOne = date_to_convert.to_time.strftime('%Y-%m-%d')

	    dayOfFirstWeek = ((7-(janOne.to_time.strftime("%u").to_i)+ weekStart.to_i) % 7)
	    
	    if dayOfFirstWeek == 0
	        dayOfFirstWeek = 7
	    end

	    date_to_convert = year.to_s + '-01-' + dayOfFirstWeek.to_s
	    dateOfFirstWeek = date_to_convert.to_time.strftime('%Y-%m-%d')
	    dateOfFirstWeek = dateOfFirstWeek.to_time
	    dayOne = DateTime.new(dateOfFirstWeek.year, dateOfFirstWeek.month, dateOfFirstWeek.day)
	    daysToGo = 7*(weekNo.to_i-2)
	    lastDay = daysToGo+6
	    dayX = nil
	    attempts = []
	    passed = []

	    while daysToGo <= lastDay do
	        dayX = dayOne + daysToGo.days
	        date_to_convert = dayX.year.to_s+'-'+dayX.month.to_s+'-'+dayX.day.to_s
	        resultDateX = date_to_convert.to_time.strftime('%Y-%m-%d')
	        formatedDate = resultDateX.to_time.strftime("%Y-%m-%d")
	        daysToGo += 1

	        if assigned == false
	            activities = LatamusersUseractivity.all.where('user_id = ? AND date = ? AND assigned = false', user.id, formatedDate)
	            passed << activities.where('successfull = true').count
	        elsif assigned == true
	            activities = LatamusersUseractivity.all.where('user_id = ? AND date = ? AND assigned = true', user.id, formatedDate)
	            passed << activities.where('successfull = true').count
	        elsif assigned == "all"
	        	activities = LatamusersUseractivity.all.where('user_id = ? AND date = ?', user.id, formatedDate)
	            passed << activities.where('successfull = true').count
	        end

	        attempts << activities.count

	   	end

		return [attempts, passed]

	end

	def general_month_scores(user, assigned)

	    now = Time.now
	    attempts = []
	    passed = []

	    for day in 1..now.day+1
	    	date_to_convert = now.year.to_s+'-'+now.month.to_s+'-'+now.day.to_s
	        date = date_to_convert.to_time.strftime("%Y-%m-%d")
	        date = date.to_time.strftime("%Y-%m-%d")

	        if assigned == false
	        	que = LatamusersUseractivity.all.where('user_id = ? AND date = ? AND assigned = false',user.id,date)
	            passed << que.where('successfull=true').count
	        elsif assigned == true
	        	que = LatamusersUseractivity.all.where('user_id = ? AND date = ? AND assigned = true',user.id,date)
	            passed << que.where('successfull=true').count
	        elsif assigned == "all"
	        	que = LatamusersUseractivity.all.where('user_id = ? AND date = ?',user.id,date)
	            passed << que.where('successfull=true').count
	        end

	        attempts << que.count

	    end

	    return [attempts, passed]

    end

	def general_year_scores(user, assigned)

	    now = Time.now
	    year = now.year
	    mon = now.month
	    attempts = []
	    passed = []

    	for monthc in 1..12

	        ano = year

	        if monthc > mon.to_i
	            ano = ano - 1.years
	        end

	        date_to_convert = ano.to_s+'-'+monthc.to_s+'-1'
	        date = date_to_convert.to_time.strftime("%Y-%m-%d")

	        if assigned == false
	        	que = LatamusersUseractivity.all.where("user_id = ? AND date_part('month',date) = ? AND date_part('year',date) = ? AND assigned=false", user.id, date.to_time.month.to_s, date.to_time.year.to_s)
	            passed << que.where('successfull=true').count
	        elsif assigned == true
	            que = LatamusersUseractivity.all.where("user_id = ? AND date_part('month',date) = ? AND date_part('year',date) = ? AND assigned=true", user.id, date.to_time.month.to_s, date.to_time.year.to_s)
	            passed << que.where('successfull=true').count
			elsif assigned == "all"
				que = LatamusersUseractivity.all.where("user_id = ? AND date_part('month',date) = ? AND date_part('year',date) = ?", user.id, date.to_time.month.to_s, date.to_time.year.to_s)
	            passed << que.where('successfull=true').count
			end

	        attempts << que.count
		end

    	return [attempts, passed]
    
    end 

	def general_by_category_scores(user, category, assigned)

	    now = Time.now
	    year = now.year
	    mon = now.month
	    attempts = []
	    passed = []

    	for monthc in 1..12

	        ano = year

	        if monthc > mon.to_i
	            ano = ano - 1.years
	        end

	        date_to_convert = ano.to_s+'-'+monthc.to_s+'-1'
	        date = date_to_convert.to_time.strftime("%Y-%m-%d")

	        if assigned == false
	        	que = LatamusersUserquestion.all.where("user_id = ? AND date_part('month',date) = ? AND date_part('year',date) = ? AND assigned=false AND category_id = ?", user.id, date.to_time.month.to_s, date.to_time.year.to_s, category)
	            passed << que.where('passed=true').count
	        elsif assigned == true
	            que = LatamusersUserquestion.all.where("user_id = ? AND date_part('month',date) = ? AND date_part('year',date) = ? AND assigned=true AND category_id = ?", user.id, date.to_time.month.to_s, date.to_time.year.to_s, category)
	            passed << que.where('passed=true').count
			elsif assigned == "all"
				que = LatamusersUserquestion.all.where("user_id = ? AND date_part('month',date) = ? AND date_part('year',date) = ? AND category_id = ?", user.id, date.to_time.month.to_s, date.to_time.year.to_s, category)
	            passed << que.where('passed=true').count
			end

	        attempts << que.count
		end

    	return [attempts, passed]
    
    end 

    def month_by_category_scores(user, category, assigned)

    	now = Time.now
	    attempts = []
	    passed = []

	    for day in 1..now.day+1
	    	date_to_convert = now.year.to_s+'-'+now.month.to_s+'-'+now.day.to_s
	        date = date_to_convert.to_time.strftime("%Y-%m-%d")
	        date = date.to_time.strftime("%Y-%m-%d")

	        if assigned == false
	        	que = LatamusersUserquestion.all.where('user_id = ? AND date = ? AND assigned = false AND category_id = ?',user.id,date,category)
	            passed << que.where('passed=true').count
	        elsif assigned == true
	        	que = LatamusersUserquestion.all.where('user_id = ? AND date = ? AND assigned = true AND category_id = ?',user.id,date,category)
	            passed << que.where('passed=true').count
	        elsif assigned == "all"
	        	que = LatamusersUserquestion.all.where('user_id = ? AND date = ? AND category_id = ?',user.id,date,category)
	            passed << que.where('passed=true').count
	        end

	        attempts << que.count

	    end

	    return [attempts, passed]

    end 

    def week_by_category_scores(user, category, assigned)
        	
    	now = Time.now
	    year = now.year
	    weekStart = 2
	    weekNo = now.strftime("%U")
	    weekDates = []
	    date_to_convert = year.to_s + '-01-01'
	    janOne = date_to_convert.to_time.strftime('%Y-%m-%d')

	    dayOfFirstWeek = ((7-(janOne.to_time.strftime("%u").to_i)+ weekStart.to_i) % 7)
	    
	    if dayOfFirstWeek == 0
	        dayOfFirstWeek = 7
	    end

	    date_to_convert = year.to_s + '-01-' + dayOfFirstWeek.to_s
	    dateOfFirstWeek = date_to_convert.to_time.strftime('%Y-%m-%d')
	    dateOfFirstWeek = dateOfFirstWeek.to_time
	    dayOne = DateTime.new(dateOfFirstWeek.year, dateOfFirstWeek.month, dateOfFirstWeek.day)
	    daysToGo = 7*(weekNo.to_i-2)
	    lastDay = daysToGo+6
	    dayX = nil
	    attempts = []
	    passed = []

	    while daysToGo <= lastDay do
	        dayX = dayOne + daysToGo.days
	        date_to_convert = dayX.year.to_s+'-'+dayX.month.to_s+'-'+dayX.day.to_s
	        resultDateX = date_to_convert.to_time.strftime('%Y-%m-%d')
	        formatedDate = resultDateX.to_time.strftime("%Y-%m-%d")
	        daysToGo += 1

	        if assigned == false
	            activities = LatamusersUserquestion.all.where('user_id = ? AND date = ? AND assigned = false AND category_id = ?', user.id, formatedDate, category)
	            passed << activities.where('passed = true').count
	        elsif assigned == true
	            activities = LatamusersUserquestion.all.where('user_id = ? AND date = ? AND assigned = true AND category_id = ?', user.id, formatedDate, category)
	            passed << activities.where('passed = true').count
	        elsif assigned == "all"
	        	activities = LatamusersUserquestion.all.where('user_id = ? AND date = ? AND category_id = ?', user.id, formatedDate, category)
	            passed << activities.where('passed = true').count
	        end

	        attempts << activities.count

	   	end

		return [attempts, passed]

    end 

	def activity

		@activity = LmsActivity.where('id = ?', params[:id]).first
		
		# Para aumentar en 1 los hits
		layout = @activity.news_article.news_layouts.where("site_id = ?", 3).first
		if layout.present?
			hits = layout.hits
			layout.update_attribute(:hits, hits+1)
		end

		# Para obtener distintas opciones del artículo
		content = NewsArticle.where(id: @activity.article_id).first.content
		@fullName = @activity.auth_user.first_name+' '+@activity.auth_user.last_name
		@content = cleaned_article(content)

		# Para obtener las preguntas del artículo
        @Questions = LmsQuestion.all.order(id: :asc).includes(:lms_answers, :media_medium).where('activity_id = ?', params[:id])
		@Questions_temp = []
		@dragAndDropParent = []
		@dragAndDrop = []
		
		@Questions.each do |question|
			answers = []
			counter = 0
			question.lms_answers.each do |answer|
				answers[counter] = JSON.parse(answer.to_json(:methods => [:media_url, :media_type]))
				counter = counter + 1
			end

			if question.type_id == 3
				if question.parent.nil?
		        	@dragAndDropParent << { :type => 3, :id => question.id, :definition => question.definition.to_s, :text => question.text.to_s, :category => question.category_id, :answer => question.lms_answers[0].text, :asociations => [], :order => question.order }
				else
					@dragAndDrop << { :parent => question.parent, :id => question.id, :text => question.text.to_s, :category => question.category_id, :answer => question.lms_answers[0].text }
				end
            elsif question.media_medium.present?
				url = question.media_medium.media.url.sub("/media/uploads/uploads/", "/media/uploads/")
				media = {:author => question.media_medium.author, :id => question.media_medium.id, :iframe_url => question.media_medium.iframe_url, :media => url, :poster => question.media_medium.poster.url, :title => question.media_medium.title, :type => question.media_medium.type}
		        @Questions_temp << { :category => question.category_id, :fill_with_voice => question.fill_with_voice, :type => question.type_id, :id => question.id, :text => question.text, :media => media, :answers => answers, :order => question.order }
            else 
				media = {:author => 'none', :id => 'none', :iframe_url => 'none', :media => 'none', :poster => 'none', :title => 'none', :type => 'none' }
		        @Questions_temp << { :category => question.category_id, :fill_with_voice => question.fill_with_voice, :type => question.type_id, :id => question.id, :text => question.text, :media => media, :answers => answers, :order => question.order }
			end
        end

		@dragAndDropParent.each do |parent|
			@dragAndDrop.each do |question|
				if parent[:id] == question[:parent]
        			parent[:asociations] << question
				end
			end
    		@Questions_temp << parent
    	end

		questions = @Questions_temp.to_json

		if @Questions_temp.empty?
			@questions = @Questions_temp 
		elsif
			@questions = questions 
		end
		@Activity = @activity.to_json
      	  
		user = current_auth_user
        assigned_activities = LatamusersUseractivity.all.joins(:latamusers_assignedactivity).where('user_id = ? AND assigned = true AND current_try <= 3 AND latamusers_useractivity.activity_id= ? AND active = true', user.id, params[:id])
        @assigned_activity = false

        if assigned_activities.count > 0

           	if assigned_activities.last.current_try < assigned_activities.last.latamusers_assignedactivity.maximum_tries
	            @assigned_activity_message = 'This activity has been assigned to you by '+assigned_activities.last.latamusers_assignedactivity.auth_user.first_name+' '+assigned_activities.last.latamusers_assignedactivity.auth_user.last_name+' and this is your try number '+(assigned_activities.last.current_try+1).to_s+' of a maximum of '+assigned_activities.last.latamusers_assignedactivity.maximum_tries.to_s+' tries <br> This activity must be done between ' + assigned_activities.last.latamusers_assignedactivity.start_date.strftime("%B, %d - %Y") + ' and ' + assigned_activities.last.latamusers_assignedactivity.end_date.strftime("%B, %d - %Y")
	       		@assigned_activity = assigned_activities.last
       		end

			if assigned_activities.last.current_try >= assigned_activities.last.latamusers_assignedactivity.maximum_tries
            	@questions = []
            	@assigned_activity_message = 'This activity has been assigned to you by '+assigned_activities.last.latamusers_assignedactivity.auth_user.first_name+' '+assigned_activities.last.latamusers_assignedactivity.auth_user.last_name+' but you can\'t try again because you complete the maximum of '+assigned_activities.last.latamusers_assignedactivity.maximum_tries.to_s+' tries' 
            end

            if assigned_activities.last.latamusers_assignedactivity.start_date > Date.today
            	@questions = []
            	@assigned_activity_message = 'This activity has been assigned to you by '+assigned_activities.last.latamusers_assignedactivity.auth_user.first_name+' '+assigned_activities.last.latamusers_assignedactivity.auth_user.last_name+' but you can\'t open it because the assignation starts at ' + assigned_activities.last.latamusers_assignedactivity.start_date.strftime("%B, %d - %Y")
            end

            if Date.today > assigned_activities.last.latamusers_assignedactivity.end_date
            	@questions = []
            	@assigned_activity_message = 'This activity has been assigned to you by '+assigned_activities.last.latamusers_assignedactivity.auth_user.first_name+' '+assigned_activities.last.latamusers_assignedactivity.auth_user.last_name+' but you can\'t open it because the assignation has expired at ' + assigned_activities.last.latamusers_assignedactivity.end_date.strftime("%B, %d - %Y")
            end

       	end
	end

	def preview

		begin
			@activity = LmsActivity.where('id = ?', params[:id]).first
			content = NewsArticle.where(id: @activity.article_id).first.content
			@fullName = @activity.auth_user.first_name+' '+@activity.auth_user.last_name
			@content = cleaned_article(content)

	        @Questions = LmsQuestion.all.order(id: :asc).includes(:lms_answers, :media_medium).where('activity_id = ?', params[:id])
			@Questions_temp = []
			@dragAndDropParent = []
			@dragAndDrop = []
			
			@Questions.each do |question|
				answers = []
				counter = 0
				question.lms_answers.each do |answer|
					answers[counter] = JSON.parse(answer.to_json(:methods => [:media_url, :media_type]))
					counter = counter + 1
				end

				if question.type_id == 3
					if question.parent.nil?
			        	@dragAndDropParent << { :type => 3, :id => question.id, :definition => question.definition.to_s, :text => question.text, :category => question.category_id, :answer => question.lms_answers[0].text, :asociations => [], :order => question.order }
					else
						@dragAndDrop << { :parent => question.parent, :id => question.id, :text => question.text, :category => question.category_id, :answer => question.lms_answers[0].text }
					end
	            elsif question.media_medium.present?
					url = question.media_medium.media.url.sub("/media/uploads/uploads/", "/media/uploads/")
					media = {:author => question.media_medium.author, :id => question.media_medium.id, :iframe_url => question.media_medium.iframe_url, :media => url, :poster => question.media_medium.poster.url, :title => question.media_medium.title, :type => question.media_medium.type}
			        @Questions_temp << { :category => question.category_id, :fill_with_voice => question.fill_with_voice, :type => question.type_id, :id => question.id, :text => question.text, :media => media, :answers => answers, :order => question.order }
	            else 
					media = {:author => 'none', :id => 'none', :iframe_url => 'none', :media => 'none', :poster => 'none', :title => 'none', :type => 'none' }
			        @Questions_temp << { :category => question.category_id, :fill_with_voice => question.fill_with_voice, :type => question.type_id, :id => question.id, :text => question.text, :media => media, :answers => answers, :order => question.order }
				end
	        end

			@dragAndDropParent.each do |parent|
				@dragAndDrop.each do |question|
					if parent[:id] == question[:parent]
	        			parent[:asociations] << question
					end
				end
	    		@Questions_temp << parent
	    	end

			questions = @Questions_temp.to_json

			if @Questions_temp.empty?
				@questions = @Questions_temp 
			elsif
				@questions = questions 
			end
			@Activity = @activity.to_json
		rescue Exception => e
			flash[:error] = "There is an error to preview the activity: " + e.to_s
			redirect_to admin_lms_activities_path
		end
      	  
	end

	def lesscount
		@activity = LmsActivity.where('id = ?', params[:id]).first

		layout = @activity.news_article.news_layouts.where("site_id = ?", 3).first
		if layout.present?
			hits = layout.hits
			layout.update_attribute(:hits, hits-1)
		end
	end

    def cleaned_article(content)
    	content = content.gsub(/&nbsp;/i,' ')

        vocabulary = LmsActivityVocabulary.where("activity_id = ?", params[:id])
        vocabulary.each do |word|
			content = content.gsub(/\b#{word.lms_word.word.strip}\b/i, '<a class="definition"><span class="tooltiptext">'+ if word.lms_word.lms_definitions.first.present? then word.lms_word.lms_definitions.first.definition.capitalize else 'No definition' end + '<br>'+ if word.lms_word.lms_definitions.first.example.present? then 'Example: ' else '' end + word.lms_word.lms_definitions.first.example.capitalize.to_s+'</span>\0</a>') 
        end

        return content
    end

	def articles

		@id_page = CmsPage.find_by_title('Articles').id
		@categories_menu = LmsCategory.all
		@levels = LmsLevel.all.order(:difficulty_level)
		@sections = NewsSection.all.joins(:django_sites).where('domain = ?', 'elpost.co')
		if !LatamusersStudent.where('user_id = ?',current_auth_user.id).first.nil?
			institution = LatamusersStudent.where('user_id = ?',current_auth_user.id).first.institution_id
	        if CategoryInstitution.where('institution_id = ?',institution).count > 0
	        	@sections = CategoryInstitution.select('section_id as id, title').all.joins(:news_section).where('institution_id = ?',institution)
	        end
        end
    	@types = InstantPhrasesTipoFrase.all

    	all_sections = "" 
		counter = 0 
		@sections.each do |section| 
		counter = counter + 1 
			if counter == 1 
				all_sections = all_sections + "'"+section.id.to_s+"'"
			else 
				all_sections = all_sections + ",'"+section.id.to_s+"'"
			end 
		end

		all_levels = "" 
		counter = 0 
		@levels.each do |level| 
		counter = counter + 1 
			if counter == 1 
				all_levels = all_levels + "'"+level.id.to_s+"'"
			else 
				all_levels = all_levels + ",'"+level.id.to_s+"'"
			end 
		end

		if params[:keyword] and params[:level] and params[:section]
			@articles = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND lower(name) like ? AND published = ? AND section_id IN('+params[:section]+') AND "LMS_level"."id" IN('+params[:level]+')', '%'+params[:keyword].downcase+'%','true').order('RANDOM()').page(params[:p]).per(10)
		elsif params[:level] and params[:section]
			@articles = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+ params[:section] +') AND published = ?', 'true').order('RANDOM()').page(params[:p]).per(10)
		elsif params[:keyword] and params[:section]
			@articles = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND lower(name) like ? AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+params[:section]+') AND published = ?','%'+params[:keyword].downcase+'%', 'true').order('RANDOM()').page(params[:p]).per(10)
		elsif params[:keyword] and params[:level]
			@articles = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND lower(name) like ? AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ?', '%'+params[:keyword].downcase+'%', 'true').order('RANDOM()').page(params[:p]).per(10)
		elsif params[:keyword]
			@articles = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND lower(name) like ? AND published = ?', '%'+params[:keyword].downcase+'%','true').order('RANDOM()').page(params[:p]).per(10)
		elsif params[:section]
			@articles = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+params[:section]+') AND published = ?', 'true').order('RANDOM()').page(params[:p]).per(10)
		elsif params[:level]
			@articles = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+params[:level]+') AND section_id IN('+all_sections+') AND published = ?', 'true').order('RANDOM()').page(params[:p]).per(10)
		else
			@articles = LmsActivity.all.joins(:lms_level, :news_article_sections).where('"LMS_activity"."id" NOT IN ('+rejected_activities+') AND "LMS_level"."id" IN ('+all_levels+') AND section_id IN('+all_sections+') AND published = ?', 'true').order('RANDOM()').page(params[:p]).per(10)
		end

	end

	def questions
		userquestion = LatamusersUserquestion.new(:user_id => params["user"], :question_id => params["question"], :category_id => params["category"], :passed => params["passed"], :assigned => params["assigned_activity"].present?, :date => DateTime.now)
		userquestion.save
	end

	def openquestions
		openquestion = OpenQuestion.new(:auth_user_id => params["user"], :lms_question_id => params["question"], :content => params["answer_without_corrections"], :content_errors => params["answer_with_corrections"])
		openquestion.save
	end

	def postActivity

		if params["assigned_activity"].present?
			user_activities = LatamusersUseractivity.where("assigned_activity_id = ? AND activity_id = ? AND user_id = ?", params["assigned_activity"], params["activity"], params["user"])
			if user_activities.count > 0
				user_activities.first.update(:user_id => params["user"], 
					:activity_id => params["activity"], 
					:successfull => params["successfull"], 
					:assigned => params["assigned_activity"].present?, 
					:assigned_activity_id => params["assigned_activity"], 
					:score => if user_activities.first.score > params["score"] then user_activities.first.score else params["score"] end, 
					:possibleScore => params["possibleScore"], 
					:current_try => user_activities.first.current_try + 1, 
					:date => DateTime.now)
			else
				user = LatamusersUseractivity.new(:user_id => params["user"], 
					:activity_id => params["activity"], 
					:successfull => params["successfull"], 
					:assigned => params["assigned_activity"].present?, 
					:assigned_activity_id => params["assigned_activity"], 
					:score => params["score"], 
					:possibleScore => params["possibleScore"], 
					:current_try => 1, 
					:date => DateTime.now)
				user.save!
			end 
		else
			user_activities = LatamusersUseractivity.where("assigned=false AND activity_id = ? AND user_id = ?", params["activity"], params["user"])
			if user_activities.count > 0
				user_activities.first.update(:user_id => params["user"], 
					:activity_id => params["activity"], 
					:successfull => params["successfull"], 
					:assigned => params["assigned_activity"].present?, 
					:score => if user_activities.first.score > params["score"] then user_activities.first.score else params["score"] end, 
					:possibleScore => params["possibleScore"], 
					:current_try => user_activities.first.current_try + 1, 
					:date => DateTime.now)
			else
				user = LatamusersUseractivity.new(:user_id => params["user"], 
					:activity_id => params["activity"],
					:successfull => params["successfull"], 
					:assigned => params["assigned_activity"].present?,
					:score => params["score"],
					:possibleScore => params["possibleScore"], 
					:current_try => 1,
					:date => DateTime.now)
				user.save!
			end
		end

	end

	def phrases
    	render :json => {:total => InstantPhrasesFrase.where("tipo = ?", params['id']).count, :ids => InstantPhrasesFrase.select("id").where("tipo = ?", params['id'])}
	end

	def parts
    	phrasestemp = InstantPhrasesParte.all.joins(:instant_phrases_palabra, :instant_phrases_phrase).where("frase_id = ?", params['id'])

    	array_phrases = []
    	phrasestemp.each do |phrase|
    		answers = InstantPhrasesPalabra.select("id, sintaxis, tipo").where("tipo = ? AND id != ?", phrase.instant_phrases_palabra.tipo, phrase.instant_phrases_palabra.id).order("RANDOM()").limit(phrase.opciones-1)
			array_phrases << {:words => answers, :phrase => phrase, :correct => phrase.instant_phrases_palabra.sintaxis, :url => phrase.instant_phrases_phrase.imagen}
    	end
		
		render :json => array_phrases
	end

	def create_group
		
	end

	def create_activity
		@id = current_auth_user.id
		@groups = current_auth_user.latamusers_usergroups
		@levels = LmsLevel.all.order(:difficulty_level)
		@sections = NewsSection.joins(:django_sites).where('domain = ?', 'elpost.co')
		@activities_init = LmsActivity.joins(:lms_level, :news_article_sections).where("difficulty_level = '0' AND section_id = 39")
		@activities_to_active = LatamusersAssignedactivity.where("teacher_id = ?", current_auth_user.id)
		if @groups[0].present?
			@students_init = AuthUser.joins(:latamusers_students).where("group_id = ? AND teacher_id = ?", @groups[0].code, current_auth_user.id).order("first_name")
		end
	end

	def create_sequence
    	level = ''
	    if params[:level].present?
	      level = ' AND level ='+params[:level]
	    end
		@id = current_auth_user.id
		@groups = current_auth_user.latamusers_usergroups
		@sections = NewsSection.joins(:django_sites).where('domain = ?', 'elpost.co')
		@sequences_init = Sequence.where('created_at is not null'+level)
		@sequences_to_active = LatamusersAssignedsequence.where("teacher_id = ?", current_auth_user.id)
		if @groups[0].present?
			@students_init = AuthUser.joins(:latamusers_students).where("group_id = ? AND teacher_id = ?", @groups[0].code, current_auth_user.id).order("first_name")
		end
	end

	# Complemento para llenar la tabla de crear actividad
	def activities_active

		activities = '{"activities":['
		activities_to_active = LatamusersAssignedactivity.where("teacher_id = ? AND active = true", current_auth_user.id).order("creation_date DESC")
		counter = 0
		activities_to_active.each do |activity|
			counter = counter + 1

			color = ""

			if activity.latamusers_useractivities.count != 0
				color = 'color:rgb(49, 156, 49)'
			else
				color = 'color:#000'
			end

			if counter == 1
				activities = activities + '{"id":"'+activity.id.to_s+'","teacher_id":"'+activity.teacher_id.to_s+
				'","activity":"'+activity.lms_activity.name.to_s+'","maximum_tries":"'+activity.maximum_tries.to_s+
				'","creation_date":"'+activity.creation_date.strftime("%b. %d, %Y").to_s+'","start_date":"'+activity.start_date.strftime("%b. %d, %Y").to_s+
 				'","end_date":"'+activity.end_date.strftime("%b. %d, %Y").to_s+'","active":"'+activity.active.to_s+'", "color":"'+color+'"}'		
			else
				activities = activities + ',{"id":"'+activity.id.to_s+'","teacher_id":"'+activity.teacher_id.to_s+
				'","activity":"'+activity.lms_activity.name.to_s+'","maximum_tries":"'+activity.maximum_tries.to_s+
				'","creation_date":"'+activity.creation_date.strftime("%b. %d, %Y").to_s+'","start_date":"'+activity.start_date.strftime("%b. %d, %Y").to_s+
				'","end_date":"'+activity.end_date.strftime("%b. %d, %Y").to_s+'","active":"'+activity.active.to_s+'", "color":"'+color+'"}'
			end
		end
		activities = activities + '], "total":'+counter.to_s+'}'

		render :json => activities
	end

	# Complemento para llenar la tabla de crear actividad
	def sequences_active

		sequences = '{"sequences":['
		sequences_to_active = LatamusersAssignedsequence.where("teacher_id = ? AND active = true", current_auth_user.id).order("creation_date DESC")
		counter = 0
		sequences_to_active.each do |sequence|
			counter = counter + 1

			group = color = ""
			if !sequence.sequence_users.first.nil?
				group = sequence.sequence_users.first.latamusers_students.first.latamusers_usergroup.name
			end
			# SequenceUser.joins(:latamusers_students).select(:auth_user_id).where("assigned_sequence_id = ? AND teacher_id = ?", params[:id], current_auth_user.id).group(:auth_user_id).first.auth_user
			if sequence.sequence_users.count != 0
				color = 'color:rgb(49, 156, 49)'
			else
				color = 'color:#000'
			end

			if counter == 1
				sequences = sequences + '{"id":"'+sequence.id.to_s+'","group":"'+group.to_s+'","teacher_id":"'+sequence.teacher_id.to_s+
				'","sequence":"'+sequence.sequence.name.to_s+'","maximum_tries":"'+sequence.maximum_tries.to_s+
				'","creation_date":"'+sequence.creation_date.strftime("%b. %d, %Y").to_s+'","start_date":"'+sequence.start_date.strftime("%b. %d, %Y").to_s+
				'","end_date":"'+sequence.end_date.strftime("%b. %d, %Y").to_s+'","active":"'+sequence.active.to_s+'", "color":"'+color+'"}'		
			else
				sequences = sequences + ',{"id":"'+sequence.id.to_s+'","group":"'+group.to_s+'","teacher_id":"'+sequence.teacher_id.to_s+
				'","sequence":"'+sequence.sequence.name.to_s+'","maximum_tries":"'+sequence.maximum_tries.to_s+
				'","creation_date":"'+sequence.creation_date.strftime("%b. %d, %Y").to_s+'","start_date":"'+sequence.start_date.strftime("%b. %d, %Y").to_s+
				'","end_date":"'+sequence.end_date.strftime("%b. %d, %Y").to_s+'","active":"'+sequence.active.to_s+'", "color":"'+color+'"}'
			end
		end
		sequences = sequences + '], "total":'+counter.to_s+'}'

		render :json =>sequences
	end

	# Complemento para llenar la tabla de crear grupo
	def current_groups
		groups = '{"groups":['
		groups_now = current_auth_user.latamusers_usergroups.order(name: :asc)
		counter = 0
		groups_now.each do |group|
			counter = counter + 1
			if counter == 1
				groups = groups + '{"code":"'+group.code.to_s+'","group":"'+group.name.to_s+'"}'		
			else
				groups = groups + ',{"code":"'+group.code.to_s+'","group":"'+group.name.to_s+'"}'
			end
		end
		groups = groups + '], "total":'+counter.to_s+'}'

		render :json => groups
	end

	# Complemento para llenar la tabla de crear grupo
	def current_groups_by_id
		groups = '{"groups":['
		groups_now = AuthUser.find(params[:id]).latamusers_usergroups
		counter = 0
		groups_now.each do |group|
			active = inactive = 0
			total = group.latamusers_students.count
			group.latamusers_students.each do |s|
				if s.auth_user_student.is_active
					active = active+1
				else
					inactive = inactive+1
				end
			end
			
			counter = counter + 1
			if counter == 1
				groups = groups + '{"code":"'+group.code.to_s+'","group":"'+group.name.to_s+'","active":"'+active.to_s+'","inactive":"'+inactive.to_s+'","total":"'+total.to_s+'"}'		
			else
				groups = groups + ',{"code":"'+group.code.to_s+'","group":"'+group.name.to_s+'","active":"'+active.to_s+'","inactive":"'+inactive.to_s+'","total":"'+total.to_s+'"}'
			end
		end
		groups = groups + '], "total":'+counter.to_s+'}'

		render :json => groups
	end

	# Complemento para llenar la tabla de estudiantes
	def current_students
		students = '{"students":['
		students_now = AuthUser.joins(:latamusers_students).where("teacher_id = ?", current_auth_user.id).order("first_name")
		counter = 0
		students_now.each do |student|

			assigned_activities_to_student = LatamusersUseractivity.all.joins(:latamusers_assignedactivity).where("user_id = ? AND assigned = true AND active = true", student.id).order('"latamusers_assignedactivity"."start_date" DESC')

	        totalScore = 0 
	        totalPossibleScore = 0 

	        assigned_activities_to_student.each do |activity_assinged| 
	           	totalScore = totalScore + activity_assinged.score 
	        	totalPossibleScore = totalPossibleScore + activity_assinged.possibleScore 
	        end 

			counter = counter + 1
			if counter == 1
				students = students + '{"id":"'+student.id.to_s+'","group":"'+student.latamusers_students.first.latamusers_usergroup.name.to_s+
				'","first_name":"'+student.first_name.to_s+'","last_name":"'+student.last_name.to_s+
				'","total_score":"'+totalScore.to_s+'/'+totalPossibleScore.to_s+'"}'	
			else
				students = students + ',{"id":"'+student.id.to_s+'","group":"'+student.latamusers_students.first.latamusers_usergroup.name.to_s+
				'","first_name":"'+student.first_name.to_s+'","last_name":"'+student.last_name.to_s+
				'","total_score":"'+totalScore.to_s+'/'+totalPossibleScore.to_s+'"}'
			end
		end
		students = students + '], "total":'+counter.to_s+'}'

		render :json => students
	end

	# Complemento para llenar la tabla de estudiantes
	def current_students_in_activity
		students = '{"students":['
		user_activities = LatamusersUseractivity.joins(:latamusers_students).select(:user_id).where("assigned_activity_id = ? AND teacher_id = ?", params[:id], current_auth_user.id).group(:user_id)
		counter = 0
		user_activities.each do |us_act|
			counter = counter + 1
			student = us_act.auth_user
			if counter == 1
				students = students + '{"id":"'+student.id.to_s+'","group":"'+student.latamusers_students.first.latamusers_usergroup.name.to_s+
				'","first_name":"'+student.first_name.to_s+'","last_name":"'+student.last_name.to_s+'","username":"'+student.username+'"}'	
			else
				students = students + ',{"id":"'+student.id.to_s+'","group":"'+student.latamusers_students.first.latamusers_usergroup.name.to_s+
				'","first_name":"'+student.first_name.to_s+'","last_name":"'+student.last_name.to_s+'","username":"'+student.username+'"}'	
			end
		end
		students = students + '], "total":'+counter.to_s+'}'

		render :json => students
	end

	# Complemento para llenar la tabla de estudiantes
	def current_students_in_sequence
		students = '{"students":['
		user_activities = SequenceUser.joins(:latamusers_students).select(:auth_user_id).where("assigned_sequence_id = ? AND teacher_id = ?", params[:id], current_auth_user.id).group(:auth_user_id)
		counter = 0

		@sequence = SequenceUser.find_by_assigned_sequence_id(params[:id]).sequence

		user_activities.each do |us_act|
			counter = counter + 1
			student = us_act.auth_user
			status = ""

			sequence_active = @sequence

			totalScoreMedia = 0 
			totalPossibleScoreMedia = 0
			totalScoreVoc = 0 
			totalPossibleScoreVoc = 0
			totalScoreRead = 0 
			totalPossibleScoreRead = 0  

			sequence_active.lms_activities.each do |activity|
				totalScoreMedia = totalScoreMedia + activity.latamusers_userquestions.where('"latamusers_userquestion"."user_id" = ? AND "LMS_question"."category_id" = 3', student.id).group("id").count.length
				totalScoreVoc = totalScoreVoc + activity.latamusers_userquestions.where('"latamusers_userquestion"."user_id" = ? AND "LMS_question"."category_id" = 2', student.id).group("id").count.length
				totalScoreRead = totalScoreRead + activity.latamusers_userquestions.where('"latamusers_userquestion"."user_id" = ? AND "LMS_question"."category_id" = 1', student.id).group("id").count.length

				totalPossibleScoreMedia = totalPossibleScoreMedia + activity.latamusers_userquestions.where('"latamusers_userquestion"."user_id" = ? AND "LMS_question"."category_id" = 3 AND "LMS_activity"."id" = ? AND "latamusers_userquestion"."passed" = true', student.id,activity.id).group("id").count.length
				totalPossibleScoreVoc =  totalPossibleScoreVoc + activity.latamusers_userquestions.where('"latamusers_userquestion"."user_id" = ? AND "LMS_question"."category_id" = 2 AND "LMS_activity"."id" = ? AND "latamusers_userquestion"."passed" = true', student.id,activity.id).group("id").count.length
				totalPossibleScoreRead = totalPossibleScoreRead + activity.latamusers_userquestions.where('"latamusers_userquestion"."user_id" = ? AND "LMS_question"."category_id" = 1 AND "LMS_activity"."id" = ? AND "latamusers_userquestion"."passed" = true', student.id,activity.id).group("id").count.length
			end


			@sequence.lms_activities.each do |lms_activity|
				activity_score = student.latamusers_useractivities.where("activity_id = ?", lms_activity.id).last
				if activity_score.present?
					activity_finded = activity_score.lms_activity
					puntaje_res = (((activity_score.score.to_f/activity_score.possibleScore.to_f)*100).round(2))
					status = status + "• #{activity_finded.name} con puntaje "+puntaje_res.to_s+"% <br>"
				end
			end

			percentage_media = ((totalPossibleScoreMedia.to_f/totalScoreMedia.to_f)*100).to_f.round(2).to_s.sub("NaN","0.0")+"%"
			percentage_voc = ((totalPossibleScoreVoc.to_f/totalScoreVoc.to_f)*100).to_f.round(2).to_s.sub("NaN","0.0")+"%"
			percentage_reading = ((totalPossibleScoreRead.to_f/totalScoreRead.to_f)*100).to_f.round(2).to_s.sub("NaN","0.0")+"%"

			if counter == 1
				students = students + '{"id":"'+student.id.to_s+'","group":"'+student.latamusers_students.first.latamusers_usergroup.name.to_s+
				'","first_name":"'+student.first_name.to_s+'","last_name":"'+student.last_name.to_s+'","username":"'+student.username+
				'","media":"'+percentage_media+'","reading":"'+percentage_reading+'","vocabulary":"'+percentage_voc+'","status":"'+if status.present? then status else "The student have not presented any activity" end+'"}'
			else
				students = students + ',{"id":"'+student.id.to_s+'","group":"'+student.latamusers_students.first.latamusers_usergroup.name.to_s+
				'","first_name":"'+student.first_name.to_s+'","last_name":"'+student.last_name.to_s+'","username":"'+student.username+
				'","media":"'+percentage_media+'","reading":"'+percentage_reading+'","vocabulary":"'+percentage_voc+'","status":"'+if status.present? then status else "The student have not presented any activity" end+'"}'
			end
		end
		students = students + '], "total":'+counter.to_s+'}'

		render :json => students
	end

	# Método para poder dejar el preview de la secuencia
	def preview_sequence
		@sequence = Sequence.find(params[:id])
	end

	# Complemento para llenar la tabla de estudiantes
	def current_students_in_group
		students = '{"students":['

		code = params[:code]

		students_now = AuthUser.joins(:latamusers_students).where("group_id = ?", code).order("first_name")

		counter = 0
		students_now.each do |student|	
			assigned_activities_to_student = LatamusersUseractivity.all.joins(:latamusers_assignedactivity).where("user_id = ? AND assigned = true AND active = true", student.id).order('"latamusers_assignedactivity"."start_date" DESC')

			assigned_activities_media = LatamusersUseractivity.joins(:latamusers_assignedactivity, :lms_activity, :lms_questions).where("user_id = ? AND assigned = true AND active = true AND category_id = 3", student.id).group("id")
			assigned_activities_vocabulary = LatamusersUseractivity.joins(:latamusers_assignedactivity, :lms_activity, :lms_questions).where("user_id = ? AND assigned = true AND active = true AND category_id = 2", student.id).group("id")
			assigned_activities_reading = LatamusersUseractivity.joins(:latamusers_assignedactivity, :lms_activity, :lms_questions).where("user_id = ? AND assigned = true AND active = true AND category_id = 1", student.id).group("id")

			# Para el puntaje global
	        totalScore = 0 
	        totalPossibleScore = 0 

	        assigned_activities_to_student.each do |activity_assinged| 
	           	totalScore = totalScore + activity_assinged.score 
	        	totalPossibleScore = totalPossibleScore + activity_assinged.possibleScore 
	        end

	        # Para el puntaje de media
	        totalScoreMedia = 0 
	        totalPossibleScoreMedia = 0 

	        assigned_activities_media.each do |activity_assinged| 
	           	totalScoreMedia = totalScoreMedia + activity_assinged.score 
	        	totalPossibleScoreMedia = totalPossibleScoreMedia + activity_assinged.possibleScore 
	        end

	        # Para el puntaje de vocabulary
	        totalScoreVoc = 0 
	        totalPossibleScoreVoc = 0 

	        assigned_activities_vocabulary.each do |activity_assinged| 
	           	totalScoreVoc = totalScoreVoc + activity_assinged.score 
	        	totalPossibleScoreVoc = totalPossibleScoreVoc + activity_assinged.possibleScore 
	        end

	        # Para el puntaje de reading
	        totalScoreRead = 0 
	        totalPossibleScoreRead = 0 

	        assigned_activities_reading.each do |activity_assinged| 
	           	totalScoreRead = totalScoreRead + activity_assinged.score 
	        	totalPossibleScoreRead = totalPossibleScoreRead + activity_assinged.possibleScore 
	        end

			counter = counter + 1
			if counter == 1
				students = students + '{"id":"'+student.id.to_s+'","first_name":"'+student.first_name.to_s+
				'","last_name":"'+student.last_name.to_s+'","total_score":"'+totalScore.to_s+'/'+totalPossibleScore.to_s+
				'","media":"'+('%.2f' % ((totalScoreMedia.to_f/totalPossibleScoreMedia.to_f)*100.0)).to_s+
				'","vocabulary":"'+('%.2f' % ((totalScoreVoc.to_f/totalPossibleScoreVoc.to_f)*100.0)).to_s+
				'","reading":"'+('%.2f' % ((totalScoreRead.to_f/totalPossibleScoreRead.to_f)*100.0)).to_s+'"}'	
			else
				students = students + ',{"id":"'+student.id.to_s+'","first_name":"'+student.first_name.to_s+
				'","last_name":"'+student.last_name.to_s+'","total_score":"'+totalScore.to_s+'/'+totalPossibleScore.to_s+
				'","media":"'+('%.2f' % ((totalScoreMedia.to_f/totalPossibleScoreMedia.to_f)*100.0)).to_s+
				'","vocabulary":"'+('%.2f' % ((totalScoreVoc.to_f/totalPossibleScoreVoc.to_f)*100.0)).to_s+
				'","reading":"'+('%.2f' % ((totalScoreRead.to_f/totalPossibleScoreRead.to_f)*100.0)).to_s+'"}'
			end
		end
		students = students + '], "total":'+counter.to_s+'}'

		render :json => students
	end

	# Complemento para llenar la tabla de estudiantes
	def current_activities_in_student
		activities = '{"activities":['
		assigned_activities_to_student = LatamusersUseractivity.all.joins(:latamusers_assignedactivity).where("user_id = ? AND assigned = true AND active = true", params[:id]).order("start_date DESC")
		counter = 0
		assigned_activities_to_student.each do |assign_activity|

			questions = assign_activity.lms_activity.lms_questions

			reading_counter = 0
			vocabulary_counter = 0
			media_counter = 0

			reading_counter_s = 0
			vocabulary_counter_s = 0
			media_counter_s = 0

			questions.each do |question|

				user_question = question.latamusers_userquestions.where("(user_id = ?) AND (date BETWEEN ? AND ?)", params[:id], assign_activity.latamusers_assignedactivity.start_date, assign_activity.latamusers_assignedactivity.end_date).last
	
				if question.category_id == 1
					reading_counter = reading_counter + 1
					if user_question.present? && user_question.passed == true
						reading_counter_s = reading_counter_s + 1
					end
				elsif question.category_id == 2
					vocabulary_counter = vocabulary_counter + 1
					if user_question.present? && user_question.passed == true
						vocabulary_counter_s = vocabulary_counter_s + 1
					end
				elsif question.category_id == 3
					media_counter = media_counter + 1
					if user_question.present? && user_question.passed == true
						media_counter_s = media_counter_s + 1
					end
				end

			end

			totalScore = 0
			if assign_activity.score == reading_counter_s + vocabulary_counter_s + media_counter_s
				totalScore = assign_activity.score
			else
				totalScore = reading_counter_s + vocabulary_counter_s + media_counter_s
			end

			counter = counter + 1
			if counter == 1
				activities = activities + '{"activity":"'+assign_activity.lms_activity.name.to_s+'","start_date":"'+assign_activity.latamusers_assignedactivity.start_date.strftime("%b. %d, %Y").to_s+
				'","end_date":"'+assign_activity.latamusers_assignedactivity.end_date.strftime("%b. %d, %Y").to_s+'","maximum_tries":"'+assign_activity.latamusers_assignedactivity.maximum_tries.to_s+
				'","current_try":"'+assign_activity.current_try.to_s+'","score":"'+totalScore.to_s+'/'+assign_activity.possibleScore.to_s+'","percentage":"'+('%.2f' % ((totalScore.to_f/assign_activity.possibleScore.to_f)*100.0)).to_s+
				'","reading":"'+ reading_counter_s.to_s + '/' + reading_counter.to_s +
				'","vocabulary":"'+ vocabulary_counter_s.to_s + '/' + vocabulary_counter.to_s +
				'","media":"'+ media_counter_s.to_s + '/' + media_counter.to_s +
				'","successfull":"'+ assign_activity.successfull.to_s+'","activity_id":"'+assign_activity.activity_id.to_s+'"}'	
			else
				activities = activities + ',{"activity":"'+assign_activity.lms_activity.name.to_s+'","start_date":"'+assign_activity.latamusers_assignedactivity.start_date.strftime("%b. %d, %Y").to_s+
				'","end_date":"'+assign_activity.latamusers_assignedactivity.end_date.strftime("%b. %d, %Y").to_s+'","maximum_tries":"'+assign_activity.latamusers_assignedactivity.maximum_tries.to_s+
				'","current_try":"'+assign_activity.current_try.to_s+'","score":"'+totalScore.to_s+'/'+assign_activity.possibleScore.to_s+'","percentage":"'+('%.2f' % ((totalScore.to_f/assign_activity.possibleScore.to_f)*100.0)).to_s+
				'","reading":"'+ reading_counter_s.to_s + '/' + reading_counter.to_s +
				'","vocabulary":"'+ vocabulary_counter_s.to_s + '/' + vocabulary_counter.to_s +
				'","media":"'+ media_counter_s.to_s + '/' + media_counter.to_s +
				'","successfull":"'+ assign_activity.successfull.to_s+'","activity_id":"'+assign_activity.activity_id.to_s+'"}'
			end

		end
		activities = activities + '], "total":'+counter.to_s+'}'

		render :json => activities
	end

	def get_students_html
		students = AuthUser.joins(:latamusers_students).where("group_id = ? AND teacher_id = ?", params[:code], current_auth_user.id).order("first_name")
		data = '<div id="students_in_group">
					<ul>'

		students.each do |student|
			data = data + 
			'<li>
            <label for="'+student.id.to_s+'" class="checkbox-inline" >
                <input value="'+student.id.to_s+'" type="checkbox" name="students-checkboxes[]" id="'+student.id.to_s+'" class="students-check">'+student.first_name + ' ' +student.last_name+'</input>
            </label>
            </li>'
		end

		data = data + '</ul>
    			</div>'

		render :html => data.html_safe
	end

	def get_activities_html

		date_start = params[:start_date]
		date_end = params[:end_date]
		active = params[:active]
		tries = params[:tries]

		levels = LmsLevel.all.order(:difficulty_level)
		sections = NewsSection.joins(:django_sites).where('domain = ?', 'elpost.co')
		activities = LmsActivity.joins(:lms_level, :news_article_sections).where("difficulty_id = ? AND section_id = ?", params[:id_level], params[:id_section])

		data = '<form action="/create_assign_activity" method="post">
    			<input type="hidden" name="csrfmiddlewaretoken" value="PuwofTfbd52FulJaPrscWtdb1D0g8c7V" />

				<div class="row">
        			<div class="form-group">
            			<label class="col-md-5">Level</label>
            			<label class="col-md-7"for="">Activity</label>
           				<div class="col-md-5">
            				<select class="form-control" id="id_level_field" name="level_field">'
		
		levels.each do |level|
			if level.id.to_s == params[:id_level]
				data = data + '<option value="'+level.id.to_s+'" selected="selected"> Level '+level.difficulty_level+'</option>'
			else
				data = data + '<option value="'+level.id.to_s+'"> Level '+level.difficulty_level+'</option>'
			end
		end
		
		data =  data + '</select>
						<br>
						<label>Section</label>'


		data = data + '<select class="form-control" id="id_section_field" name="section_field">'

		sections.each do |section|
			if section.id.to_s == params[:id_section]
				data = data + '<option value="'+section.id.to_s+'" selected="selected">'+section.title+'</option>'
			else
				data = data + '<option value="'+section.id.to_s+'">'+section.title+'</option>'
			end
		end

        data = data + '</select>
        			</div>
            	<div class="col-md-7">
            	<select class="form-control" id="id_activity" name="activity">'

        activities.each do |activity|
        	data = data + '<option value="'+activity.id.to_s+'">'+activity.name+'</option>'
        end

        data = data + '</select>
			            </div>     
			        </div>        

			        <div class="form-group">
			            <label for="">Maximum tries</label>
			            <input class="form-control" id="id_maximum_tries" min="0" name="maximum_tries" type="number" '+if tries != "none" then 'value = "'+tries+'"' else '' end+' required="required"/>  
			        </div>
			    </div>
			   
			    <div class="row">
			        <div class="form-group">
			            <label for="">Start date:</label>
			            <input class="form-control" id="id_start_date" name="start_date" type="date" required="required"'+if date_start != "0" then 'value = "'+date_start+'"' else '' end+'/> 
			        </div>        

			        <div class="form-group">
			            <label for="">End date:</label>
			            <input class="form-control" id="id_end_date" name="end_date" type="date" required="required"'+if date_end != "0" then 'value = "'+date_end+'"' else '' end+'/>
			        </div>
			    </div>
			    
			    <button type="submit" class="btn-post" name="create_activity">Save Activity</button>
			</form>'

		render :html => data.html_safe
	end

	def assigned_activities
		
	end

	def edit_activity
		@levels = LmsLevel.all.order(:difficulty_level)
		@sections = NewsSection.joins(:django_sites).where('domain = ?', 'elpost.co')
		@assigned = LatamusersAssignedactivity.find(params[:id])
		@activities_init = LmsActivity.joins(:lms_level, :news_article_sections).where("difficulty_id = ? AND section_id = ? ", @assigned.lms_activity.difficulty_id, @assigned.lms_activity.news_article.news_sections.first.id)
	end

	def remove_activity
		LatamusersAssignedactivity.find(params[:id]).update(:active => false)
		flash[:notice] = "Eliminaste esta asignación de manera satisfactoria"
		redirect_to create_activity_path
	end

	def edit_sequence
		@assigned = LatamusersAssignedsequence.find(params[:id])
		@sequences_init = Sequence.all
	end


	def students_assignation 
		@groups = current_auth_user.latamusers_usergroups
	end

	# Crear actividades asignadas
	def create_assign_activity
		active = true
		
		assigned = LatamusersAssignedactivity.new(:activity_id => params[:activity], 
			:maximum_tries => params[:maximum_tries],
			:start_date => params[:start_date],
			:end_date => params[:end_date],
			:teacher_id => current_auth_user.id,
			:creation_date => Time.now,
			:active => active)
		
		if assigned.save!
			redirect_to create_activity_path
			flash[:success] = "Successfully created the activity"
		else
			redirect_to create_activity_path
			flash[:success] = "There was an error when creating the activity"
		end
	end

	# Crear secuencias asignadas
	def create_assign_sequence
		active = ""

		if params[:active]
			active = true
		else
			active = false
		end
		
		assigned = LatamusersAssignedsequence.new(:sequence_id => params[:sequence],
			:maximum_tries => 3,
			:start_date => params[:start_date],
			:end_date => params[:end_date],
			:teacher_id => current_auth_user.id,
			:creation_date => Time.now,
			:active => true)
		
		if assigned.save!
			redirect_to create_sequence_path
			flash[:success] = "Successfully created the sequence"
		else
			redirect_to create_sequence_path
			flash[:success] = "There was an error when creating the sequence"
		end
	end

	# Eliminar secuencia de manera lógica
	def remove_secuence
		LatamusersAssignedsequence.find(params[:id]).update(:active => false)
		flash[:notice] = "Eliminaste esta asignación de manera satisfactoria"
		redirect_to create_sequence_path
	end

	# Crear asignación de de secuencias
	def assign_sequence

		students_ids = params["students-checkboxes"]
		sequences_ids = params["sequences-checkboxes"]

		group_id = params["group-select"]
		counter = 0
		mensaje = ""

        if sequences_ids && students_ids

            students_ids.each do |sc|
                sequences_ids.each do |ac|
                	student = AuthUser.find(sc)
                    assigned_sequence = LatamusersAssignedsequence.find(ac)


                	if SequenceUser.where("sequence_id = ? AND auth_user_id = ? AND assigned_sequence_id = ?", assigned_sequence.sequence.id, student.id, assigned_sequence.id).count > 0
     					user = AuthUser.find(student.id)
     					sequence = Sequence.find(assigned_sequence.sequence.id)
     					mensaje = mensaje + "The student " + user.first_name + ' ' + user.last_name + " could not be assigned to '" + sequence.name + "' </br>"
     					next
   					end

                    user_activity = SequenceUser.new(:auth_user_id => student.id,
                    	:sequence_id => assigned_sequence.sequence.id,
                    	:assigned => true,
                    	:assigned_sequence_id => assigned_sequence.id,
                    	:score => 0,
                    	:date => Time.now,
                    	:successfull => false,
                    	:current_try => 0)

                   	if user_activity.save!
                		counter = counter + 1
                	end
                end
            end

			if counter == students_ids.count * sequences_ids.count
            	flash[:success] = "The sequences were assigned successfully"
            else
            	flash[:danger] = "There are some repeated sequences for students that could not be added. </br> " + mensaje
            end

        elsif !(sequences_ids)
            flash[:success] = "You didn't choose sequences to assign"
        elsif !(students_ids)
            flash[:success] = "You didn't choose students to assign the sequences"
        end

        redirect_to create_sequence_path

	end

	# Editar actividad asignada 
	def edit_activity_post
		active = true

		assigned = LatamusersAssignedactivity.find(params[:pk])
		
		if assigned.update(:activity_id => params[:activity], 
			:maximum_tries => params[:maximum_tries],
			:start_date => params[:start_date],
			:end_date => params[:end_date],
			:active => active)

			redirect_to('/create-activity')
			flash[:success] = "Successfully updated the activity"
		else
			redirect_to edit_activity_path(assigned.id)
			flash[:success] = "There was an error updating the activity"
		end
	end

	# Editar secuencia asignada 
	def edit_sequence_post
		active = ""

		assigned = LatamusersAssignedsequence.find(params[:pk])
		
		if assigned.update(:sequence_id => params[:sequence], 
			:start_date => params[:start_date],
			:end_date => params[:end_date])

			redirect_to('/create-sequence')
			flash[:success] = "Successfully updated the sequence"
		else
			redirect_to edit_activity_path(assigned.id)
			flash[:success] = "There was an error updating the sequence"
		end
	end

	def all_assigned_activities
		activities_to_active = LatamusersAssignedactivity.where("teacher_id = ?", current_auth_user.id)
		
		counter = 0
		activities_to_active.each do |assign|
			if assign.update(:active => true)
				counter = counter + 1
			end
		end

		if counter == activities_to_active.count
			redirect_to create_activity_path
			flash[:success] = "Successfully updated all activities to active"
		else
			redirect_to create_activity_path
			flash[:success] = "Some activities can't be updated"
		end
	end

	def assign_activity

		students_ids = params["students-checkboxes"]
		activities_ids = params["activities-checkboxes"]
		group_id = params["group-select"]
		counter = 0
		mensaje = ""

        if activities_ids && students_ids

            students_ids.each do |sc|
                activities_ids.each do |ac|
                	student = AuthUser.find(sc)
                    assigned_activity = LatamusersAssignedactivity.find(ac)


                	if LatamusersUseractivity.where("activity_id = ? AND user_id = ? AND assigned_activity_id = ?", assigned_activity.lms_activity.id, student.id, assigned_activity.id).count > 0
     					user = AuthUser.find(student.id)
     					activity = LmsActivity.find(assigned_activity.lms_activity.id)
     					mensaje = mensaje + "The student " + user.first_name + ' ' + user.last_name + " couldn't be assigned to \"" + activity.name + "\"<br>"
     					next
   					end

                    user_activity = LatamusersUseractivity.new(:user_id => student.id,
                    	:activity_id => assigned_activity.lms_activity.id,
                    	:possibleScore => assigned_activity.lms_activity.lms_questions.count,
                    	:assigned => true,
                    	:assigned_activity_id => assigned_activity.id,
                    	:score => 0,
                    	:date => Time.now,
                    	:successfull => false,
                    	:current_try => 0)

                   	if user_activity.save!
                		counter = counter + 1
                	end
                end
            end

			if counter == students_ids.count * activities_ids.count
            	flash[:success] = "The activities were assigned successfully"
            else
            	flash[:danger] = "There are some repeated activities for students that couldn't be added.<br>" + mensaje
            end

        elsif !(activities_ids)
            flash[:success] = "You didn't choose activities to assign"
        elsif !(students_ids)
            flash[:success] = "You didn't choose students to assign the activities"
        end

        redirect_to create_activity_path

	end

	def create_student_group
		students_checkboxes = params['students-checkboxes']
        group_select = params['group-select']
        counter = 0

        if students_checkboxes
	        group = LatamusersUsergroup.find(group_select)
	        students_checkboxes.each do |sc|
	            student = AuthUser.find(sc)
	            if student.latamusers_students.first.update(:group_id => group_select)
		        	counter = counter + 1
		        end
	        end

	        if counter == students_checkboxes.count
	        	flash[:success] = "The students have been successfully assigned to the selected group"
	        else
	        	flash[:success] = "The students couldn't be updated to the selected group"
	        end
	    else
	    	flash[:success] = "You didn't choose students for the groups"
	    end

	    redirect_to students_assignation_path

	end

	def students_in_group
		@name_group = LatamusersUsergroup.find_by_code(params[:code]).name
	end

	def students_with_activity
	end

	def students_with_sequence
		sequence = SequenceUser.find_by_assigned_sequence_id(params[:id])
		@sequence = nil
		if sequence.present?
			@sequence = sequence.sequence
		end
	end

	def student_activities

		@student = AuthUser.find(params[:id])

		@user_activity = LatamusersUseractivity.all.where("user_id = ? AND assigned = false", @student.id)
    	@categories = LmsCategory.all
    	@assigned_activities = LatamusersUseractivity.all.joins(:latamusers_assignedactivity).where("user_id = ? AND assigned = true AND active = true", @student.id)
    	
    	@arreglo_general = general_year_scores(@student, "all")
		@arreglo_category_reading = general_by_category_scores(@student, 1, "all")
		@arreglo_category_vocabulary = general_by_category_scores(@student, 2, "all")
		@arreglo_category_media = general_by_category_scores(@student, 3, "all")

		if params["stats-range"] 
			if params["stats-range"] == "mes"
				if params["assigned"]
					if params["assigned"] == "true" 
						@arreglo_general = general_month_scores(@student, true)
						@arreglo_category_reading = month_by_category_scores(@student, 1, true)
						@arreglo_category_vocabulary = month_by_category_scores(@student, 2, true)
						@arreglo_category_media = month_by_category_scores(@student, 3, true)
					elsif params["assigned"] == "false"
						@arreglo_general = general_month_scores(@student, false)
						@arreglo_category_reading = month_by_category_scores(@student, 1, false)
						@arreglo_category_vocabulary = month_by_category_scores(@student, 2, false)
						@arreglo_category_media = month_by_category_scores(@student, 3, false)
					elsif params["assigned"] == "all"
						@arreglo_general = general_month_scores(@student, "all")
						@arreglo_category_reading = month_by_category_scores(@student, 1, "all")
						@arreglo_category_vocabulary = month_by_category_scores(@student, 2, "all")
						@arreglo_category_media = month_by_category_scores(@student, 3, "all")
					end
				end
			elsif params["stats-range"] == "semana"
				if params["assigned"]
					if params["assigned"] == "true"
						@arreglo_general = general_week_scores(@student,true)
						@arreglo_category_reading = week_by_category_scores(@student, 1, true)
						@arreglo_category_vocabulary = week_by_category_scores(@student, 2, true)
						@arreglo_category_media = week_by_category_scores(@student, 3, true)
					elsif params["assigned"] == "false"
						@arreglo_general = general_week_scores(@student, false)
						@arreglo_category_reading = week_by_category_scores(@student, 1, false)
						@arreglo_category_vocabulary = week_by_category_scores(@student, 2, false)
						@arreglo_category_media = week_by_category_scores(@student, 3, false)
					elsif params["assigned"] == "all"
						@arreglo_general = general_week_scores(@student, "all")
						@arreglo_category_reading = week_by_category_scores(@student, 1, "all")
						@arreglo_category_vocabulary = week_by_category_scores(@student, 2, "all")
						@arreglo_category_media = week_by_category_scores(@student, 3, "all")
					end
				end
			elsif params["stats-range"] == "año"
				if params["assigned"] 
					if params["assigned"] == "true"
						@arreglo_general = general_year_scores(@student, true)
						@arreglo_category_reading = general_by_category_scores(@student, 1, true)
						@arreglo_category_vocabulary = general_by_category_scores(@student, 2, true)
						@arreglo_category_media = general_by_category_scores(@student, 3, true)
					elsif params["assigned"] == "false"
						@arreglo_general = general_year_scores(@student, false)
						@arreglo_category_reading = general_by_category_scores(@student, 1, false)
						@arreglo_category_vocabulary = general_by_category_scores(@student, 2, false)
						@arreglo_category_media = general_by_category_scores(@student, 3, false)
					elsif params["assigned"] == "all"
						@arreglo_general = general_year_scores(@student, "all")
						@arreglo_category_reading = general_by_category_scores(@student, 1, "all")
						@arreglo_category_vocabulary = general_by_category_scores(@student, 2, "all")
						@arreglo_category_media = general_by_category_scores(@student, 3, "all")
					end
				end
			end
		end

	end

	def open_domain_questions
	end

	#Función para listar las preguntas abiertas en el post
	def open_questions_api
		openquestions = '{"openquestions":['
		current_groups = current_auth_user.latamusers_usergroups
		counter = 0

		current_groups.each do |group|
			group.latamusers_students.each do |latam_student|
				student = latam_student.auth_user_student
				questions = student.open_questions
				questions.each do |question|
				counter = counter + 1
					if counter == 1
						openquestions = openquestions + '{"student":"'+student.first_name.to_s+' '+student.last_name.to_s+'","question":"'+question.lms_question.text.gsub('"','\"').gsub("\n","").gsub("\r","")+'","answer":"'+question.content.gsub('"','\"').gsub("\n","").gsub("\r","")+'","correction":"'+question.content_errors.gsub("\"","'").gsub("\n","").gsub("\r","")+'","id":"'+question.id.to_s+'"}'
					else
						openquestions = openquestions + ',{"student":"'+student.first_name.to_s+' '+student.last_name.to_s+'","question":"'+question.lms_question.text.gsub('"','\"').gsub("\n","").gsub("\r","")+'","answer":"'+question.content.gsub('"','\"').gsub("\n","").gsub("\r","")+'","correction":"'+question.content_errors.gsub("\"","'").gsub("\n","").gsub("\r","")+'","id":"'+question.id.to_s+'"}'
					end
				end
			end
		end
		puts "questions: ", openquestions

		openquestions = openquestions + '], "total":'+counter.to_s+'}'
		render :json => openquestions
	end

	def open_questions_show
		@open_question = OpenQuestion.find(params[:id])
		@comments = OpenQuestion.find(params[:id]).review_open_questions.page(params[:page]).per(6)
		@comment = OpenQuestion.find(params[:id]).review_open_questions.new
	end

	def open_question_create
		@open_question = OpenQuestion.find(params[:id_question])
		@preview = @open_question.review_open_questions.new(review_params)
		@preview.student_id = params[:id_student]
		@preview.teacher_id = params[:id_teacher]
		if @preview.save
			UserMailer.review_open_question(current_auth_user, AuthUser.find(params[:id_student]), @open_question, @preview).deliver_now
			flash[:success] = "El comentario fue creado exitosamente, se ha enviado un correo al estudiante como notificación."
		else
			flash[:error] = "Hubo un error creando el comentario, inténtalo de nuevo"
		end
		redirect_back(fallback_location: request.referer)
	end

	def elearning
		@categories = LmsCategory.all
		activities = LmsActivity.all
		@example_activity = activities[rand(0..(activities.count-1))]
		questions = @example_activity.lms_questions
		@example_question = questions[rand(0..(questions.count-1))]
		@best_scores = AuthUser.group("first_name || ' ' || last_name").joins(:latamusers_useractivities).order('sum_score DESC').where("assigned = false").sum(:score).first(5)
	end

	def my_subscription
		@date_end = current_auth_user.latamusers_userprofiles.first.date_end
	end

	def edit_password
    	@user = current_auth_user
  	end

  	def update_password
    	@user = AuthUser.find(current_auth_user.id)
    	@admin = Administrator.where("auth_user_id = ?", current_auth_user.id).first
    	if @user.update_with_password(user_params)
      		bypass_sign_in(@user)
      		flash[:success] = "The password was updated successfully"
      		if @admin.present?
      			@admin.update(:password => params[:auth_user][:password])
      		end
      		redirect_to profile_path
    	else
      		render "edit_password"
    	end
  	end

  	def send_contact_mail
    	UserMailer.more_info_email(params[:txtNombre], params[:txtApellido], params[:txtEmail], params[:txtTelefono], params[:txtPais], params[:txtCiudad], params[:chkAcepto]).deliver_now!
	end

  	def edit_password_admin
  		@disable_nav = true
		@disable_foot = true
    	@user = AuthUser.find(params[:id])
  	end

  	def update_password_admin
  		@user = AuthUser.find(params[:id])
  		@admin = Administrator.where("auth_user_id = ?", params[:id]).first

    	if @user.update(user_params_admin)
      		flash[:success] = "The password was updated successfully"
			if @admin.present?
				@admin.update(:password => params[:auth_user][:password])
			end
      		render :html => "<p> Successfully updated the password </p>".html_safe
    	else
      		render :html => ("<p> Something wrong, try again </p>" + @user.errors.first.to_s).html_safe
    	end
  	end

  	# Funciones para importar un usuario desde un csv 
  	def import
  		@disable_nav = true
		@disable_foot = true
  	end

	def import_users
		command = AuthUser.import(params[:file])
		if command[0]
			redirect_to import_users_path, notice: "All users were added successfully"
		else
			redirect_to import_users_path, notice: command[1]
		end
	end

	# Funciones para generar audio a partir de un texto
	def generate_audio_layout
		@disable_nav = true
		@disable_foot = true
	end

	def generate_audio
		
		title = params[:title]
		text = params[:text]
		volume = params[:volume]
		speed = params[:speed]
		voice = params[:voice]

		path = ""

		if Rails.env == "production"
			path = "/var/app/current/public/uploads/tmp"
		else
			path = Rails.root.to_s+"/public/uploads/tmp"
		end

		delete(path)

		array_of_words = separate_text(1500, text)

		counter = 0
		array_of_words.each do |part|
			audio = generate_audios(part, path, counter, volume, speed, voice)
			counter = counter + 1
		end

		merge_audios(path,title)

		send_file "#{path}/#{title}.mp3"

	end

	def generate_audios(text, path, number, volume, speed, voice)

		polly = Aws::Polly::Client.new(
			access_key_id: 'AKIAJ5GZRCSU2B4K3RFQ',
			secret_access_key: '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',
			region: 'us-west-2'
		)

		ssml = "<speak><prosody volume=\"#{volume}\" rate=\"#{speed}\">#{text}</prosody></speak>"
		resp = polly.synthesize_speech({
			response_target: "#{path}/tmp_#{number}.mp3",
			output_format: "mp3",
			sample_rate: "16000",
			text: ssml.gsub("&","and"),
			text_type: "ssml",
			voice_id: voice
		})

	end

	def delete(path)
		Dir["#{path}/*.mp3"].each do |file|
			FileUtils.rm_rf file
		end

		Dir["#{path}/*.raw"].each do |file|
			FileUtils.rm_rf file
		end

		Dir["#{path}/*.wav"].each do |file|
			FileUtils.rm_rf file
		end
	end

	def merge_audios(path, title)
		File.open(path + '/' + title + ".mp3",'a') do |mergedFile|
		    filesInDir = Dir["#{path}/tmp_*.mp3"]
		    filesInDir.each do |file|
		        text = File.open(file, 'r').read
		        text.each_line do |line|
		          	mergedFile << line
		        end
		    end
		end
	end

	def separate_text(number, all_text)
		combined_texts = []
		tmp_string = ""
		all_text.chars.each do |text|
			if tmp_string.size + text.size > number
				combined_texts << tmp_string
				tmp_string = text
			else
				tmp_string << "#{text}"
			end
		end
		combined_texts << tmp_string

		return combined_texts
	end

	# Método para guardar el audio en el servidor
	def save_audio

		path = ""
		filename = "audio#{current_auth_user.present? ? current_auth_user.id : "preview"}.wav"
		filename_raw = "audio#{current_auth_user.present? ? current_auth_user.id : "preview"}.raw"
		
		if Rails.env == "production"
			path = "/var/app/current/public/uploads/tmp"
		else
			path = Rails.root.to_s+"/public/uploads/tmp"
		end

		path_key = ""
		if Rails.env == "production"
			path_key = "/var/app/current/translate-b547359e10c1.json"
		else
			path_key = Rails.root.to_s+"/latammedia/translate-b547359e10c1.json"
		end

		data = params[:audio]
		# puts "path generated: ", path+"/"+filename
		File.open(path+"/"+filename, 'wb') do |f|
			f.write data.read
		end

		require "google/cloud/speech"
		speech = Google::Cloud::Speech.new project: 'translate-192514',
										keyfile: path_key

		op = nil
		begin

			`ffmpeg -i #{path}/#{filename} -f s16le -acodec pcm_s16le #{path}/#{filename_raw}`

			audio = speech.audio "#{path}/#{filename_raw}",
				encoding: :linear16,
				language: "en-US",
				sample_rate: 48000

			op = audio.process(:words => true)
			while !op.done?
				op.reload!
			end

			delete(path)

		rescue => ex
			puts "Error analizando el audio " + ex.to_json
		end
		
		render json: (op.present?) ? op.results : op
	end
	
	def correct_answer
		text = params[:text]
		AfterTheDeadline.set_language('en')
		AfterTheDeadline(nil, ['Passive voice', 'Hidden Verbs', 'Complex Expression'])
		response = AfterTheDeadline.check(text)
		render json: response.to_json
	end

	# Método para transformar voz a texto
	# def transcribe(filename, url)
	#	transcribe = Aws::TranscribeService::Client.new(
	#		access_key_id: 'AKIAJ5GZRCSU2B4K3RFQ',
	#		secret_access_key: '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',
	#		region: 'us-east-1'
	#	)

	#	random_name = rand(10000..1000000)
    #	resp = transcribe.start_transcription_job({
	#	  transcription_job_name: "Transcription#{random_name}",
	#	  language_code: "en-US",
	#	  media_sample_rate_hertz: 48000,
	#	  media_format: "mp3",
	#	  media: {
	#	    media_file_uri: url
	#	  },
	#	})

	#	while (resp.transcription_job.transcription_job_status == "IN_PROGRESS")
	#		client = Aws::TranscribeService::Client.new(
	#			access_key_id: 'AKIAJ5GZRCSU2B4K3RFQ',
	#			secret_access_key: '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',
	#			region: 'us-east-1')

	#		resp = client.get_transcription_job({
	#			transcription_job_name: "Transcription#{random_name}",
	#		})

	#		puts resp.transcription_job.transcription_job_status
	#		sleep 1
	#	end

	#	uri = URI(resp.transcription_job.transcript.transcript_file_uri)
	#	request = Net::HTTP::Get.new(uri)
	#	response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
	#		http.request(request)
	#	end

	#	return response.body
	#end

	# Funciones para ejecutar el refresh de las funciones
	def update_lms_words
		words = LmsWord.order(word: :asc)
		json = []
		words.each do |word|
			json << {:id => word.id, :word => word.word, :use => word.lms_definitions.first.present? ? word.lms_definitions.first.use : 'not present'}
		end
		render json: json
	end

	def update_articles
		articles = NewsArticle.select(:id, :title).order(id: :desc)
		render json: articles
	end

	def update_media
		media_param = params[:media]
		media = MediaMedium.select(:id, :title).where('type in(?)', media_param).order(id: :desc)
		render json: media
	end

	# Temporal para mostrar las estadísticas
	def admin_statistics
		sign_in(AuthUser.find_by_username("Oblanco"))
		redirect_to reports_path
	end

	def view_as_user
		sign_in(AuthUser.find_by_username(params[:username]))
		redirect_to profile_path
	end

  	private

		def user_params
			params.require(:auth_user).permit(:current_password, :password, :password_confirmation)
		end

		def user_params_admin
			params.require(:auth_user).permit(:password, :password_confirmation)
		end

		def review_params
			params.require(:review_open_question).permit(:text)
		end

		def validate_caracterization
			if auth_user_signed_in?
				if current_auth_user.sign_in_count == 1
					current_auth_user.update(:sign_in_count => 2)
		            flash[:success] = "It's the first time you signed in, please complete your profile information"
		            redirect_to edit_auth_user_registration_path
				elsif current_auth_user.caracterization_citemclientrelations.count == 0
					flash[:danger] = "You have not completed all your caracterization, please let us know more about you"
					redirect_to caracterization_profile_path
				end
			end
		end

end