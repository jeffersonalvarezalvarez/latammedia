class AuthUsers::RegistrationsController < Devise::RegistrationsController

  def new
    super
  end

  def create
    build_resource(sign_up_params)
    resource.latamusers_userprofiles.build(:english_level => "none", 
    	:education => "none",
    	:ocupation => "none",
    	:type => "student",
    	:date_subscription => Time.now,
    	:date_end => Time.now + 6.days,
    	:subscripted => true)

    resource.last_sign_in_at =  Time.now
    resource.date_joined =  Time.now
    resource.first_name = ''
    resource.last_name = ''
    resource.skip_confirmation!
    resource.save

    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end

  private

    # Método para redirigir en caso de una actualización
    def after_update_path_for(resource)
      	profile_path
    end
    
end