function getCookie(name) {
var cookieValue = null;
if (document.cookie && document.cookie != '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) == (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            break;
        }
    }
}
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function showPlay() {
    // show or hide de play fast phrases
    if($("#play").is(":visible")){
      $("#buttom_juego").html('Show');
      $("#play").hide();  
    }else{
      $("#buttom_juego").html('Hide');
      $("#play").show();
    }
}
function showLevel(level){
  activity = document.getElementsByClassName("activityList");
  for(i = 0; i<activity.length;i++){
    if(level == activity[i].attributes.name.value || level == -1){
      activity[i].style.display = "block";
    }else{
      activity[i].style.display = "none";
    }
  }
}
function showSection(link_section){
  var win = window.location.replace(link_section);
}
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$(document).on('change', '#group_selection', 
function(){
      $.ajax({
          url:'/get_group_students/'+this.value,
          type:"post",
          data:{
                group_value:$("#group_selection").val()
             },
          success:function(data){
                $("#students_in_group").html(data);  
          },
          error: function(xmlhttprequest, textstatus, errorthrown) { 
               alert("status: " + textstatus); alert("error: " + errorthrown); 
           }       
       });
});

$(document).on('change', '#group_selection2', 
function(){
      $.ajax({
          url:'.',
          type:"post",
          data:{
                group_value:$("#group_selection2").val()
             },
          success:function(data){
                $("#assigned_activities_by_teacher").html(data);  
          },
          error: function(xmlhttprequest, textstatus, errorthrown) { 
               alert("status: " + textstatus); alert("error: " + errorthrown); 
           }       
       });
});

$(document).on('change', '#id_level_field', 
function(){

      var start_date = !($("#id_start_date").val()) ? 0 : $("#id_start_date").val();
      var end_date = !($("#id_end_date").val()) ? 0 : $("#id_end_date").val();
      var active = $("#id_active").prop('checked') ? 1 : 0;
      var tries = !($("#id_maximum_tries").val()) ? 'none' : $("#id_maximum_tries").val();

      $.ajax({
          url:'/get_activities/'+this.value+'/'+$("#id_section_field").val()+'/'+start_date+'/'+end_date+'/'+active+'/'+tries,
          type:"post",
          data:{
                 change_level:$("#id_level_field").val()
             },
          success:function(data){
                $("#activity_creation_and_edition_form").html(data);  
          },
          error: function(xmlhttprequest, textstatus, errorthrown) { 
               alert("status: " + textstatus); alert("error: " + errorthrown); 
           }       
       });
});

$(document).on('change', '#id_section_field', 
function(){

      var start_date = !($("#id_start_date").val()) ? 0 : $("#id_start_date").val();
      var end_date = !($("#id_end_date").val()) ? 0 : $("#id_end_date").val();
      var active = $("#id_active").prop('checked') ? 1 : 0;
      var tries = !($("#id_maximum_tries").val()) ? 'none' : $("#id_maximum_tries").val();

      $.ajax({
          url:'/get_activities/'+ $("#id_level_field").val() +'/'+this.value+'/'+start_date+'/'+end_date+'/'+active+'/'+tries,
          type:"post",
          data:{
                 change_section:$("#id_section_field").val()
             },
          success:function(data){
                $("#activity_creation_and_edition_form").html(data);  
          },
          error: function(xmlhttprequest, textstatus, errorthrown) { 
               alert("status: " + textstatus); alert("error: " + errorthrown); 
           }       
       });
});

function selectAll(source){
       var x = document.getElementsByClassName("students-check");
       for(var i = 0; i<x.length; i++){
           x[i].checked = source.checked;
       }
}