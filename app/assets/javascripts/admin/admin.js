//= require admin/base
//= require jquery.tagsinput

$('#tag1').tagsInput();

$("#tag1_tag").on('paste',function(e){
    var element=this;
    setTimeout(function () {
        var text = $(element).val();
        var target=$("#tag1");
        var tags = (text).split(/[ ,]+/);
        for (var i = 0, z = tags.length; i<z; i++) {
              var tag = $.trim(tags[i]);
              if (!target.tagExist(tag)) {
                    target.addTag(tag);
              }
              else
              {
                  $("#tag1_tag").val('');
              }
         }
    }, 0);
});