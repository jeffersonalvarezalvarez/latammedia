var palabraNumero = 0;
var totalEjercicios = 0;
var ejercicio = [];
var siguiente = 0;
var contador = 0;
var aciertos = 0;
var palabras = [];
var ids = null;

function getExcersisesForCat(id){
    $.ajax({
	    url: "/phrases",
	    data: {"id": id},
	    success: function(data) {
			totalEjercicios = data.total;
			ids = data.ids;
			document.getElementById('types').style.display = 'none';
			document.getElementById('excersises').style.display = 'block';
			siguienteEjercicio();
	    },
	    error: function(xhr) {
	       //alert(data.length); 
	    }
	});
}

function validar(palabra)
{
	contador ++;
	document.getElementById('palabra'+palabraNumero).innerHTML=palabra.value;
	if(palabra.value == document.getElementById('palabra'+palabraNumero).getAttribute('value')	){
		++aciertos;
		document.getElementById('score').innerHTML = ""+aciertos;
		//cambiar el color de la palabra a verde para indicar que es correcta
		cambiar_color(document.getElementById('palabra'+palabraNumero),'#01DF01');
		if(document.getElementById('totalPalabras').getAttribute('value') == palabraNumero)
		{
			document.getElementById('opciones').style.display = "none";
			if(ejercicio.length < totalEjercicios){
				palabras = [];
				setTimeout(function(){siguienteEjercicio()},1000);
			}else{
	 			contador = 0;
				aciertos = 0;
				siguiente = 0;
				palabras = [];
				ids = null;
				setTimeout(function(){terminar()},1000);
			}
		}else{
			//aumenta la variable palabraNumero en uno y consulta la siguiente palabra de la frase
			setTimeout(function(){siguientePalabra(++palabraNumero)},1000);
		}
	}else{
		--aciertos;
		document.getElementById('score').innerHTML = ""+aciertos;
		cambiar_color(document.getElementById('palabra'+palabraNumero),'#DF0101');
		setTimeout(function(){marcar_incorrecto(palabra)},400);
	}
	
}

function marcar_incorrecto(palabra){
	document.getElementById('palabra'+palabraNumero).style.color = '';
	palabra.style.display = "none";
	document.getElementById('palabra'+palabraNumero).innerHTML= "...";
	var x = document.getElementsByName('opcion');
	var i;
	for (i = 0; i < x.length; i++) {
	        x[i].disabled = false;
	}
}

function pintar(palabra)
{
	document.getElementById('palabra'+palabraNumero).innerHTML=palabra;
}

function cambiar_color(palabra, color)
{
	palabra.style.color = color;
	var x = document.getElementsByName('opcion');
	var i;
	for (i = 0; i < x.length; i++) {
	        x[i].disabled = true;
	}
}

function limpiarPalabra(palabra)
{
	if(palabras.length != 0)
	document.getElementById('palabra'+palabraNumero).innerHTML="...";
}

function siguienteEjercicio()
{
	//Consulta a base de datos para traer las palabras del siguiente ejercicio.
	var correctas = [];
 	document.getElementById("imagenEjercicio").src = "";
 	 $.ajax({
	    url: "/parts",
	    data: {"id": ids[siguiente].id},
	    success: function(response) {
			ejercicio[siguiente++] = siguiente;
			document.getElementById('opciones').style.display = 'block';
		    for (var posicion in response) {
        		correctas[posicion] = {"sintaxis": response[posicion].correct};
        		palabras[posicion] = response[posicion].words;
        		palabras[posicion][response[posicion].words.length] = correctas[posicion];
        		palabras[posicion] = mezclar(palabras[posicion]);
	        }
	 		document.getElementById("imagenEjercicio").src = response[0].url;
			var html = '';
			//Pintamos los campos de las palabras que forman la frase y le asignamos el valor correcto
			for(var i = 0; i<correctas.length; i++){
		        html +=     '<div id="palabra'+i+'" value="'+correctas[i].sintaxis+'" class="label-replace">...</div>';
			}
			
			document.getElementById('coincidencias').innerHTML=html;
			document.getElementById('totalPalabras').value = palabras.length-1;
	    	palabraNumero = 0;
			siguientePalabra(palabraNumero)
		},
	    error: function(xhr) {
	       //alert(data.length); 
	    }
	});
}
/**
* Consulta la siguiente palabra de la frase y pinta los botones opcionales
*/
function siguientePalabra(numero)
{
	var html = '';
	//Pintamos los botones que hacen parte del campo que se va a comparar 
	for(var i = 0; i<palabras[numero].length; i++){
        html +=     ' <div style="padding-right: 5px; display: inline-block;">'
                        +'<input type="button" name="opcion" class="btn btn-info" value="'+palabras[numero][i].sintaxis+'" onmouseout="limpiarPalabra()" onmouseover="javascript:pintar(this.value);" onclick="javascript:validar(this);">'+
                    '</div>';
	}
	document.getElementById('opciones').innerHTML=html;
}
function terminar(){
	document.getElementById('ejercicio').style.display = "none";
	document.getElementById('felicitaciones').style.display = "block";
}

function volver_a_menu(){
	document.getElementById('score').innerHTML = ""+aciertos;
	document.getElementById('ejercicio').style.display = "block";
	document.getElementById('felicitaciones').style.display = "none";
	document.getElementById('types').style.display = 'block';
	document.getElementById('excersises').style.display = 'none';
}

/*Agrega la palabra correcta al array de palabras y las desordena*/
function mezclar(bar){
   var m= bar.length-1;
   for (var i=m;i>1;i--){ 
      alea=Math.floor(i*Math.random()); 
      temp=bar[i]; 
       bar[i]=bar[alea]; 
       bar[alea]=temp; 
    }
   return(bar);
}