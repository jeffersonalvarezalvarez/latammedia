class UserMailer < ActionMailer::Base
	default from: 'elPost <soporte@elpost.co>'
 
 	# Correo para más información
	def more_info_email(nombre, apellido, email, telefono, pais, ciudad, acepto)
		@user = [nombre, apellido, email, telefono, pais, ciudad, acepto]
		mail(to: email, subject: 'Subscripción a noticias')
	end

	# Métodos para enviar aviso de vencimiento de subcripción a usuarios
	def search_for_subscriptions
		content = '<p>¡Tu subscripción vencerá en 5 días!</p>' +
      		'<p>Recuerda que puedes renovar tu suscipción ingresando a <a href="http://elpost.co/my_subscription/">My Subscription<a>' +
      		'<p>En elPost trabajamos para ofrecerte el mejor servicio.</p>'+
      		'<p>Gracias por tu confianza.</p>' +
         	'<p>Equipo <strong>elPost</strong></p>'
         	
		AuthUser.all.each do |user|
			if user.latamusers_userprofiles.first.present?
				if user.latamusers_userprofiles.first.date_end == Date.today + 5.days
					UserMailer.subscription_to_overcome(user.email,content).deliver_now!
				end
			end
		end
	end

	def subscription_to_overcome(recipient, content)
		@content = content
		mail(to: recipient, subject: 'Tu subscripción está próxima a vencerse')
	end

	# Métodos para enviar correos de bienvenida a usuarios específicos
	def send_welcome_emails_to(users)

		content = '<p>¡Has tomado una decisión importante para tu desarrollo profesional!  Queremos acompañarte a lo largo de tu experiencia con nosotros, déjanos conocer tus inquietudes para darles respuesta inmediata escribiéndonos a <a href="mailto:suscriptores@latam.media">suscriptores@latam.media</a>.</p>' +
      		'<p>Recuerda que puedes renovar tu suscipción ingresando a <a href="http://elpost.co/my_subscription/">My Subscription<a>' +
      		'<p>En elPost trabajamos para ofrecerte el mejor servicio.</p>'+
      		'<p>Gracias por tu confianza.</p>' +
         	'<p>Equipo <strong>elPost</strong></p>'

		users.each do |user|
			if user.email.present?
				UserMailer.welcome(user,content).deliver_now!
			end
		end
	end

	def template(user, content, subject)
		@content = content
		@user = user
		mail(to: user.email, subject: subject)
	end

	def welcome(user, content)
		@user = user
		@content = content
		mail(to: user.email, subject: "Bienvenido a El Post")
	end

	def review_open_question(user, student, open_question, review)
		@user = user
		@student = student
		@open_question = open_question
		@review = review
		mail(to: student.email, subject: "El profesor #{@user.first_name.to_s+' '+@user.last_name.to_s} ha hecho un comentario a tu pregunta abierta")
	end

end
