class ApplicationMailer < ActionMailer::Base
  default from: 'elPost <soporte@elpost.co>'
  layout 'mailer'
end
