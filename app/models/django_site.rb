class DjangoSite < ActiveRecord::Base
    self.table_name = 'django_site'

    has_many :cms_siteconfigs, :class_name => 'CmsSiteconfig'
    has_many :cms_page_sites, :class_name => 'CmsPageSite', :foreign_key => :site_id
    has_many :cms_module_sites, :class_name => 'CmsModuleSite'
    has_many :cms_menus, :class_name => 'CmsMenu'
    has_many :news_layout, :class_name => 'NewsLayout', :foreign_key => :site_id
    has_many :ads_ad_sites, :class_name => 'AdsAdSite'
    has_many :ads_ads, through: :ads_ad_sites, :class_name => 'Ads_ad'
    has_many :news_section_sites, :class_name => 'NewsSectionSite'

    validates :domain, presence: true
    validates :name, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Sites",
	    priority: 1,
	    permit_params: [
	    	:domain,
	      	:name
	    ]
	}

end
