class DjangoAdminLog < ActiveRecord::Base
    self.table_name = 'django_admin_log'

    belongs_to :django_content_type, :class_name => 'DjangoContentType', :foreign_key => :content_type_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id, optional: true
end
