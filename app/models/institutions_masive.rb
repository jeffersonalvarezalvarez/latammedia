class InstitutionsMasive < ActiveRecord::Base
    belongs_to :masive, :class_name => 'Masive', :foreign_key => :masive_id, optional: true
    belongs_to :latamusers_intitution, :class_name => 'LatamusersIntitution', :foreign_key => :latamusers_intitution_id, optional: true
end