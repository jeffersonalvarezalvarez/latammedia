class Administrator < ActiveRecord::Base

  attr_accessor :login

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable,
         :authentication_keys => [:login]
  

  def self.find_for_database_authentication warden_conditions
      conditions = warden_conditions.dup
      login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", {value: login.strip.downcase}]).first
  end

  # Algoritmo de hashing compatible con PBKDFA
  def valid_password?(pwd)
      begin
          super(pwd)
      rescue
          Pbkdf2PasswordHasher.check_password(pwd,self.encrypted_password)
      end
  end

end