class MediaGalleryMedium < ActiveRecord::Base
    self.table_name = 'Media_gallery_media'

    belongs_to :media_gallery, :class_name => 'MediaGallery', :foreign_key => :gallery_id, optional: true
    belongs_to :media_medium, :class_name => 'MediaMedium', :foreign_key => :media_id, optional: true
end
