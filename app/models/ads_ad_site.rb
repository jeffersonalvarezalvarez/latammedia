class AdsAdSite < ActiveRecord::Base
    self.table_name = 'Ads_ad_sites'

    belongs_to :django_site, :class_name => 'DjangoSite', :foreign_key => :site_id, optional: true
    belongs_to :ads_ad, :class_name => 'AdsAd', :foreign_key => :ad_id, optional: true
end
