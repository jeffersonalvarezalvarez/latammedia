class Sequence < ActiveRecord::Base

    has_many :sequence_lms_activities, :class_name => 'SequenceLmsActivity', dependent: :destroy
    has_many :lms_activities, through: :sequence_lms_activities, :class_name => 'LmsActivity'
    has_many :sequence_users, :class_name => 'SequenceUser', dependent: :destroy
    has_many :latamusers_assignedsequences, :class_name => 'LatamusersAssignedsequence', dependent: :destroy


    accepts_nested_attributes_for :sequence_lms_activities, allow_destroy: true

    validates :name, presence: true
    validates :level, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Sequences of activities",
	    priority: 8,
	    permit_params: [
            :name,
	    	:level,
            :description,
	      	:sequence_lms_activities_attributes => [
	      		:id,
        		:_destroy,
        		:lms_activity_id,
        		:percentage,
        		:type_activity,
        		:order
			]
	    ]
	}
	
end
