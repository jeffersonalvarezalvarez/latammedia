class LmsActivity < ActiveRecord::Base
    self.table_name = 'LMS_activity'

    has_many :lms_questions, :class_name => 'LmsQuestion', :foreign_key => :activity_id, dependent: :destroy
    has_many :lms_categories, through: :lms_questions, :class_name => 'LmsCategory'

    has_many :sequence_lms_activities, :class_name => 'SequenceLmsActivity', :foreign_key => :lms_activity_id, dependent: :destroy
    has_many :sequences, through: :sequence_lms_activities, :class_name => 'Sequence'

    has_many :lms_activity_vocabularies, :class_name => 'LmsActivityVocabulary', :foreign_key => :activity_id, dependent: :destroy
    has_many :lms_words, through: :lms_activity_vocabularies, :class_name => 'LmsWord'

    has_many :latamusers_useractivities, :class_name => 'LatamusersUseractivity', :foreign_key => :activity_id, dependent: :destroy
    has_many :latamusers_userquestions, through: :latamusers_useractivities, :class_name => 'LatamusersUserquestion'
    has_many :latamusers_assignedactivities, :class_name => 'LatamusersAssignedactivity', :foreign_key => :activity_id, dependent: :destroy

    has_many :activity_institutions, :class_name => 'ActivityInstitution', :foreign_key => :activity_id
    has_many :latamusers_intitutions, through: :activity_institutions, :class_name => 'LatamusersIntitution', dependent: :destroy

    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :created_by_id, optional: true
    belongs_to :lms_course, :class_name => 'LmsCourse', :foreign_key => :course_id, optional: true
    belongs_to :lms_level, :class_name => 'LmsLevel', :foreign_key => :difficulty_id, optional: true
    belongs_to :news_article, :class_name => 'NewsArticle', :foreign_key => :article_id, optional: true

    has_many :news_article_sections, through: :news_article,:class_name => 'NewsArticleSection', :foreign_key => :article_id	

    validates :name, presence: true
    validates :introduction, presence: true
    validates :reward, presence: true

	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Lms",
	    priority: 1,
	    permit_params: [
	    	:name,
	      	:introduction,
	      	:public,
	      	:reward,
	      	:time,
	      	:published,
	      	:created_by_id,
	      	:difficulty_id,
	      	:course_id,
	      	:article_id,
	      	:private_content,
			:lms_word_ids => [],
			:latamusers_intitution_ids => []
	    ]
	}

end
