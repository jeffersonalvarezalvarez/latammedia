class AuthGroupPermission < ActiveRecord::Base
    belongs_to :auth_permission, :class_name => 'AuthPermission', :foreign_key => :permission_id, optional: true
    belongs_to :auth_group, :class_name => 'AuthGroup', :foreign_key => :group_id, optional: true
end
