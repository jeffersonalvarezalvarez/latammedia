class SequenceLmsActivity < ActiveRecord::Base

    belongs_to :sequence, :class_name => 'Sequence', :foreign_key => :sequence_id, optional: true
    belongs_to :lms_activity, :class_name => 'LmsActivity', :foreign_key => :lms_activity_id, optional: true

end
