class ReportBuilderDisplayfield < ActiveRecord::Base
    self.table_name = 'report_builder_displayfield'

    belongs_to :report_builder_report, :class_name => 'ReportBuilderReport', :foreign_key => :report_id, optional: true
    belongs_to :report_builder_format, :class_name => 'ReportBuilderFormat', :foreign_key => :display_format_id, optional: true
end
