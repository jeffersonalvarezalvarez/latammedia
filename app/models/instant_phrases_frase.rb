class InstantPhrasesFrase < ActiveRecord::Base
    self.table_name = 'instant_phrases_frase'

 	has_one :instant_phrases_tipo, :class_name => 'InstantPhrasesTipoFrase', :foreign_key => :tipo
    has_many :instant_phrases_partes, :class_name => 'InstantPhrasesParte', :foreign_key => :frase_id, dependent: :destroy
    
    accepts_nested_attributes_for :instant_phrases_partes, allow_destroy: true
    
    manage_with_tolaria using: {
        icon: "file-o",
        category: "InstantPhrases",
        priority: 1,
        permit_params: [
            :imagen,
            :nivel,
            :tipo,
            :instant_phrases_partes_attributes => [
                :id,
                :_destroy,
                :posicion,
                :opciones,
                :palabra_id
            ]
        ]
    }

end
