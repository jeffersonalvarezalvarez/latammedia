class UsersMasive < ActiveRecord::Base
    belongs_to :masive, :class_name => 'Masive', :foreign_key => :masive_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :auth_user_id, optional: true
end