class NewsLayout < ActiveRecord::Base
    self.table_name = 'News_layout'

    belongs_to :django_site, :class_name => 'DjangoSite', :foreign_key => :site_id, optional: true
    belongs_to :news_article, :class_name => 'NewsArticle', :foreign_key => :article_id, optional: true

end
