class TaggitTaggeditem < ActiveRecord::Base
    self.table_name = 'taggit_taggeditem'

	belongs_to :django_content_type, :class_name => 'DjangoContentType', :foreign_key => :content_type_id, optional: true
    belongs_to :taggit_tag, :class_name => 'TaggitTag', :foreign_key => :tag_id, optional: true
end
