class AdsAdArticle < ActiveRecord::Base
    self.table_name = 'Ads_ad_articles'

    belongs_to :ads_ad, :class_name => 'AdsAd', :foreign_key => :ad_id, optional: true
    belongs_to :news_article, :class_name => 'NewsArticle', :foreign_key => :article_id, optional: true
end
