class PaymentPasarelSubscriptioncategory < ActiveRecord::Base
    self.table_name = 'PaymentPasarel_subscriptioncategory'
    has_many :paymentpasarel_coupon_categories, :class_name => 'PaymentPasarelCouponCategory'

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Paymentpasarel",
	    priority: 2,
	    permit_params: [
	    	:description,
	    	:name,
	    	:value,
	    	:duration,
	    	:state,
	    	:iva
	    ]
	}

end
