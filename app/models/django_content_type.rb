class DjangoContentType < ActiveRecord::Base
    self.table_name = 'django_content_type'

    has_many :media_galleries, :class_name => 'MediaGallery'
    has_many :taggit_taggeditems, :class_name => 'TaggitTaggeditem'
    has_many :report_builder_reports, :class_name => 'ReportBuilderReport'
    has_many :django_admin_logs, :class_name => 'DjangoAdminLog'
    has_many :auth_permissions, :class_name => 'AuthPermission'
end
