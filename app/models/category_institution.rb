class CategoryInstitution < ActiveRecord::Base
	self.table_name = 'category_institution'

    belongs_to :news_section, :class_name => 'NewsSection', :foreign_key => :section_id, optional: true
    belongs_to :latamusers_intitution, :class_name => 'LatamusersIntitution', :foreign_key => :institution_id, optional: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Content private",
	    priority: 1,
	    permit_params: [
	    	:section_id,
	    	:institution_id
	    ]
	}

end
