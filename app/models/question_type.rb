class QuestionType < ApplicationRecord
    self.table_name = 'question_type'
 	
    manage_with_tolaria using: {
        icon: "file-o",
        category: "Lms",
        priority: 7,
        permit_params: [
            :name
        ]
    }
end
