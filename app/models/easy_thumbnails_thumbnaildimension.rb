class EasyThumbnailsThumbnaildimension < ActiveRecord::Base
    belongs_to :easy_thumbnails_thumbnail, :class_name => 'EasyThumbnailsThumbnail', :foreign_key => :thumbnail_id, optional: true
end
