class LmsQuote < ActiveRecord::Base
    self.table_name = 'LMS_quote'

    has_many :cms_siteconfigs, :class_name => 'CmsSiteconfig'

    validates :author, presence: true
    validates :quote, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Lms",
	    priority: 5,
	    permit_params: [
	    	:author,
	    	:quote,
	    ]
	}
	
end
