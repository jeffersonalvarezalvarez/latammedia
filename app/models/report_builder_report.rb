class ReportBuilderReport < ActiveRecord::Base
    self.table_name = 'report_builder_report'


    has_many :report_builder_report_starreds, :class_name => 'ReportBuilderReportStarred'
    has_many :report_builder_filterfields, :class_name => 'ReportBuilderFilterfield'
    has_many :report_builder_displayfields, :class_name => 'ReportBuilderDisplayfield'
    belongs_to :django_content_type, :class_name => 'DjangoContentType', :foreign_key => :root_model_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_modified_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_created_id, optional: true
end
