class NestedAdminItem < ActiveRecord::Base
    self.table_name = 'nested_admin_item'


    belongs_to :nested_admin_section, :class_name => 'NestedAdminSection', :foreign_key => :section_id, optional: true
end
