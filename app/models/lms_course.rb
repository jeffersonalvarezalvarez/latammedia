class LmsCourse < ActiveRecord::Base
    self.table_name = 'LMS_course'


    has_many :lms_activities, :class_name => 'LmsActivity'
end
