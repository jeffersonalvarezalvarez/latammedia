class CaracterizationCitemclientrelation < ActiveRecord::Base
    self.table_name = 'Caracterization_citemclientrelation'

    belongs_to :caracterization_caracterizationitem, :class_name => 'CaracterizationCaracterizationitem', :foreign_key => :citem_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id, optional: true

	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Caracterization",
	    priority: 1,
	    permit_params: [
	    	:user_id,
	      	:citem_id,
	      	:current_state
	    ]
	}

end
