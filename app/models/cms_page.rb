class CmsPage < ActiveRecord::Base
    self.table_name = 'CMS_page'


    has_many :cms_page_sites, :class_name => 'CmsPageSite', :foreign_key => :page_id, dependent: :destroy
    has_many :django_sites, through: :cms_page_sites, :class_name => 'DjangoSite'
    
    has_many :cms_module_pages, :class_name => 'CmsModulePage'
    has_many :ads_ad_pages, :class_name => 'AdsAdPage'
    has_many :ads_ads, through: :ads_ad_pages, :class_name => 'Ads_ad'

    has_many :media_galleries, :class_name => 'MediaGallery', :foreign_key => :object_id, dependent: :destroy
    accepts_nested_attributes_for :media_galleries, allow_destroy: true

    validates :title, presence: true
    validates :content, presence: true
    validates :excerpt, presence: true
    validates :pubDate, presence: true
    validates :upDate, presence: true

	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Cms",
	    priority: 2,
	    permit_params: [
	    	:title,
	      	:content,
	      	:excerpt,
	      	:featured,
	      	:published,
	      	:pubDate,
	      	:upDate,
	      	:slug,
	      	:media_galleries_attributes => [
        		:id,
        		:_destroy,
        		:style,
        		:position,
        		:media_medium_ids => []
			],
			:django_site_ids => []
	    ]
	}

end
