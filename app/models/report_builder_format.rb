class ReportBuilderFormat < ActiveRecord::Base
    self.table_name = 'report_builder_format'


    has_many :report_builder_displayfields, :class_name => 'ReportBuilderDisplayfield'
end
