class Temp < ActiveRecord::Base
  self.table_name = 'templates'

  manage_with_tolaria using: {
    icon: "file-o",
    category: "Templates de correos",
    priority: 1,
    permit_params: [
      :title,
      :template
    ]
  }

end