class CaracterizationCaracterizationcategory < ActiveRecord::Base
    self.table_name = 'Caracterization_caracterizationcategory'

    has_many :caracterization_caracterizationitems, :class_name => 'CaracterizationCaracterizationitem', :foreign_key => :category_id

    validates :detail, presence: true
    
	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Caracterization",
	    priority: 2,
	    permit_params: [
	    	:detail,
	      	:description
	    ]
	}

end
