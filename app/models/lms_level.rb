class LmsLevel < ActiveRecord::Base
    self.table_name = 'LMS_level'

    has_many :lms_activities, :class_name => 'LmsActivity'

    validates :difficulty_level, presence: true
    validates :color, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Lms",
	    priority: 3,
	    permit_params: [
	    	:difficulty_level,
	    	:color	    
	    ]
	}

end
