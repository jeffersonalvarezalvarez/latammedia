class InstantPhrasesPalabra < ApplicationRecord
    self.table_name = 'instant_phrases_palabra'
 	
 	belongs_to :instant_phrases_tipo, :class_name => 'InstantPhrasesTipoFrase', :foreign_key => :tipo, optional: true
    
    manage_with_tolaria using: {
        icon: "file-o",
        category: "InstantPhrases",
        priority: 2,
        permit_params: [
            :sintaxis,
            :definicion,
            :tipo
        ]
    }

end
