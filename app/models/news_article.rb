class NewsArticle < ActiveRecord::Base
    self.table_name = 'News_article'

    has_many :news_article_sections, :class_name => 'NewsArticleSection', :foreign_key => :article_id, dependent: :destroy
    has_many :news_sections, through: :news_article_sections, :class_name => 'NewsSection'

    has_many :media_galleries, :class_name => 'MediaGallery', :foreign_key => :object_id, dependent: :destroy
    accepts_nested_attributes_for :media_galleries

    has_many :taggit_taggeditems, :class_name => 'TaggitTaggeditem', :foreign_key => :object_id, dependent: :destroy
    has_many :taggit_tags, through: :taggit_taggeditems, :class_name => 'TaggitTag'

    has_many :news_layouts, :class_name => 'NewsLayout', :foreign_key => :article_id, dependent: :destroy
    accepts_nested_attributes_for :news_layouts

    has_many :lms_activities, :class_name => 'LmsActivity', :foreign_key => :article_id, dependent: :destroy
    has_many :cms_module_articles, :class_name => 'CmsModuleArticle'
    has_many :ads_ad_articles, :class_name => 'AdsAdArticle', :foreign_key => :article_id, dependent: :destroy
    has_many :ads_ads, through: :ads_ad_articles, :class_name => 'Ads_ad'

    validates :pubDate, presence: true
    validates :upDate, presence: true
    validates :title, presence: true
    validates :content, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "News",
	    priority: 1,
	    permit_params: [
	    	:title,
			:content,
			:excerpt,
			:author,
			:source,
			:source_url,
			:pubDate,
			:upDate,
			:slug,
			:featured,
			:show_link_to_post,
			:description,
			:watch,
			:tags,
			:media_galleries_attributes => [
        		:id,
        		:_destroy,
        		:style,
        		:position,
        		:content_type_id,
        		:media_medium_ids => []
			],
			:news_layouts_attributes => [
				:id,
				:_destroy,
				:editors_pick,
				:site_id,
				:column_layout,
				:published,
				:show_comments,
				:schedulePublication,
				:hits
			],
			:news_section_ids => []
	    ]
	}

	def tags=(tags)
		tags_items = TaggitTag.select(:name).collect{ |u| u.name }
		split = tags.split(',')
		array_tags = []
		array_taggit_tagged = []

		self.taggit_tags.destroy_all

		split.each do |tag|
			tag = tag.strip
			if tags_items.include? tag
				array_taggit_tagged << TaggitTaggeditem.new(:tag_id => TaggitTag.find_by_name(tag).id, :content_type_id => 13)
			else
				array_tags << TaggitTag.new(name: tag, slug: tag.parameterize)
			end
		end

		if array_tags.count > 0
  			self.taggit_tags = array_tags
  		end

  		if array_taggit_tagged.count > 0
  			self.taggit_taggeditems = array_taggit_tagged
  		end
	end


	def tags
		tags_items = self.taggit_tags.select(:name)
		tags_final = ""
		counter = 0
		tags_items.each do |tag|
			if counter == 0
				tags_final = tags_final + tag.name
			else
				tags_final = tags_final + "," + tag.name
			end
			counter = counter + 1
		end
		
		tags_final
	end

end