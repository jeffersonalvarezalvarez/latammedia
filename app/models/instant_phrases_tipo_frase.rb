class InstantPhrasesTipoFrase < ApplicationRecord
    self.table_name = 'instant_phrases_tipo_frase'
 	
    manage_with_tolaria using: {
        icon: "file-o",
        category: "InstantPhrases",
        priority: 2,
        permit_params: [
            :nombre
        ]
    }
end
