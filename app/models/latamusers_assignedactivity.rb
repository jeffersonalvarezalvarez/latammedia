class LatamusersAssignedactivity < ActiveRecord::Base
    self.table_name = 'latamusers_assignedactivity'

    belongs_to :lms_activity, :class_name => 'LmsActivity', :foreign_key => :activity_id, optional: true
    has_many :latamusers_useractivities, :class_name => 'LatamusersUseractivity', :foreign_key => :assigned_activity_id, dependent: :destroy
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :teacher_id, optional: true

    validates :maximum_tries, presence: true
    validates :creation_date, presence: true
    validates :start_date, presence: true
    validates :end_date, presence: true

	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Latamusers",
	    priority: 1,
	    permit_params: [
	    	:teacher_id,
	      	:activity_id,
	      	:maximum_tries,
	      	:creation_date,
	      	:start_date,
	      	:end_date,
	      	:active
	    ]
	}

end
