class NestedAdminGroup < ActiveRecord::Base
    self.table_name = 'nested_admin_group'


    has_many :nested_admin_sections, :class_name => 'NestedAdminSection'
end
