class DjangoCronCronjoblog < ActiveRecord::Base
    self.table_name = 'django_cron_cronjoblog'

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Cron",
	    priority: 1,
	    permit_params: [
	    	:code,
	      	:start_time,
	      	:end_time,
	      	:is_success,
	      	:message
	    ]
	}

end
