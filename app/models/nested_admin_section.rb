class NestedAdminSection < ActiveRecord::Base
    self.table_name = 'nested_admin_section'


    belongs_to :nested_admin_group, :class_name => 'NestedAdminGroup', :foreign_key => :group_id, optional: true
    has_many :nested_admin_items, :class_name => 'NestedAdminItem'
end
