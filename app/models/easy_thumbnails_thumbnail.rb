class EasyThumbnailsThumbnail < ActiveRecord::Base
    self.table_name = 'easy_thumbnails_thumbnail'

    has_many :easy_thumbnails_thumbnaildimensions, :class_name => 'EasyThumbnailsThumbnaildimension'
    belongs_to :easy_thumbnails_source, :class_name => 'EasyThumbnailsSource', :foreign_key => :source_id, optional: true
end
