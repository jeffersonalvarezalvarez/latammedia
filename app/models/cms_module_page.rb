class CmsModulePage < ActiveRecord::Base
    self.table_name = 'CMS_module_pages'

    belongs_to :cms_module, :class_name => 'CmsModule', :foreign_key => :module_id, optional: true
    belongs_to :cms_page, :class_name => 'CmsPage', :foreign_key => :page_id, optional: true
end
