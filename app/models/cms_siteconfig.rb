class CmsSiteconfig < ActiveRecord::Base
    self.table_name = 'CMS_siteconfig'


    belongs_to :lms_quote, :class_name => 'LmsQuote', :foreign_key => :quote_id, optional: true
    belongs_to :django_site, :class_name => 'DjangoSite', :foreign_key => :site_id, optional: true
    belongs_to :news_section, :class_name => 'NewsSection', :foreign_key => :watch_module_category_id, optional: true
    belongs_to :lms_word, :class_name => 'LmsWord', :foreign_key => :word_of_the_day_id, optional: true

    validates :site_id, uniqueness: true
    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Cms",
	    priority: 4,
	    permit_params: [
	    	:site_id,
	      	:facebook,
	      	:twitter,
	      	:youtube,
	      	:linkedin,
	      	:highlight_featured,
	      	:featured_title,
	      	:google_analytics,
	      	:watch_module_category_id,
	      	:category_link_color
	    ]
	}

end
