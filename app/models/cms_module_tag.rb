class CmsModuleTag < ActiveRecord::Base
    self.table_name = 'CMS_module_tags'

    belongs_to :cms_module, :class_name => 'CmsModule', :foreign_key => :module_id, optional: true
    belongs_to :taggit_tag, :class_name => 'TaggitTag', :foreign_key => :tag_id, optional: true
end
