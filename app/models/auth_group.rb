class AuthGroup < ActiveRecord::Base
    self.table_name = 'auth_group'


    has_many :auth_user_groups, :class_name => 'AuthUserGroup'
    has_many :auth_group_permissions, :class_name => 'AuthGroupPermission', :foreign_key => :group_id
    has_many :auth_permissions, through: :auth_group_permissions, :class_name => 'AuthPermission'

    validates :name, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Auth",
	    priority: 1,
	    permit_params: [
	    	:name,
	    	:auth_permission_ids => []
	    ]
	}
	
end
