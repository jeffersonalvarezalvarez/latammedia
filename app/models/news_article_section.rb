class NewsArticleSection < ActiveRecord::Base
    self.table_name = 'News_article_section'

    belongs_to :news_section, :class_name => 'NewsSection', :foreign_key => :section_id, optional: true
    belongs_to :news_article, :class_name => 'NewsArticle', :foreign_key => :article_id, optional: true
end
