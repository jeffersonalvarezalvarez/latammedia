class BlogEntry < ActiveRecord::Base
    self.table_name = 'Blog_entry'

    has_many :media_galleries, :class_name => 'MediaGallery', :foreign_key => :object_id, dependent: :destroy
    accepts_nested_attributes_for :media_galleries, allow_destroy: true

    validates :pubDate, presence: true
    validates :upDate, presence: true
    validates :title, presence: true
    validates :slug, presence: true
    validates :content, presence: true
    validates :author, presence: true
    validates :author_email, presence: true

	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Blog",
	    priority: 1,
	    permit_params: [
	    	:title,
	      	:slug,
	      	:content,
	      	:author,
	      	:author_email,
	      	:published,
	      	:featured,
	      	:show_email,
	      	:show_comments,
	      	:upDate,
        	:pubDate,
	      	:media_galleries_attributes => [
        		:id,
        		:_destroy,
        		:style,
        		:position,
                :content_type_id,
        		:media_medium_ids => []
			]
	    ]
	}


end
