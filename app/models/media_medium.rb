class MediaMedium < ActiveRecord::Base
    self.table_name = 'Media_media'

    self.inheritance_column = :ruby_type
    has_many :media_gallery_media, :class_name => 'MediaGalleryMedium', :foreign_key => :media_id
    has_many :lms_slides, :class_name => 'LmsSlide'
    has_many :lms_questions, :class_name => 'LmsQuestion'

    validates :title, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Media",
	    priority: 1,
	    permit_params: [
		    :title,
		    :type,
		    :media,
		    :poster,
		    :iframe_url,
		    :author
	    ]
	}

	mount_uploader :media, MediaImageUploader
	mount_uploader :poster, AttachmentUploader

end
