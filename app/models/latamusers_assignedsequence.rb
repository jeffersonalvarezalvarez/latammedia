class LatamusersAssignedsequence < ApplicationRecord

	belongs_to :sequence, :class_name => 'Sequence', :foreign_key => :sequence_id, optional: true
    has_many :sequence_users, :class_name => 'SequenceUser', :foreign_key => :assigned_sequence_id
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :teacher_id, optional: true

    validates :maximum_tries, presence: true
    validates :creation_date, presence: true
    validates :start_date, presence: true
    validates :end_date, presence: true

end
