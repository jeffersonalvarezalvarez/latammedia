class AdsAdTag < ActiveRecord::Base
    self.table_name = 'Ads_ad_tags'

    belongs_to :ads_ad, :class_name => 'AdsAd', :foreign_key => :ad_id, optional: true
    belongs_to :taggit_tag, :class_name => 'TaggitTag', :foreign_key => :tag_id, optional: true

end
