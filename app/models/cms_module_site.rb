class CmsModuleSite < ActiveRecord::Base
    self.table_name = 'CMS_module_sites'

    belongs_to :django_site, :class_name => 'DjangoSite', :foreign_key => :site_id, optional: true
    belongs_to :cms_module, :class_name => 'CmsModule', :foreign_key => :module_id, optional: true
end
