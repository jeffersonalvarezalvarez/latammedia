class NewsSectionsidebarcontentSection < ActiveRecord::Base
    self.table_name = 'News_sectionsidebarcontent_sections'

    belongs_to :news_sectionsidebarcontent, :class_name => 'NewsSectionsidebarcontent', :foreign_key => :sectionsidebarcontent_id, optional: true
    belongs_to :news_section, :class_name => 'NewsSection', :foreign_key => :section_id, optional: true
end
