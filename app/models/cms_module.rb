class CmsModule < ActiveRecord::Base
    self.table_name = 'CMS_module'

    has_many :cms_module_tags, :class_name => 'CmsModuleTag', :foreign_key => :module_id, dependent: :destroy
    has_many :taggit_tags, through: :cms_module_tags, :class_name => 'TaggitTag'

    has_many :cms_module_sites, :class_name => 'CmsModuleSite', :foreign_key => :module_id, dependent: :destroy
    has_many :django_sites, through: :cms_module_sites, :class_name => 'DjangoSite'

    has_many :cms_module_sections, :class_name => 'CmsModuleSection', :foreign_key => :module_id
    has_many :news_sections, through: :cms_module_sections, :class_name => 'NewsSection'

    has_many :cms_module_pages, :class_name => 'CmsModulePage', :foreign_key => :module_id, dependent: :destroy
    has_many :cms_pages, through: :cms_module_pages, :class_name => 'CmsPage'

    has_many :cms_module_articles, :class_name => 'CmsModuleArticle', :foreign_key => :module_id, dependent: :destroy
    has_many :news_articles, through: :cms_module_articles, :class_name => 'NewsArticle'

    validates :name, presence: true
    validates :order, presence: true

	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Cms",
	    priority: 3,
	    permit_params: [
	    	:name,
	      	:html,
	      	:javascript,
	      	:js_file,
	      	:css,
	      	:css_file,
	      	:order,
	      	:position,
	      	:display,
	      	:taggit_tag_ids => [],
	      	:django_site_ids => [],
			:news_section_ids => [],
			:cms_page_ids => [],
			:news_article_ids => []
	    ]
	}

	mount_uploader :js_file, AttachmentUploader
	mount_uploader :css_file, AttachmentUploader

end
