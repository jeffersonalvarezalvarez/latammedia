class AuthUser < ActiveRecord::Base

    require 'csv'
    require 'roo'

    self.table_name = 'auth_users'

    attr_accessor :login
    attr_accessor :terms
    
    devise :database_authenticatable, :registerable,
        :recoverable, :rememberable, :trackable, :validatable, 
        :confirmable, :authentication_keys => [:login]
    
    has_one :administrator

    has_many :caracterization_citemclientrelations, :class_name => 'CaracterizationCitemclientrelation', :foreign_key => :user_id, dependent: :destroy
    has_many :report_builder_report_starreds, :class_name => 'ReportBuilderReportStarred', :foreign_key => :user_id, dependent: :destroy
    has_many :report_builder_reports_modified, :class_name => 'ReportBuilderReport', :foreign_key => :user_modified_id, dependent: :destroy
    has_many :report_builder_reports_created, :class_name => 'ReportBuilderReport', :foreign_key => :user_created_id, dependent: :destroy
    has_many :registration_registrationprofiles, :class_name => 'RegistrationRegistrationprofile', :foreign_key => :user_id, dependent: :destroy
    has_many :latamusers_userquestions, :class_name => 'LatamusersUserquestion', :foreign_key => :user_id
    has_many :latamusers_userprofiles, :class_name => 'LatamusersUserprofile', :foreign_key => :user_id, dependent: :destroy
    has_many :latamusers_usergroups, :class_name => 'LatamusersUsergroup', :foreign_key => :teacher_id, dependent: :destroy
    
    has_many :latamusers_assignedsequences, :class_name => 'LatamusersAssignedsequence', :foreign_key => :teacher_id, dependent: :destroy
    has_many :latamusers_assignedactivities, :class_name => 'LatamusersAssignedactivity', :foreign_key => :teacher_id, dependent: :destroy
    
    has_many :sequence_users, :class_name => 'SequenceUser', :foreign_key => :auth_user_id, dependent: :destroy
    has_many :latamusers_useractivities, :class_name => 'LatamusersUseractivity', :foreign_key => :user_id, dependent: :destroy
    has_many :lms_activities, through: :latamusers_useractivities, :class_name => 'LmsActivity'
    has_many :created_activites, :class_name => 'LmsActivity', :foreign_key => :created_by_id, dependent: :destroy
    has_many :lms_questions, through: :lms_activities, :class_name => 'LmsQuestion'

    has_many :latamusers_students, :class_name => 'LatamusersStudent', :foreign_key => :user_id, dependent: :destroy
    has_many :latamusers_students_teacher, :class_name => 'LatamusersStudent', :foreign_key => :teacher_id, dependent: :destroy
    has_many :latamusers_assignedactivities, :class_name => 'LatamusersAssignedactivity', :foreign_key => :teacher_id, dependent: :destroy
    has_many :django_admin_logs, :class_name => 'DjangoAdminLog', :foreign_key => :user_id, dependent: :destroy

    has_many :auth_user_user_permissions, :class_name => 'AuthUserUserPermission', :foreign_key => :user_id, dependent: :destroy
    has_many :auth_permissions, through: :auth_user_user_permissions, :class_name => 'AuthPermission'

    has_many :auth_user_groups, :class_name => 'AuthUserGroup', :foreign_key => :user_id, dependent: :destroy
    has_many :auth_groups, through: :auth_user_groups, :class_name => 'AuthGroup'

    has_many :paymentpasarel_subscriptioninformations, :class_name => 'PaymentPasarelSubscriptioninformation', :foreign_key => :user_id, dependent: :destroy

    has_many :open_questions, :class_name => "OpenQuestion", :foreign_key => :auth_user_id, dependent: :destroy
    accepts_nested_attributes_for :latamusers_userprofiles
    
    validates_acceptance_of :terms
    validates :email, uniqueness: true
    validates :email, presence: true
    validates :username, uniqueness: true
    validates :username, presence: true
    validates :last_sign_in_at, presence: true
    validates :date_joined, presence: true

    manage_with_tolaria using: {
        icon: "file-o",
        category: "Auth",
        priority: 2,
        permit_params: [
            :username,
            :email,
            :first_name,
            :last_name,
            :is_active,
            :date_joined,
            :is_staff,
            :is_superuser,
            :last_sign_in_at,
            :password,
            :auth_permission_ids => [],
            :auth_group_ids => []
        ]
    }

    # Para que se pueda autenticar con email o username
    def self.find_for_database_authentication warden_conditions
        conditions = warden_conditions.dup
        login = conditions.delete(:login)
        where(conditions).where(["lower(username) = :value OR lower(email) = :value", {value: login.strip.downcase}]).first
    end

    # Algoritmo de hashing compatible con PBKDFA
    def valid_password?(pwd)
        begin
            super(pwd)
        rescue
            Pbkdf2PasswordHasher.check_password(pwd,self.encrypted_password)
        end
    end

    # Para autenticarse necesita cumplir con estos requisitos
    def active_for_authentication?
        super and self.is_active? and self.latamusers_userprofiles.first.date_end > Date.today
    end

    # Mensaje para mostrar que no está activo para loguearse
    def inactive_message
        if self.latamusers_userprofiles.first.date_end < Date.today
            "Your subscription has expired at " + self.latamusers_userprofiles.first.date_end.strftime("%b. %d, %Y")
        elsif !self.is_active?
            "Currently you are not active in the platform"
        elsif self.confirmed_at.blank?
            "You have not confirmed your email"
        end
    end

    # Creación de un método que permitirá importar datos desde un csv
    def self.import(file)

        success = true
        str_errors = ""
        spreadsheet = open_spreadsheet(file)
        header = spreadsheet.row(1)
        counter = 0;
        (2..spreadsheet.last_row).each do |i|
            row = Hash[[header, spreadsheet.row(i)].transpose]
            row = row.to_hash
            hash = HashWithIndifferentAccess.new(row)
            user = new

            group = LatamusersUsergroup.find_by_name(hash[:group_name])
            institution = LatamusersIntitution.find_by_name(hash[:institution_name])
            teacher = AuthUser.find_by_username(hash[:teacher_username])

            if institution.blank?
                institution = LatamusersIntitution.new(:name => hash[:institution_name],
                    :nit => hash[:nit],
                    :contact_name => hash[:contact_name],
                    :contact_phone => hash[:contact_phone],
                    :contact_email => hash[:contact_email])
                if !(institution.save)
                        success = false
                        str_errors = str_errors + "In the row number " + i.to_s + " appears the next errors: <br>"
                        institution.errors.full_messages.each do |error|
                        str_errors = str_errors + error.to_s + "<br>"
                    end
                end
            end

            if group.blank?
                group = LatamusersUsergroup.new(:name => hash[:group_name],
                    :code => hash[:group_name] + ":" + Time.now.to_s,
                    :teacher_id => teacher.id)
                if !(group.save)
                        success = false
                        str_errors = str_errors + "In the row number " + i.to_s + " appears the next errors: <br>"
                        group.errors.full_messages.each do |error|
                        str_errors = str_errors + error.to_s + "<br>"
                    end
                end                
            end

            user_found = AuthUser.where("email = ? OR username = ?",hash[:email],hash[:username].to_s)
            if user_found.present?
                
                user_found.first.update_attribute(:first_name, hash[:first_name])
                user_found.first.update_attribute(:last_name, hash[:last_name])
                user_found.first.update_attribute(:password, hash[:username].to_s)
                user_found.first.update_attribute(:email, hash[:email])
                user_found.first.update_attribute(:username, hash[:username].to_s)

                user_found.first.latamusers_students.destroy_all
                user_found.first.latamusers_students.build(:teacher_id => teacher.id,
                    :group_id => group.id,
                    :institution_id => institution.id)

                user_found.first.latamusers_userprofiles.destroy_all
                user_found.first.latamusers_userprofiles.build(:english_level => "none", 
                    :education => "none",
                    :ocupation => "none",
                    :type => "student",
                    :date_subscription => Time.now,
                    :date_end => hash[:date_end],
                    :subscripted => true,
                    :gender => hash[:gender])

                if !(user_found.first.save(:validate => false))
                    success = false
                    str_errors = str_errors + "In the row number " + i.to_s + " appears the next errors: <br>"
                    user_found.first.errors.full_messages.each do |error|
                        str_errors = str_errors + error.to_s + "<br>"
                    end
                end
            else
                user.latamusers_students.build(:teacher_id => teacher.id,
                    :group_id => group.id,
                    :institution_id => institution.id)

                user.latamusers_userprofiles.build(:english_level => "none", 
                    :education => "none",
                    :ocupation => "none",
                    :type => "student",
                    :date_subscription => Time.now,
                    :date_end => hash[:date_end],
                    :subscripted => true,
                    :gender => hash[:gender])

                user.email = hash[:email]
                user.username = hash[:username]
                user.password = hash[:username]
                user.last_sign_in_at =  Time.now
                user.date_joined =  Time.now
                user.first_name = hash[:first_name]
                user.last_name = hash[:last_name]
                user.skip_confirmation!

                if !(user.save)
                    success = false
                    str_errors = str_errors + "In the row number " + i.to_s + " appears the next errors: <br>"
                    user.errors.full_messages.each do |error|
                        str_errors = str_errors + error.to_s + "<br>"
                    end
                end
            end
        end

        return [success, str_errors]

    end

    def self.open_spreadsheet(file)
      case File.extname(file.original_filename)
        when ".csv" then Csv.new(file.path)
        when ".xls" then Roo::Excel.new(file.path)
        when ".xlsx" then Roo::Excelx.new(file.path)
        else raise "Unknown file type: #{file.original_filename}"
      end
    end

end
