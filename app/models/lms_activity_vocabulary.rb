class LmsActivityVocabulary < ActiveRecord::Base
    self.table_name = 'LMS_activity_vocabulary'


    belongs_to :lms_activity, :class_name => 'LmsActivity', :foreign_key => :activity_id, optional: true
    belongs_to :lms_word, :class_name => 'LmsWord', :foreign_key => :word_id, optional: true
end
