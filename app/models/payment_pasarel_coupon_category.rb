class PaymentPasarelCouponCategory < ActiveRecord::Base
    self.table_name = 'PaymentPasarel_coupon_categories'

    belongs_to :paymentpasarel_coupon, :class_name => 'PaymentPasarelCoupon', :foreign_key => :coupon_id, optional: true
    belongs_to :paymentpasarel_subscriptioncategory, :class_name => 'PaymentPasarelSubscriptioncategory', :foreign_key => :subscriptioncategory_id, optional: true
end
