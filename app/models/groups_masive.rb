class GroupsMasive < ActiveRecord::Base
    belongs_to :masive, :class_name => 'Masive', :foreign_key => :masive_id, optional: true
    belongs_to :latamusers_usergroup, :class_name => 'LatamusersUsergroup', :foreign_key => :latamusers_usergroup_id, optional: true
end