class AdsAd < ActiveRecord::Base
    self.table_name = 'Ads_ad'

    self.inheritance_column = :ruby_type
    has_many :ads_ad_tags, :class_name => 'AdsAdTag', :foreign_key => :ad_id
    has_many :taggit_tags, through: :ads_ad_tags, :class_name => 'TaggitTag'

    has_many :ads_ad_sites, :class_name => 'AdsAdSite', :foreign_key => :ad_id, dependent: :destroy
    has_many :django_sites, through: :ads_ad_sites, :class_name => 'DjangoSite'

    has_many :ads_ad_sections, :class_name => 'AdsAdSection', :foreign_key => :ad_id
    has_many :news_sections, through: :ads_ad_sections, :class_name => 'NewsSection'

    has_many :ads_ad_pages, :class_name => 'AdsAdPage', :foreign_key => :ad_id
    has_many :cms_pages, through: :ads_ad_pages, :class_name => 'CmsPage'

    has_many :ads_ad_articles, :class_name => 'AdsAdArticle', :foreign_key => :ad_id
    has_many :news_articles, through: :ads_ad_articles, :class_name => 'NewsArticle'

    validates :title, presence: true
    validates :type, presence: true
    validates :script, presence: true
    validates :image, presence: true
    validates :include_code, presence: true
    validates :position, presence: true
    validates :display, presence: true
    validates :link, presence: true
    validates :order, presence: true
    
    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Ads",
	    priority: 1,
	    permit_params: [
	    	:title,
	      	:type,
	      	:image,
	      	:link,
	      	:script,
	      	:include_code,
	      	:position,
	      	:display,
	      	:order,
			:news_section_ids => [],
			:news_article_ids => [],
			:cms_page_ids => [],
			:taggit_tag_ids =>[],
			:django_site_ids =>[]
	    ]
	}

	mount_uploader :image, MediaImageUploader

end
