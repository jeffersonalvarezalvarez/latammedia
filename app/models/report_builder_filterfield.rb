class ReportBuilderFilterfield < ActiveRecord::Base
    self.table_name = 'report_builder_filterfield'

    belongs_to :report_builder_report, :class_name => 'ReportBuilderReport', :foreign_key => :report_id, optional: true
end
