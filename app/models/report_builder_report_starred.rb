class ReportBuilderReportStarred < ActiveRecord::Base
    self.table_name = 'report_builder_report_starred'


    belongs_to :report_builder_report, :class_name => 'ReportBuilderReport', :foreign_key => :report_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id, optional: true
end
