class AdsAdSection < ActiveRecord::Base
    self.table_name = 'Ads_ad_sections'


    belongs_to :news_section, :class_name => 'NewsSection', :foreign_key => :section_id, optional: true
    belongs_to :ads_ad, :class_name => 'AdsAd', :foreign_key => :ad_id, optional: true
end
