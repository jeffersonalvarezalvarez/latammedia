class CmsMenu < ActiveRecord::Base
    self.table_name = 'CMS_menu'

    belongs_to :cms_menu, :class_name => 'CmsMenu', :foreign_key => :parent_id, optional: true
    belongs_to :django_site, :class_name => 'DjangoSite', :foreign_key => :site_id, optional: true

    validates :order, presence: true
    validates :name, presence: true

	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Cms",
	    priority: 1,
	    permit_params: [
	    	:name,
	      	:url,
	      	:parent_id,
	      	:order,
	      	:color,
	      	:site_id,
	      	:published,
	      	:location
	    ]
	}

end
