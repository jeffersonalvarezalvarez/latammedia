class LatamusersUserprofile < ActiveRecord::Base
    self.table_name = 'latamusers_userprofile'

    self.inheritance_column = :ruby_type
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id, required: false, optional: true
    validates :user_id, uniqueness: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Latamusers",
	    priority: 5,
	    permit_params: [
	      	:user_id,
	      	:thumbnail,
	      	:type,
	      	:city,
	      	:country,
	      	:gender,
	      	:birthday,
	      	:english_level,
	      	:education,
	      	:ocupation,
	      	:subscripted,
	      	:date_subscription,
	      	:date_end
	    ]
	}

	mount_uploader :thumbnail, ProfileimgUploader

end
