class AdsAdPage < ActiveRecord::Base
    self.table_name = 'Ads_ad_pages'
    
    belongs_to :ads_ad, :class_name => 'AdsAd', :foreign_key => :ad_id, optional: true
    belongs_to :cms_page, :class_name => 'CmsPage', :foreign_key => :page_id, optional: true
end
