class NewsSectionSite < ActiveRecord::Base
    self.table_name = 'News_section_sites'
    
    belongs_to :django_site, :class_name => 'DjangoSite', :foreign_key => :site_id, optional: true
    belongs_to :news_section, :class_name => 'NewsSection', :foreign_key => :section_id, optional: true

end
