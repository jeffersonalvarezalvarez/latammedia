class AuthUserUserPermission < ActiveRecord::Base
    belongs_to :auth_permission, :class_name => 'AuthPermission', :foreign_key => :permission_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id, optional: true
end
