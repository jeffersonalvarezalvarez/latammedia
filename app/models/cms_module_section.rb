class CmsModuleSection < ActiveRecord::Base
    self.table_name = 'CMS_module_sections'

    belongs_to :news_section, :class_name => 'NewsSection', :foreign_key => :section_id, optional: true
    belongs_to :cms_module, :class_name => 'CmsModule', :foreign_key => :module_id, optional: true
end
