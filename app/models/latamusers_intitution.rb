class LatamusersIntitution < ActiveRecord::Base
    has_many :latamusers_students, :class_name => 'LatamusersStudent', :foreign_key => :institution_id, dependent: :destroy

    validates :name, presence: true
	validates :nit, presence: true

	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Latamusers",
	    priority: 1,
	    permit_params: [
	    	:name,
	      	:remove_logo,
	      	:logo,
	      	:nit,
	      	:contact_name,
	      	:contact_phone,
	      	:contact_email
	    ]
	}

	mount_uploader :logo, InstitutionImageUploader

end
