class LatamusersUsergroup < ActiveRecord::Base
    self.table_name = 'latamusers_usergroups'
    self.primary_key = :code

    has_many :latamusers_students, :class_name => 'LatamusersStudent', :foreign_key => :group_id, dependent: :destroy
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :teacher_id, optional: true

    validates :code, uniqueness: true
	validates :code, presence: true
	validates :name, presence: true

	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Latamusers",
	    priority: 4,
	    permit_params: [
	      	:name,
	      	:teacher_id,
	      	:code,
	      	:methodology,
	      	:sequence_id
	    ]
	}

end
