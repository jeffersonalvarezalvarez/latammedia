class AuthPermission < ActiveRecord::Base
    self.table_name = 'auth_permission'

    has_many :auth_user_user_permissions, :class_name => 'AuthUserUserPermission', :foreign_key => :permission_id
    has_many :auth_group_permissions, :class_name => 'AuthGroupPermission'
    belongs_to :django_content_type, :class_name => 'DjangoContentType', :foreign_key => :content_type_id, optional: true
end
