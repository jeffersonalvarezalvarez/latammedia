class ActivityInstitution < ActiveRecord::Base

    belongs_to :lms_activity, :class_name => 'LmsActivity', :foreign_key => :activity_id, optional: true
    belongs_to :latamusers_intitution, :class_name => 'LatamusersIntitution', :foreign_key => :institution_id, optional: true

end
