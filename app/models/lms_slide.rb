class LmsSlide < ActiveRecord::Base
    self.table_name = 'LMS_slide'

    belongs_to :media_medium, :class_name => 'MediaMedium', :foreign_key => :image_id, optional: true
    validates :title, presence: true
    validates :text, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Lms",
	    priority: 6,
	    permit_params: [
	    	:title,
	    	:text,
	    	:image_id
	    ]
	}

end
