class InstantPhrasesParte < ActiveRecord::Base
    self.table_name = 'instant_phrases_parte'

 	belongs_to :instant_phrases_palabra, :class_name => 'InstantPhrasesPalabra', :foreign_key => :palabra_id, optional: true
 	belongs_to :instant_phrases_phrase, :class_name => 'InstantPhrasesFrase', :foreign_key => :frase_id, optional: true
end
