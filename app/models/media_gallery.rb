class MediaGallery < ActiveRecord::Base
    self.table_name = 'Media_gallery'

    belongs_to :django_content_type, :class_name => 'DjangoContentType', :foreign_key => :content_type_id, optional: true
    belongs_to :blog_entry, :class_name => 'BlogEntry', :foreign_key => :object_id, optional: true
    belongs_to :cms_page, :class_name => 'CmsPage', :foreign_key => :object_id, optional: true
    belongs_to :news_article, :class_name => 'NewsArticle', :foreign_key => :object_id, optional: true

    has_many :media_gallery_media, :class_name => 'MediaGalleryMedium', :foreign_key => :gallery_id, dependent: :destroy
    has_many :media_media, through: :media_gallery_media, :class_name => 'MediaMedium'
    accepts_nested_attributes_for :media_media
    
end
