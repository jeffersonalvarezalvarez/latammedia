class CaracterizationCaracterizationitem < ActiveRecord::Base
    self.table_name = 'Caracterization_caracterizationitem'


    has_many :caracterization_citemclientrelations, :class_name => 'CaracterizationCitemclientrelation'
    belongs_to :caracterization_caracterizationcategory, :class_name => 'CaracterizationCaracterizationcategory', :foreign_key => :category_id, optional: true

    validates :name, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Caracterization",
	    priority: 3,
	    permit_params: [
	    	:category_id,
	    	:name,
	    	:state
	    ]
	}

end
