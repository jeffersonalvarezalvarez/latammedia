class LatamusersUseractivity < ActiveRecord::Base
    self.table_name = 'latamusers_useractivity'


    belongs_to :lms_activity, :class_name => 'LmsActivity', :foreign_key => :activity_id, optional: true
    has_many :lms_questions, through: :lms_activity, :class_name => 'LmsQuestion'
    has_many :latamusers_userquestions, through: :lms_questions, :class_name => 'LatamusersUserquestion'
    has_many :lms_categories, through: :lms_questions, :class_name => 'LmsCategory'

    belongs_to :latamusers_assignedactivity, :class_name => 'LatamusersAssignedactivity', :foreign_key => :assigned_activity_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id, optional: true
    has_many :latamusers_students, through: :auth_user, :class_name => 'LatamusersStudent'

    validates :score, presence: true
    validates :possibleScore, presence: true
    validates :current_try, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Latamusers",
	    priority: 7,
	    permit_params: [
	    	:activity_id,
	    	:assigned_activity_id,
	    	:user_id,
	    	:successfull,
	    	:assigned,
	    	:score,
	    	:possibleScore,
	    	:date,
	    	:current_try
	    ]
	}

end
