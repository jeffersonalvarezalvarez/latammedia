class ReviewOpenQuestion < ActiveRecord::Base
    belongs_to :auth_user_student, :class_name => "AuthUser", :foreign_key => "student_id", optional: :true
    belongs_to :auth_user_teacher, :class_name => "AuthUser", :foreign_key => "teacher_id", optional: :true
    belongs_to :open_question, :class_name => "OpenQuestion", :foreign_key => "open_question_id", optional: :true

    validates :text, presence: true
end
