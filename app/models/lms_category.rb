class LmsCategory < ActiveRecord::Base
    self.table_name = 'LMS_category'


    has_many :lms_questions, :class_name => 'LmsQuestion'
    has_many :latamusers_userquestions, :class_name => 'LatamusersUserquestion'

    validates :name, presence: true
    validates :icon, presence: true
    validates :intro, presence: true
    validates :slug, presence: true
    validates :order, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Lms",
	    priority: 2,
	    permit_params: [
	    	:name,
	    	:icon,
	    	:intro,
	    	:slug,
	    	:order
	    ]
	}

end
