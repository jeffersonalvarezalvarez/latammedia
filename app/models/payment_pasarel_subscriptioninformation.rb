class PaymentPasarelSubscriptioninformation < ActiveRecord::Base
    self.table_name = 'PaymentPasarel_subscriptioninformation'
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Paymentpasarel",
	    priority: 3,
	    permit_params: [
	    	:reference,
	    	:moneda,
	    	:valor,
	    	:respuesta,
	    	:cuentanro,
	    	:metodousado,
	    	:autorizacion,
	    	:nrotransaccion,
	    	:user_id
	    ]
	}

end
