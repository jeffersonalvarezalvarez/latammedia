class CmsPageSite < ActiveRecord::Base
    self.table_name = 'CMS_page_sites'

    belongs_to :django_site, :class_name => 'DjangoSite', :foreign_key => :site_id, optional: true
    belongs_to :cms_page, :class_name => 'CmsPage', :foreign_key => :page_id, optional: true
end
