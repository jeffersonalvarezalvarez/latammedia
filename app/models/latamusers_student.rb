class LatamusersStudent < ActiveRecord::Base
    self.table_name = 'latamusers_student'


    belongs_to :latamusers_usergroup, :class_name => 'LatamusersUsergroup', :foreign_key => :group_id, optional: true
    belongs_to :auth_user_teacher, :class_name => 'AuthUser', :foreign_key => :teacher_id, optional: true
    belongs_to :auth_user_student, :class_name => 'AuthUser', :foreign_key => :user_id, optional: true
    belongs_to :latamusers_intitution, :class_name => 'LatamusersIntitution', :foreign_key => :institution_id, optional: true
    
    validates :user_id, uniqueness: true
    
	manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Latamusers",
	    priority: 3,
	    permit_params: [
	    	:group_id,
	      	:user_id,
	      	:teacher_id,
	      	:institution_id
	    ]
	}

end
