class NewsSectionsidebarcontent < ActiveRecord::Base
    self.table_name = 'News_sectionsidebarcontent'

    has_many :news_sectionsidebarcontent_sections, :class_name => 'NewsSectionsidebarcontentSection', :foreign_key => :sectionsidebarcontent_id, dependent: :destroy
    has_many :news_sections, through: :news_sectionsidebarcontent_sections, :class_name => 'NewsSection'
    belongs_to :news_section, :class_name => 'NewsSection', :foreign_key => :section_to_show_id, optional: true

    validates :number_of_articles, presence: true
    validates :title, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "News",
	    priority: 2,
	    permit_params: [
	      	:title,
	      	:display,
	      	:color,
	      	:number_of_articles,
	      	:section_to_show_id,
			:news_section_ids => []
	    ]
	}

end
