class SequenceUser < ApplicationRecord

	belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :auth_user_id, optional: true
	has_many :latamusers_students, through: :auth_user, :class_name => 'LatamusersStudent'
	
	belongs_to :sequence, :class_name => 'Sequence', :foreign_key => :sequence_id, optional: true
	has_many :sequence_lms_activities, through: :sequence, :class_name => 'SequenceLmsActivity'
    has_many :lms_activities, through: :sequence_lms_activities, :class_name => 'LmsActivity'
	belongs_to :latamusers_assignedsequence, :class_name => 'LatamusersAssignedsequence', :foreign_key => :assigned_sequence_id, optional: true

    validates :current_try, presence: true

end
