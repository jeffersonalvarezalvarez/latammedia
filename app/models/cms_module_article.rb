class CmsModuleArticle < ActiveRecord::Base
    self.table_name = 'CMS_module_articles'

    belongs_to :cms_module, :class_name => 'CmsModule', :foreign_key => :module_id, optional: true
    belongs_to :news_article, :class_name => 'NewsArticle', :foreign_key => :article_id, optional: true
end
