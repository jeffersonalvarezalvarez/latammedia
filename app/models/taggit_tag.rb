class TaggitTag < ActiveRecord::Base
    self.table_name = 'taggit_tag'

    has_many :taggit_taggeditems, :class_name => 'TaggitTaggeditem', :foreign_key => :tag_id
    has_many :ads_ad_tags, :class_name => 'AdsAdTag'
    has_many :ads_ads, through: :ads_ad_tags, :class_name => 'Ads_ad'
    accepts_nested_attributes_for :taggit_taggeditems, allow_destroy: true

    validates :name, presence: true
    validates :slug, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Taggit",
	    priority: 1,
	    permit_params: [
			:name,
			:slug,
			:taggit_taggeditems_attributes => [
        		:id,
        		:_destroy,
        		:content_type_id,
        		:object_id
			]
	    ]
	}


end
