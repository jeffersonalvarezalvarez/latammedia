class OpenQuestion < ApplicationRecord
	belongs_to :auth_user, optional: :true
	belongs_to :lms_question, optional: :true
    has_many :review_open_questions, :class_name => "ReviewOpenQuestion", :foreign_key => :open_question_id, dependent: :destroy

    manage_with_tolaria using: {
        icon: "file-o",
        category: "Lms",
        priority: 8,
        permit_params: [
        	:content,
        	:auth_user_id,
        	:lms_question_id
        ]
    }

end
