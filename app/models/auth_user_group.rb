class AuthUserGroup < ActiveRecord::Base
    belongs_to :auth_group, :class_name => 'AuthGroup', :foreign_key => :group_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id, optional: true
end
