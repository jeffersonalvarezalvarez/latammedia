class LatamusersUserquestion < ActiveRecord::Base
    self.table_name = 'latamusers_userquestion'


    belongs_to :lms_category, :class_name => 'LmsCategory', :foreign_key => :category_id, optional: true
    belongs_to :lms_question, :class_name => 'LmsQuestion', :foreign_key => :question_id, optional: true
    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id, optional: true


    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Latamusers",
	    priority: 6,
	    permit_params: [
	    	:user_id,
	    	:category_id,
	    	:question_id,
	    	:passed,
	    	:assigned,
	    	:phrases_id,
	    	:date
	    ]
	}


end
