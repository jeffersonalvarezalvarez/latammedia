class PaymentPasarelCoupon < ActiveRecord::Base
    self.table_name = 'PaymentPasarel_coupon'

    has_many :paymentpasarel_coupon_categories, :class_name => 'PaymentPasarelCouponCategory', :foreign_key => :coupon_id
    has_many :paymentpasarel_subscriptioncategory, through: :paymentpasarel_coupon_categories, :class_name => 'PaymentPasarelSubscriptioncategory'

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Paymentpasarel",
	    priority: 1,
	    permit_params: [
	    	:reference,
	    	:percentage_discount,
	    	:date_start,
	    	:date_end,
	    	:paymentpasarel_subscriptioncategory_ids => []
	    ]
	}

end
