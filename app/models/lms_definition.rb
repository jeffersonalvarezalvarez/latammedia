class LmsDefinition < ActiveRecord::Base
    self.table_name = 'LMS_definition'

    belongs_to :lms_word, :class_name => 'LmsWord', :foreign_key => :word_id, optional: true
end
