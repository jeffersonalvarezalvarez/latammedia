class NewsSection < ActiveRecord::Base
    self.table_name = 'News_section'


    has_many :news_article_sections, :class_name => 'NewsArticleSection'
    has_many :cms_siteconfigs, :class_name => 'CmsSiteconfig'
    has_many :cms_module_sections, :class_name => 'CmsModuleSection'
    has_many :ads_ad_sections, :class_name => 'AdsAdSection'
    has_many :ads_ads, through: :ads_ad_sections, :class_name => 'Ads_ad'
    has_many :news_sectionsidebarcontent_sections, :class_name => 'NewsSectionsidebarcontentSection'
    has_many :news_sectionsidebarcontents, :class_name => 'NewsSectionsidebarcontent'
    has_many :news_section_sites, :class_name => 'NewsSectionSite', :foreign_key => :section_id
    has_many :django_sites, through: :news_section_sites, :class_name => 'DjangoSite'
    belongs_to :news_section, :class_name => 'NewsSection', :foreign_key => :parent_id, optional: true

    validates :title, presence: true
    validates :slug, presence: true

    manage_with_tolaria using: {
        icon: "file-o",
        category: "News",
        priority: 3,
        permit_params: [
            :title,
            :watch_title,
            :slug,
            :style,
            :color,
            :highlight_featured,
            :parent_id,
            :featured_title,
            :django_site_ids => []
        ]
    }

end
