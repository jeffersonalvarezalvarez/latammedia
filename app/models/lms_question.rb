class LmsQuestion < ActiveRecord::Base
    self.table_name = 'LMS_question'

    belongs_to :lms_question, :class_name => 'LmsQuestion', :foreign_key => :parent, optional: true
    belongs_to :lms_activity, :class_name => 'LmsActivity', :foreign_key => :activity_id, optional: true
    belongs_to :lms_category, :class_name => 'LmsCategory', :foreign_key => :category_id, optional: true
    has_many :lms_answers, :class_name => 'LmsAnswer', :foreign_key => :question_id,  dependent: :destroy
    has_many :lms_questions, :class_name => 'LmsQuestion', :foreign_key => :parent,  dependent: :destroy
    has_many :latamusers_userquestions, :class_name => 'LatamusersUserquestion', :foreign_key => :question_id, dependent: :destroy
    has_many :open_questions, :class_name => 'OpenQuestion', :foreign_key => :lms_question_id, dependent: :destroy
    belongs_to :media_medium, :class_name => 'MediaMedium', :foreign_key => :media_id, optional: true
    belongs_to :question_type, :class_name => 'QuestionType', :foreign_key => :type_id, optional:true
    accepts_nested_attributes_for :lms_answers, allow_destroy: true
    accepts_nested_attributes_for :lms_questions, allow_destroy: true

    validates :text, presence: true
    validates :activity_id, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Lms",
	    priority: 4,
	    permit_params: [
            :definition,
	      	:text,
	      	:activity_id,
	      	:category_id,
	      	:media_id,
            :type_id,
            :order,
            :fill_with_voice,
            :parent,
            :lms_answers_attributes => [
                :id,
                :_destroy,
                :text,
                :Media_media_id,
                :correctAnswer
            ],
	      	:lms_questions_attributes => [
        		:id,
        		:_destroy,
        		:text,
                :activity_id,
                :category_id,
                :media_id,
                :type_id
			]
	    ]
	}

end
