class RegistrationRegistrationprofile < ActiveRecord::Base
    self.table_name = 'registration_registrationprofile'

    belongs_to :auth_user, :class_name => 'AuthUser', :foreign_key => :user_id, optional: true

    manage_with_tolaria using: {
        icon: "file-o",
        category: "Registration",
        priority: 1,
        permit_params: [
            :user_id,
            :activation_key
        ]
    }

end
