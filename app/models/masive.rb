class Masive < ActiveRecord::Base

  has_many :users_masives, :class_name => 'UsersMasive', :foreign_key => :masive_id, dependent: :destroy
  has_many :auth_users, through: :users_masives, :class_name => 'AuthUser'

  has_many :groups_masives, :class_name => 'GroupsMasive', :foreign_key => :masive_id, dependent: :destroy
  has_many :latamusers_usergroups, through: :groups_masives, :class_name => 'LatamusersUsergroup'

  has_many :institutions_masives, :class_name => 'InstitutionsMasive', :foreign_key => :masive_id, dependent: :destroy
  has_many :latamusers_intitutions, through: :institutions_masives, :class_name => 'LatamusersIntitution'
  
  manage_with_tolaria using: {
    icon: "file-o",
    category: "Correos masivos",
    priority: 1,
    permit_params: [
      :template_id,
      :subject,
      :auth_user_ids => [],
      :latamusers_intitution_ids => [],
      :latamusers_usergroup_ids => [],
    ]
  }

end