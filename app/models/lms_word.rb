class LmsWord < ActiveRecord::Base
    self.table_name = 'LMS_word'

    has_many :lms_definitions, :class_name => 'LmsDefinition', :foreign_key => :word_id, dependent: :destroy
    has_many :lms_activity_vocabularies, :class_name => 'LmsActivityVocabulary', :foreign_key => :word_id, dependent: :destroy
    has_many :lms_activities, through: :lms_activity_vocabularies, :class_name => 'LmsActivity'
    has_many :cms_siteconfigs, :class_name => 'CmsSiteconfig', :foreign_key => :word_of_the_day_id, dependent: :destroy
    accepts_nested_attributes_for :lms_definitions, allow_destroy: true

    validates :word, presence: true

    manage_with_tolaria using: {
	    icon: "file-o",
	    category: "Lms",
	    priority: 7,
	    permit_params: [
	    	:word,
	      	:lms_definitions_attributes => [
	      		:id,
        		:_destroy,
        		:use,
        		:definition,
        		:example,
			]
	    ]
	}

    def word_with_use
        self.word.to_s + ' - ' + (self.lms_definitions.first.present? ? self.lms_definitions.first.use : 'not present') 
    end

end
