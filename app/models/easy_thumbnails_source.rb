class EasyThumbnailsSource < ActiveRecord::Base
    self.table_name = 'easy_thumbnails_source'


    has_many :easy_thumbnails_thumbnails, :class_name => 'EasyThumbnailsThumbnail'
end
