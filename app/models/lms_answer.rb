class LmsAnswer < ActiveRecord::Base
    self.table_name = 'LMS_answer'
    attr_accessor :media_url, :media_type


    belongs_to :media_medium, :class_name => 'MediaMedium', :foreign_key => :Media_media_id, optional: true
    belongs_to :lms_question, :class_name => 'LmsQuestion', :foreign_key => :question_id, optional: true

	def media_url
		media = MediaMedium.find_by_id(self.Media_media_id)
		@media_url || if media.present? then media.media.url.sub('/uploads/uploads/','/uploads/') else '' end
	end

	def media_type
		media = MediaMedium.find_by_id(self.Media_media_id)
		@media_url || if media.present? then media.type else '' end
	end

end
