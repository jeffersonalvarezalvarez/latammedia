source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Para corrector de spelling y gramática
gem 'after_the_deadline'
# Para usar tags
gem 'tagsinput-rails'
# Para la transcripción de audio a texto
gem 'google-cloud-speech'
# Para transcribir audio a texto
gem 'aws-sdk-polly'
# Para compilarlos assets del proyecto
gem 'webpacker'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.1'
# Use sqlite3 as the database for Active Record
gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
gem 'toastr-rails'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Administrador
gem 'tolaria', '~> 2.0'
# Imágenes y archivos 
gem 'carrierwave'
# Para amazon web services
gem 'fog'
# Imágenes
gem 'mini_magick'
# Texto enriquecido
gem 'redcarpet', '~> 3.0.0'
# JQuery
gem 'jquery-rails'
# Ruby API Builder Language rendering resources in different format (JSON, XML, BSON, ...)
gem 'rabl-rails'
# JSON
gem 'json', '~> 1.8', '>= 1.8.3'
# Autenticación de usuarios
gem 'devise'
# Encriptación
gem 'devise-encryptable'
# Hashing algorithm
gem 'pbkdf2_password_hasher'
# Cron job  tareas automatizadas
gem 'whenever'
# AWS
# gem 'aws-sdk', '~> 2'
# Texto enriquecido 
gem "ckeditor"
# Para importar usuarios desde Excel y csv
gem "roo", "~> 2.7.0"
# Gema para evitar errores en la importación de archivos muy pesados
gem 'activerecord-session_store'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
