# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180712200123) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "Ads_ad", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.string "type", limit: 25, null: false
    t.text "script", null: false
    t.string "image", limit: 100, null: false
    t.text "include_code", null: false
    t.string "position", limit: 255, null: false
    t.string "display", limit: 20, null: false
    t.string "link", limit: 200, null: false
    t.integer "order", null: false
  end

  create_table "Ads_ad_articles", id: :serial, force: :cascade do |t|
    t.integer "ad_id", null: false
    t.integer "article_id", null: false
    t.index ["ad_id", "article_id"], name: "Ads_ad_articles_ad_id_key", unique: true
    t.index ["ad_id"], name: "Ads_ad_articles_ad_id"
    t.index ["article_id"], name: "Ads_ad_articles_article_id"
  end

  create_table "Ads_ad_pages", id: :serial, force: :cascade do |t|
    t.integer "ad_id", null: false
    t.integer "page_id", null: false
    t.index ["ad_id", "page_id"], name: "Ads_ad_pages_ad_id_key", unique: true
    t.index ["ad_id"], name: "Ads_ad_pages_ad_id"
    t.index ["page_id"], name: "Ads_ad_pages_page_id"
  end

  create_table "Ads_ad_sections", id: :serial, force: :cascade do |t|
    t.integer "ad_id", null: false
    t.integer "section_id", null: false
    t.index ["ad_id", "section_id"], name: "Ads_ad_sections_ad_id_key", unique: true
    t.index ["ad_id"], name: "Ads_ad_sections_ad_id"
    t.index ["section_id"], name: "Ads_ad_sections_section_id"
  end

  create_table "Ads_ad_sites", id: :serial, force: :cascade do |t|
    t.integer "ad_id", null: false
    t.integer "site_id", null: false
    t.index ["ad_id", "site_id"], name: "Ads_ad_sites_ad_id_key", unique: true
    t.index ["ad_id"], name: "Ads_ad_sites_ad_id"
    t.index ["site_id"], name: "Ads_ad_sites_site_id"
  end

  create_table "Ads_ad_tags", id: :serial, force: :cascade do |t|
    t.integer "ad_id", null: false
    t.integer "tag_id", null: false
    t.index ["ad_id", "tag_id"], name: "Ads_ad_tags_ad_id_key", unique: true
    t.index ["ad_id"], name: "Ads_ad_tags_ad_id"
    t.index ["tag_id"], name: "Ads_ad_tags_tag_id"
  end

  create_table "Blog_entry", id: :serial, force: :cascade do |t|
    t.datetime "pubDate", null: false
    t.datetime "upDate", null: false
    t.string "title", limit: 255, null: false
    t.string "slug", limit: 255, null: false
    t.text "content", null: false
    t.string "author", limit: 255, null: false
    t.string "author_email", limit: 75, null: false
    t.boolean "published", null: false
    t.boolean "featured", null: false
    t.boolean "show_email", null: false
    t.boolean "show_comments", null: false
    t.index "slug varchar_pattern_ops", name: "Blog_entry_slug_like"
    t.index ["slug"], name: "Blog_entry_slug"
  end

  create_table "CMS_menu", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.string "url", limit: 255, null: false
    t.integer "parent_id"
    t.integer "order", null: false
    t.string "color", limit: 15, null: false
    t.integer "site_id", null: false
    t.boolean "published", null: false
    t.string "location", limit: 20, null: false
    t.index ["parent_id"], name: "CMS_menu_parent_id"
    t.index ["site_id"], name: "CMS_menu_site_id"
  end

  create_table "CMS_module", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.text "html", null: false
    t.text "javascript", null: false
    t.string "js_file", limit: 100, null: false
    t.text "css", null: false
    t.string "css_file", limit: 100, null: false
    t.integer "order", null: false
    t.string "position", limit: 20, null: false
    t.string "display", limit: 20, null: false
  end

  create_table "CMS_module_articles", id: :serial, force: :cascade do |t|
    t.integer "module_id", null: false
    t.integer "article_id", null: false
    t.index ["article_id"], name: "CMS_module_articles_article_id"
    t.index ["module_id", "article_id"], name: "CMS_module_articles_module_id_key", unique: true
    t.index ["module_id"], name: "CMS_module_articles_module_id"
  end

  create_table "CMS_module_pages", id: :serial, force: :cascade do |t|
    t.integer "module_id", null: false
    t.integer "page_id", null: false
    t.index ["module_id", "page_id"], name: "CMS_module_pages_module_id_key", unique: true
    t.index ["module_id"], name: "CMS_module_pages_module_id"
    t.index ["page_id"], name: "CMS_module_pages_page_id"
  end

  create_table "CMS_module_sections", id: :serial, force: :cascade do |t|
    t.integer "module_id", null: false
    t.integer "section_id", null: false
    t.index ["module_id", "section_id"], name: "CMS_module_sections_module_id_key", unique: true
    t.index ["module_id"], name: "CMS_module_sections_module_id"
    t.index ["section_id"], name: "CMS_module_sections_section_id"
  end

  create_table "CMS_module_sites", id: :serial, force: :cascade do |t|
    t.integer "module_id", null: false
    t.integer "site_id", null: false
    t.index ["module_id", "site_id"], name: "CMS_module_sites_module_id_53b07ca25f423846_uniq", unique: true
    t.index ["module_id"], name: "CMS_module_sites_module_id"
    t.index ["site_id"], name: "CMS_module_sites_site_id"
  end

  create_table "CMS_module_tags", id: :serial, force: :cascade do |t|
    t.integer "module_id", null: false
    t.integer "tag_id", null: false
    t.index ["module_id", "tag_id"], name: "CMS_module_tags_module_id_key", unique: true
    t.index ["module_id"], name: "CMS_module_tags_module_id"
    t.index ["tag_id"], name: "CMS_module_tags_tag_id"
  end

  create_table "CMS_page", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.text "content", null: false
    t.text "excerpt", null: false
    t.datetime "pubDate", null: false
    t.datetime "upDate", null: false
    t.boolean "featured", null: false
    t.boolean "published", null: false
    t.string "slug", limit: 50, null: false
    t.index "slug varchar_pattern_ops", name: "CMS_page_slug_like"
    t.index ["slug"], name: "CMS_page_slug"
  end

  create_table "CMS_page_sites", id: :serial, force: :cascade do |t|
    t.integer "page_id", null: false
    t.integer "site_id", null: false
    t.index ["page_id", "site_id"], name: "CMS_page_sites_page_id_key", unique: true
    t.index ["page_id"], name: "CMS_page_sites_page_id"
    t.index ["site_id"], name: "CMS_page_sites_site_id"
  end

  create_table "CMS_siteconfig", id: :serial, force: :cascade do |t|
    t.integer "site_id", null: false
    t.string "facebook", limit: 200, null: false
    t.string "twitter", limit: 200, null: false
    t.string "youtube", limit: 200, null: false
    t.string "linkedin", limit: 200, null: false
    t.boolean "highlight_featured", null: false
    t.string "featured_title", limit: 255, null: false
    t.string "google_analytics", limit: 20, null: false
    t.integer "watch_module_category_id"
    t.string "category_link_color", limit: 30, null: false
    t.integer "word_of_the_day_id"
    t.integer "quote_id"
    t.index ["quote_id"], name: "CMS_siteconfig_quote_id"
    t.index ["site_id"], name: "CMS_siteconfig_site_id_key", unique: true
    t.index ["watch_module_category_id"], name: "CMS_siteconfig_watch_module_category_id"
    t.index ["word_of_the_day_id"], name: "CMS_siteconfig_word_of_the_day_id"
  end

  create_table "Caracterization_caracterizationcategory", id: :serial, force: :cascade do |t|
    t.string "detail", limit: 255, null: false
    t.string "description", limit: 255, null: false
    t.index "detail varchar_pattern_ops", name: "Caracterization_caracterizationcategory_detail_like"
  end

  create_table "Caracterization_caracterizationitem", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.boolean "state", null: false
    t.integer "category_id", null: false
    t.index ["category_id"], name: "Caracterization_caracterizationitem_category_id"
  end

  create_table "Caracterization_citemclientrelation", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "citem_id", null: false
    t.boolean "current_state", null: false
    t.index ["citem_id"], name: "Caracterization_citemclientrelation_citem_id"
    t.index ["user_id"], name: "Caracterization_citemclientrelation_user_id"
  end

  create_table "LMS_activity", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.integer "difficulty_id", null: false
    t.text "introduction", null: false
    t.integer "course_id", null: false
    t.integer "article_id"
    t.integer "created_by_id", null: false
    t.boolean "public", null: false
    t.integer "reward", null: false
    t.integer "time"
    t.boolean "published", null: false
    t.boolean "private_content", default: false, null: false
    t.index ["article_id"], name: "LMS_activity_article_id"
    t.index ["course_id"], name: "LMS_activity_course_id"
    t.index ["created_by_id"], name: "LMS_activity_created_by_id"
    t.index ["difficulty_id"], name: "LMS_activity_difficulty_id"
  end

  create_table "LMS_activity_vocabulary", id: :serial, force: :cascade do |t|
    t.integer "activity_id", null: false
    t.integer "word_id", null: false
    t.index ["activity_id", "word_id"], name: "LMS_activity_vocabulary_activity_id_3a5c80bf6edf4153_uniq", unique: true
    t.index ["activity_id"], name: "LMS_activity_vocabulary_activity_id"
    t.index ["word_id"], name: "LMS_activity_vocabulary_word_id"
  end

  create_table "LMS_answer", id: :serial, force: :cascade do |t|
    t.integer "question_id", null: false
    t.text "text", null: false
    t.boolean "correctAnswer", null: false
    t.bigint "Media_media_id"
    t.index ["Media_media_id"], name: "index_LMS_answer_on_Media_media_id"
    t.index ["question_id"], name: "LMS_answer_question_id"
  end

  create_table "LMS_category", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.text "icon", null: false
    t.text "intro", null: false
    t.string "slug", limit: 50, null: false
    t.integer "order"
    t.index "slug varchar_pattern_ops", name: "LMS_category_slug_like"
    t.index ["slug"], name: "LMS_category_slug"
  end

  create_table "LMS_course", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
  end

  create_table "LMS_definition", id: :serial, force: :cascade do |t|
    t.integer "word_id", null: false
    t.string "use", limit: 255, null: false
    t.text "definition", null: false
    t.text "example", null: false
    t.index ["word_id"], name: "LMS_definition_word_id"
  end

  create_table "LMS_level", id: :serial, force: :cascade do |t|
    t.string "difficulty_level", limit: 10, null: false
    t.string "color", limit: 20, null: false
  end

  create_table "LMS_question", id: :serial, force: :cascade do |t|
    t.integer "activity_id", null: false
    t.text "text", null: false
    t.integer "media_id"
    t.integer "category_id", null: false
    t.integer "type_id", default: 1, null: false, comment: "id del tipo de pregunta"
    t.integer "parent", comment: "Si es pregunta de tipo drag and drop este campo indica a que palabra está asociada, si esta vacia es la palabra a la que las demás hace referencia."
    t.boolean "fill_with_voice"
    t.text "definition"
    t.integer "order"
    t.index ["activity_id"], name: "LMS_question_activity_id"
    t.index ["category_id"], name: "LMS_question_category_id"
    t.index ["media_id"], name: "LMS_question_media_id"
  end

  create_table "LMS_quote", id: :serial, force: :cascade do |t|
    t.text "quote", null: false
    t.string "author", limit: 255, null: false
  end

  create_table "LMS_slide", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.text "text", null: false
    t.integer "image_id", null: false
    t.index ["image_id"], name: "LMS_slide_image_id"
  end

  create_table "LMS_word", id: :serial, force: :cascade do |t|
    t.string "word", limit: 255, null: false
  end

  create_table "Media_gallery", id: :serial, force: :cascade do |t|
    t.integer "content_type_id", null: false
    t.integer "object_id", null: false
    t.string "style", default: "", null: false
    t.string "position", default: "", null: false
    t.index ["content_type_id"], name: "Media_gallery_content_type_id"
  end

  create_table "Media_gallery_media", id: :serial, force: :cascade do |t|
    t.integer "gallery_id", null: false
    t.integer "media_id", null: false
    t.index ["gallery_id", "media_id"], name: "Media_gallery_media_gallery_id_key", unique: true
    t.index ["gallery_id"], name: "Media_gallery_media_gallery_id"
    t.index ["media_id"], name: "Media_gallery_media_media_id"
  end

  create_table "Media_media", id: :serial, force: :cascade do |t|
    t.string "media", default: "", null: false
    t.string "poster", default: "", null: false
    t.string "title", limit: 255, null: false
    t.string "author", limit: 100, null: false
    t.string "type", limit: 10, null: false
    t.string "iframe_url", limit: 200, null: false
  end

  create_table "News_article", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.text "content", null: false
    t.text "excerpt", null: false
    t.string "author", limit: 255, null: false
    t.string "source", limit: 255, null: false
    t.string "source_url", limit: 200, null: false
    t.datetime "pubDate", null: false
    t.datetime "upDate", null: false
    t.string "slug", limit: 50, null: false
    t.boolean "featured", null: false
    t.boolean "show_link_to_post", null: false
    t.string "description", limit: 255
    t.boolean "watch", null: false
    t.index "slug varchar_pattern_ops", name: "News_article_slug_like"
    t.index ["slug"], name: "News_article_slug"
  end

  create_table "News_article_section", id: :serial, force: :cascade do |t|
    t.integer "article_id", null: false
    t.integer "section_id", null: false
    t.index ["article_id", "section_id"], name: "News_article_section_article_id_key", unique: true
    t.index ["article_id"], name: "News_article_section_article_id"
    t.index ["section_id"], name: "News_article_section_section_id"
  end

  create_table "News_layout", id: :integer, default: -> { "nextval('news_layout_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "article_id"
    t.integer "site_id"
    t.boolean "editors_pick"
    t.string "column_layout", limit: 10
    t.boolean "published"
    t.boolean "show_comments"
    t.datetime "schedulePublication"
    t.integer "hits"
  end

  create_table "News_layout1", id: false, force: :cascade do |t|
    t.integer "id", default: -> { "nextval('\"News_layout_id_seq\"'::regclass)" }, null: false
    t.integer "article_id", null: false
    t.integer "site_id", null: false
    t.boolean "editors_pick", null: false
    t.string "column_layout", limit: 10
    t.boolean "published", null: false
    t.boolean "show_comments", null: false
    t.datetime "schedulePublication"
    t.integer "hits", null: false
  end

  create_table "News_section", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.string "slug", limit: 50, null: false
    t.string "style", limit: 20, null: false
    t.boolean "highlight_featured", null: false
    t.string "color", limit: 20, null: false
    t.integer "parent_id"
    t.string "featured_title", limit: 255
    t.string "watch_title", limit: 120, null: false
    t.index "slug varchar_pattern_ops", name: "News_section_slug_like"
    t.index ["parent_id"], name: "News_section_parent_id"
    t.index ["slug"], name: "News_section_slug"
  end

  create_table "News_section_sites", id: :serial, force: :cascade do |t|
    t.integer "section_id", null: false
    t.integer "site_id", null: false
    t.index ["section_id", "site_id"], name: "News_section_sites_section_id_key", unique: true
    t.index ["section_id"], name: "News_section_sites_section_id"
    t.index ["site_id"], name: "News_section_sites_site_id"
  end

  create_table "News_sectionsidebarcontent", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.string "display", limit: 20, null: false
    t.string "color", limit: 20, null: false
    t.integer "number_of_articles", null: false
    t.integer "section_to_show_id"
    t.index ["section_to_show_id"], name: "News_sectionsidebarcontent_section_to_show_id"
  end

  create_table "News_sectionsidebarcontent_sections", id: :serial, force: :cascade do |t|
    t.integer "sectionsidebarcontent_id", null: false
    t.integer "section_id", null: false
    t.index ["section_id"], name: "News_sectionsidebarcontent_sections_section_id"
    t.index ["sectionsidebarcontent_id", "section_id"], name: "News_sectionside_sectionsidebarcontent_id_5700db963b528aaf_uniq", unique: true
    t.index ["sectionsidebarcontent_id"], name: "News_sectionsidebarcontent_sections_sectionsidebarcontent_id"
  end

  create_table "PaymentPasarel_coupon", id: :serial, force: :cascade do |t|
    t.string "reference", limit: 255, null: false
    t.decimal "percentage_discount", precision: 4, scale: 1, null: false
    t.date "date_start", null: false
    t.date "date_end", null: false
    t.index "reference varchar_pattern_ops", name: "PaymentPasarel_coupon_reference_like"
    t.index ["reference"], name: "PaymentPasarel_coupon_reference_key", unique: true
  end

  create_table "PaymentPasarel_coupon_categories", id: :serial, force: :cascade do |t|
    t.integer "coupon_id", null: false
    t.integer "subscriptioncategory_id", null: false
    t.index ["coupon_id", "subscriptioncategory_id"], name: "PaymentPasarel_coupon_categorie_coupon_id_7797bddfefc8305f_uniq", unique: true
    t.index ["coupon_id"], name: "PaymentPasarel_coupon_categories_coupon_id"
    t.index ["subscriptioncategory_id"], name: "PaymentPasarel_coupon_categories_subscriptioncategory_id"
  end

  create_table "PaymentPasarel_subscriptioncategory", id: :serial, force: :cascade do |t|
    t.string "description", limit: 255, null: false
    t.string "name", limit: 255, null: false
    t.integer "value", null: false
    t.integer "duration", null: false
    t.boolean "state", null: false
    t.decimal "iva", precision: 6, scale: 2, null: false
  end

  create_table "PaymentPasarel_subscriptioninformation", id: :serial, force: :cascade do |t|
    t.string "reference", limit: 255, null: false
    t.string "moneda", limit: 255, null: false
    t.string "valor", limit: 255, null: false
    t.string "respuesta", limit: 255, null: false
    t.string "cuentanro", limit: 255, null: false
    t.string "metodousado", limit: 255, null: false
    t.string "autorizacion", limit: 255, null: false
    t.string "nrotransaccion", limit: 255, null: false
    t.integer "user_id", null: false
    t.index ["user_id"], name: "PaymentPasarel_subscriptioninformation_user_id"
  end

  create_table "activity_institutions", id: :integer, default: -> { "nextval('activity_institutions_seq'::regclass)" }, comment: "Llave primaria de la tabla ", force: :cascade do |t|
    t.integer "activity_id", null: false, comment: "Referencia a la tabla Lms_activity"
    t.integer "institution_id", null: false, comment: "Referencia a la tabla Latamusers_institutions"
  end

  create_table "admin_user_groups", force: :cascade do |t|
    t.bigint "auth_users_id"
    t.string "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["auth_users_id"], name: "index_admin_user_groups_on_auth_users_id"
  end

  create_table "administrators", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "auth_token", limit: 32, null: false
    t.string "name", null: false
    t.string "username", null: false
    t.integer "auth_user_id"
    t.index ["auth_token"], name: "index_administrators_on_auth_token"
    t.index ["email"], name: "index_administrators_on_email", unique: true
    t.index ["reset_password_token"], name: "index_administrators_on_reset_password_token", unique: true
  end

  create_table "auth_group", id: :serial, force: :cascade do |t|
    t.string "name", limit: 80, null: false
    t.index "name varchar_pattern_ops", name: "auth_group_name_like"
    t.index ["name"], name: "auth_group_name_key", unique: true
  end

  create_table "auth_group_permissions", id: :serial, force: :cascade do |t|
    t.integer "group_id", null: false
    t.integer "permission_id", null: false
    t.index ["group_id", "permission_id"], name: "auth_group_permissions_group_id_key", unique: true
    t.index ["group_id"], name: "auth_group_permissions_group_id"
    t.index ["permission_id"], name: "auth_group_permissions_permission_id"
  end

  create_table "auth_permission", id: :serial, force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "content_type_id", null: false
    t.string "codename", limit: 100, null: false
    t.index ["content_type_id", "codename"], name: "auth_permission_content_type_id_key", unique: true
    t.index ["content_type_id"], name: "auth_permission_content_type_id"
  end

  create_table "auth_user_groups", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "group_id", null: false
    t.index ["group_id"], name: "auth_user_groups_group_id"
    t.index ["user_id", "group_id"], name: "auth_user_groups_user_id_key", unique: true
    t.index ["user_id"], name: "auth_user_groups_user_id"
  end

  create_table "auth_user_user_permissions", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "permission_id", null: false
    t.index ["permission_id"], name: "auth_user_user_permissions_permission_id"
    t.index ["user_id", "permission_id"], name: "auth_user_user_permissions_user_id_key", unique: true
    t.index ["user_id"], name: "auth_user_user_permissions_user_id"
  end

  create_table "auth_users", id: :serial, force: :cascade do |t|
    t.string "encrypted_password", limit: 128, null: false
    t.datetime "last_sign_in_at", null: false
    t.boolean "is_superuser", default: false, null: false
    t.string "username", limit: 30, null: false
    t.string "first_name", limit: 30, null: false
    t.string "last_name", limit: 30, null: false
    t.string "email", limit: 75, null: false
    t.boolean "is_staff", default: false, null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "date_joined", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.index "username varchar_pattern_ops", name: "auth_user_username_like"
    t.index ["confirmation_token"], name: "index_auth_users_on_confirmation_token", unique: true
    t.index ["reset_password_token"], name: "index_auth_users_on_reset_password_token", unique: true
    t.index ["username"], name: "auth_user_username_key", unique: true
  end

  create_table "category_institution", id: :integer, default: -> { "nextval('category_institutions_seq'::regclass)" }, comment: "Llave primaria de la tabla ", force: :cascade do |t|
    t.integer "section_id", null: false, comment: "Referencia a la tabla News_section"
    t.integer "institution_id", null: false, comment: "Referencia a la tabla Latamusers_institutions"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "countries", force: :cascade do |t|
    t.string "code", null: false
    t.string "country", null: false
  end

  create_table "django_admin_log", id: :serial, force: :cascade do |t|
    t.datetime "action_time", null: false
    t.integer "user_id", null: false
    t.integer "content_type_id"
    t.text "object_id"
    t.string "object_repr", limit: 200, null: false
    t.integer "action_flag", limit: 2, null: false
    t.text "change_message", null: false
    t.index ["content_type_id"], name: "django_admin_log_content_type_id"
    t.index ["user_id"], name: "django_admin_log_user_id"
  end

  create_table "django_content_type", id: :serial, force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.string "app_label", limit: 100, null: false
    t.string "model", limit: 100, null: false
    t.index ["app_label", "model"], name: "django_content_type_app_label_key", unique: true
  end

  create_table "django_cron_cronjoblog", id: :serial, force: :cascade do |t|
    t.string "code", limit: 64, null: false
    t.datetime "start_time", null: false
    t.datetime "end_time", null: false
    t.boolean "is_success", null: false
    t.text "message", null: false
    t.time "ran_at_time"
    t.index "code varchar_pattern_ops", name: "django_cron_cronjoblog_code_like"
    t.index ["code"], name: "django_cron_cronjoblog_code"
    t.index ["end_time"], name: "django_cron_cronjoblog_end_time"
    t.index ["ran_at_time", "is_success", "code"], name: "django_cron_cronjoblog_ran_at_time_4ea210bda4c6e3ce"
    t.index ["ran_at_time", "start_time", "code"], name: "django_cron_cronjoblog_ran_at_time_443d2262d93762b6"
    t.index ["ran_at_time"], name: "django_cron_cronjoblog_ran_at_time"
    t.index ["start_time", "code"], name: "django_cron_cronjoblog_start_time_6a2e23b661afe427"
    t.index ["start_time"], name: "django_cron_cronjoblog_start_time"
  end

  create_table "django_session", primary_key: "session_key", id: :string, limit: 40, force: :cascade do |t|
    t.text "session_data", null: false
    t.datetime "expire_date", null: false
    t.index "session_key varchar_pattern_ops", name: "django_session_session_key_like"
    t.index ["expire_date"], name: "django_session_expire_date"
  end

  create_table "django_site", id: :serial, force: :cascade do |t|
    t.string "domain", limit: 100, null: false
    t.string "name", limit: 50, null: false
  end

  create_table "easy_thumbnails_source", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.datetime "modified", null: false
    t.string "storage_hash", limit: 40, null: false
    t.index ["name", "storage_hash"], name: "easy_thumbnails_source_name_7549c98cc6dd6969_uniq", unique: true
    t.index ["name"], name: "easy_thumbnails_source_name"
    t.index ["storage_hash"], name: "easy_thumbnails_source_storage_hash"
  end

  create_table "easy_thumbnails_thumbnail", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.datetime "modified", null: false
    t.integer "source_id", null: false
    t.string "storage_hash", limit: 40, null: false
    t.index ["name"], name: "easy_thumbnails_thumbnail_name"
    t.index ["source_id", "name", "storage_hash"], name: "easy_thumbnails_thumbnail_source_id_1f50d53db8191480_uniq", unique: true
    t.index ["source_id"], name: "easy_thumbnails_thumbnail_source_id"
    t.index ["storage_hash"], name: "easy_thumbnails_thumbnail_storage_hash"
  end

  create_table "easy_thumbnails_thumbnaildimensions", id: :serial, force: :cascade do |t|
    t.integer "thumbnail_id", null: false
    t.integer "width"
    t.integer "height"
    t.index ["thumbnail_id"], name: "easy_thumbnails_thumbnaildimensions_thumbnail_id_key", unique: true
  end

  create_table "groups_masives", force: :cascade do |t|
    t.string "latamusers_usergroup_id"
    t.bigint "masive_id"
    t.index ["masive_id"], name: "index_groups_masives_on_masive_id"
  end

  create_table "instant_phrases_frase", id: :integer, default: -> { "nextval('id_frase_seq'::regclass)" }, force: :cascade do |t|
    t.string "imagen", limit: 255, comment: "url de la imagen"
    t.integer "nivel", null: false
    t.integer "tipo", null: false, comment: "tipo de frase"
  end

  create_table "instant_phrases_palabra", id: :integer, default: -> { "nextval('id_palabra_seq'::regclass)" }, force: :cascade do |t|
    t.string "sintaxis", null: false
    t.string "definicion", default: "false", null: false
    t.integer "tipo", comment: "1. verbo, 2. Adjetivo, 3. Adverbio..... etc. Completar según requerimientos"
  end

  create_table "instant_phrases_parte", id: :integer, default: -> { "nextval('id_frase_seq'::regclass)" }, force: :cascade, comment: "Parte a la que corresponde la palabra dentro de la frase" do |t|
    t.integer "frase_id", null: false, comment: "frase a la que pertenece"
    t.integer "posicion", null: false, comment: "posicion correspondiente dentro de la frase"
    t.integer "opciones", default: 3, null: false, comment: "Cantidad de palabras que muestra como posibles soluciones"
    t.integer "palabra_id", comment: "llave foránea a palabra"
  end

  create_table "instant_phrases_tipo_frase", id: :integer, default: -> { "nextval('id_tipo_frase_seq'::regclass)" }, comment: "identificador del registro", force: :cascade, comment: "Contiene los tipos de frases que se pueden generar" do |t|
    t.text "nombre", null: false, comment: "nombre del tipo de frase"
  end

  create_table "institutions_masives", force: :cascade do |t|
    t.bigint "latamusers_intitution_id"
    t.bigint "masive_id"
    t.index ["latamusers_intitution_id"], name: "index_institutions_masives_on_latamusers_intitution_id"
    t.index ["masive_id"], name: "index_institutions_masives_on_masive_id"
  end

  create_table "latamusers_assignedactivity", id: :serial, force: :cascade do |t|
    t.integer "teacher_id", null: false
    t.integer "activity_id"
    t.integer "maximum_tries", null: false
    t.date "creation_date", null: false
    t.date "start_date", null: false
    t.date "end_date", null: false
    t.boolean "active", null: false
    t.index ["activity_id"], name: "latamusers_assignedactivity_activity_id"
    t.index ["teacher_id"], name: "latamusers_assignedactivity_teacher_id"
  end

  create_table "latamusers_assignedsequences", force: :cascade do |t|
    t.integer "sequence_id"
    t.integer "maximum_tries"
    t.date "start_date"
    t.date "end_date"
    t.integer "teacher_id"
    t.date "creation_date"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "latamusers_intitutions", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.string "nit", limit: 50, null: false
    t.string "contact_name", limit: 50, null: false
    t.string "contact_phone", limit: 30, null: false
    t.string "contact_email", limit: 50, null: false
    t.string "logo"
  end

  create_table "latamusers_student", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "teacher_id", null: false
    t.string "group_id", limit: 255
    t.integer "institution_id", null: false
    t.index "group_id varchar_pattern_ops", name: "latamusers_student_group_id_like"
    t.index ["group_id"], name: "latamusers_student_group_id"
    t.index ["institution_id"], name: "latamusers_student_institution_id"
    t.index ["teacher_id"], name: "latamusers_student_teacher_id"
    t.index ["user_id"], name: "latamusers_student_user_id_key", unique: true
  end

  create_table "latamusers_useractivity", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "activity_id", null: false
    t.integer "score", null: false
    t.integer "possibleScore", null: false
    t.datetime "date", null: false
    t.boolean "successfull", null: false
    t.boolean "assigned", null: false
    t.integer "assigned_activity_id"
    t.integer "current_try", null: false
    t.index ["activity_id"], name: "latamusers_useractivity_activity_id"
    t.index ["assigned_activity_id"], name: "latamusers_useractivity_assigned_activity_id"
    t.index ["user_id"], name: "latamusers_useractivity_user_id"
  end

  create_table "latamusers_usergroups", primary_key: "code", id: :string, limit: 255, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.integer "teacher_id"
    t.integer "identificador", default: -> { "nextval('latamusers_usergroups_id_seq'::regclass)" }, null: false
    t.index "code varchar_pattern_ops", name: "latamusers_usergroup_code_like"
    t.index ["identificador"], name: "index_latamusers_usergroups_on_identificador", unique: true
    t.index ["teacher_id"], name: "latamusers_usergroup_teacher_id"
  end

  create_table "latamusers_userprofile", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "thumbnail", limit: 100
    t.string "type", limit: 10, null: false
    t.string "city", limit: 255
    t.string "country", limit: 255
    t.string "gender", limit: 1
    t.date "birthday"
    t.string "english_level", limit: 144, null: false
    t.string "education", limit: 144, null: false
    t.string "ocupation", limit: 144, null: false
    t.boolean "subscripted", null: false
    t.date "date_subscription", null: false
    t.date "date_end", null: false
    t.index ["user_id"], name: "latamusers_userprofile_user_id_key", unique: true
  end

  create_table "latamusers_userquestion", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "question_id", null: false
    t.integer "category_id", null: false
    t.boolean "passed", null: false
    t.date "date", null: false
    t.boolean "assigned", null: false
    t.integer "phrases_id", comment: "foreing key to palabra of instant phrases"
    t.index ["category_id"], name: "latamusers_userquestion_category_id"
    t.index ["question_id"], name: "latamusers_userquestion_question_id"
    t.index ["user_id"], name: "latamusers_userquestion_user_id"
  end

  create_table "masives", force: :cascade do |t|
    t.boolean "success"
    t.string "subject"
    t.integer "template_id"
  end

  create_table "nested_admin_group", id: :serial, force: :cascade do |t|
    t.string "slug", limit: 128, null: false
  end

  create_table "nested_admin_item", id: :serial, force: :cascade do |t|
    t.string "name", limit: 128, null: false
    t.integer "section_id", null: false
    t.integer "position", null: false
    t.index ["section_id"], name: "nested_admin_item_section_id"
  end

  create_table "nested_admin_section", id: :serial, force: :cascade do |t|
    t.string "slug", limit: 128, null: false
    t.integer "position", null: false
    t.integer "group_id", null: false
    t.index ["group_id"], name: "nested_admin_section_group_id"
  end

  create_table "news_layout2", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "article_id"
    t.integer "site_id"
    t.boolean "editors_pick"
    t.string "column_layout", limit: 10
    t.boolean "published"
    t.boolean "show_comments"
    t.datetime "schedulePublication"
    t.integer "hits"
  end

  create_table "news_layout3", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "article_id"
    t.integer "site_id"
    t.boolean "editors_pick"
    t.string "column_layout", limit: 10
    t.boolean "published"
    t.boolean "show_comments"
    t.datetime "schedulePublication"
    t.integer "hits"
  end

  create_table "news_layoutold", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "article_id"
    t.integer "site_id"
    t.boolean "editors_pick"
    t.string "column_layout", limit: 10
    t.boolean "published"
    t.boolean "show_comments"
    t.datetime "schedulePublication"
    t.integer "hits"
  end

  create_table "open_questions", force: :cascade do |t|
    t.text "content"
    t.bigint "auth_user_id"
    t.bigint "lms_question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "content_errors"
    t.index ["auth_user_id"], name: "index_open_questions_on_auth_user_id"
    t.index ["lms_question_id"], name: "index_open_questions_on_lms_question_id"
  end

  create_table "question_type", id: :serial, force: :cascade, comment: "Tipos de preguntas \"Drag and drop\", \"Completar texto\", etc" do |t|
    t.string "name", limit: 255, null: false, comment: "Nombre del tipo de actividad"
  end

  create_table "registration_registrationprofile", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "activation_key", limit: 40, null: false
    t.index ["user_id"], name: "registration_registrationprofile_user_id_key", unique: true
  end

  create_table "report_builder_displayfield", id: :serial, force: :cascade do |t|
    t.integer "report_id", null: false
    t.string "path", limit: 2000, null: false
    t.string "path_verbose", limit: 2000, null: false
    t.string "field", limit: 2000, null: false
    t.string "field_verbose", limit: 2000, null: false
    t.string "name", limit: 2000, null: false
    t.integer "sort"
    t.boolean "sort_reverse", null: false
    t.integer "width", null: false
    t.string "aggregate", limit: 5, null: false
    t.integer "position", limit: 2
    t.boolean "total", null: false
    t.boolean "group", null: false
    t.integer "display_format_id"
    t.index ["display_format_id"], name: "report_builder_displayfield_display_format_id"
    t.index ["report_id"], name: "report_builder_displayfield_report_id"
  end

  create_table "report_builder_filterfield", id: :serial, force: :cascade do |t|
    t.integer "report_id", null: false
    t.string "path", limit: 2000, null: false
    t.string "path_verbose", limit: 2000, null: false
    t.string "field", limit: 2000, null: false
    t.string "field_verbose", limit: 2000, null: false
    t.string "filter_type", limit: 20, null: false
    t.string "filter_value", limit: 2000, null: false
    t.string "filter_value2", limit: 2000, null: false
    t.boolean "exclude", null: false
    t.integer "position", limit: 2
    t.index ["report_id"], name: "report_builder_filterfield_report_id"
  end

  create_table "report_builder_format", id: :serial, force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.string "string", limit: 300, null: false
  end

  create_table "report_builder_report", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.string "slug", limit: 50, null: false
    t.text "description", null: false
    t.integer "root_model_id", null: false
    t.date "created", null: false
    t.date "modified", null: false
    t.integer "user_created_id"
    t.integer "user_modified_id"
    t.boolean "distinct", null: false
    t.string "report_file", limit: 100, null: false
    t.datetime "report_file_creation"
    t.index "slug varchar_pattern_ops", name: "report_builder_report_slug_like"
    t.index ["root_model_id"], name: "report_builder_report_root_model_id"
    t.index ["slug"], name: "report_builder_report_slug"
    t.index ["user_created_id"], name: "report_builder_report_user_created_id"
    t.index ["user_modified_id"], name: "report_builder_report_user_modified_id"
  end

  create_table "report_builder_report_starred", id: :serial, force: :cascade do |t|
    t.integer "report_id", null: false
    t.integer "user_id", null: false
    t.index ["report_id", "user_id"], name: "report_builder_report_starred_report_id_key", unique: true
    t.index ["report_id"], name: "report_builder_report_starred_report_id"
    t.index ["user_id"], name: "report_builder_report_starred_user_id"
  end

  create_table "review_open_questions", force: :cascade do |t|
    t.text "text"
    t.integer "teacher_id"
    t.integer "student_id"
    t.integer "open_question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sequence_lms_activities", force: :cascade do |t|
    t.string "percentage"
    t.string "type_activity"
    t.integer "order"
    t.bigint "lms_activity_id"
    t.bigint "sequence_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lms_activity_id"], name: "index_sequence_lms_activities_on_lms_activity_id"
    t.index ["sequence_id"], name: "index_sequence_lms_activities_on_sequence_id"
  end

  create_table "sequence_users", force: :cascade do |t|
    t.integer "auth_user_id"
    t.integer "sequence_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "assigned"
    t.integer "assigned_sequence_id"
    t.integer "score"
    t.date "date"
    t.boolean "successfull"
    t.integer "current_try"
  end

  create_table "sequences", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.integer "level"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "south_migrationhistory", id: :serial, force: :cascade do |t|
    t.string "app_name", limit: 255, null: false
    t.string "migration", limit: 255, null: false
    t.datetime "applied", null: false
  end

  create_table "taggit_tag", id: :serial, force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.string "slug", limit: 100, null: false
    t.index "slug varchar_pattern_ops", name: "taggit_tag_slug_like"
    t.index ["name"], name: "taggit_tag_name_uniq", unique: true
  end

  create_table "taggit_taggeditem", id: :serial, force: :cascade do |t|
    t.integer "tag_id", null: false
    t.integer "object_id", null: false
    t.integer "content_type_id", default: 13, null: false
    t.index ["content_type_id"], name: "taggit_taggeditem_content_type_id"
    t.index ["object_id"], name: "taggit_taggeditem_object_id"
    t.index ["tag_id"], name: "taggit_taggeditem_tag_id"
  end

  create_table "templates", force: :cascade do |t|
    t.string "title"
    t.string "template"
  end

  create_table "users_masives", force: :cascade do |t|
    t.bigint "auth_user_id"
    t.bigint "masive_id"
    t.index ["auth_user_id"], name: "index_users_masives_on_auth_user_id"
    t.index ["masive_id"], name: "index_users_masives_on_masive_id"
  end

  add_foreign_key "Ads_ad_articles", "\"Ads_ad\"", column: "ad_id", name: "ad_id_refs_id_063f1a6d"
  add_foreign_key "Ads_ad_articles", "\"News_article\"", column: "article_id", name: "Ads_ad_articles_article_id_fkey"
  add_foreign_key "Ads_ad_pages", "\"Ads_ad\"", column: "ad_id", name: "ad_id_refs_id_2db047ba"
  add_foreign_key "Ads_ad_pages", "\"CMS_page\"", column: "page_id", name: "Ads_ad_pages_page_id_fkey"
  add_foreign_key "Ads_ad_sections", "\"Ads_ad\"", column: "ad_id", name: "ad_id_refs_id_5907c4c8"
  add_foreign_key "Ads_ad_sections", "\"News_section\"", column: "section_id", name: "Ads_ad_sections_section_id_fkey"
  add_foreign_key "Ads_ad_sites", "\"Ads_ad\"", column: "ad_id", name: "ad_id_refs_id_f04c312a"
  add_foreign_key "Ads_ad_sites", "django_site", column: "site_id", name: "Ads_ad_sites_site_id_fkey"
  add_foreign_key "Ads_ad_tags", "\"Ads_ad\"", column: "ad_id", name: "ad_id_refs_id_83c1d511"
  add_foreign_key "CMS_menu", "\"CMS_menu\"", column: "parent_id", name: "CMS_menu_parent_id_fkey"
  add_foreign_key "CMS_menu", "django_site", column: "site_id", name: "CMS_menu_site_id_fkey"
  add_foreign_key "CMS_module_articles", "\"CMS_module\"", column: "module_id", name: "module_id_refs_id_784cda61"
  add_foreign_key "CMS_module_articles", "\"News_article\"", column: "article_id", name: "article_id_refs_id_2c30c3cd"
  add_foreign_key "CMS_module_pages", "\"CMS_module\"", column: "module_id", name: "module_id_refs_id_cae5ae39"
  add_foreign_key "CMS_module_pages", "\"CMS_page\"", column: "page_id", name: "CMS_module_pages_page_id_fkey"
  add_foreign_key "CMS_module_sections", "\"CMS_module\"", column: "module_id", name: "module_id_refs_id_9535642f"
  add_foreign_key "CMS_module_sections", "\"News_section\"", column: "section_id", name: "section_id_refs_id_e2be3439"
  add_foreign_key "CMS_module_sites", "\"CMS_module\"", column: "module_id", name: "module_id_refs_id_4fb8848b"
  add_foreign_key "CMS_module_sites", "django_site", column: "site_id", name: "site_id_refs_id_a548f286"
  add_foreign_key "CMS_module_tags", "\"CMS_module\"", column: "module_id", name: "module_id_refs_id_a444d41c"
  add_foreign_key "CMS_page_sites", "\"CMS_page\"", column: "page_id", name: "page_id_refs_id_a8901be6"
  add_foreign_key "CMS_page_sites", "django_site", column: "site_id", name: "CMS_page_sites_site_id_fkey"
  add_foreign_key "CMS_siteconfig", "\"LMS_quote\"", column: "quote_id", name: "quote_id_refs_id_f0e437f0"
  add_foreign_key "CMS_siteconfig", "\"LMS_word\"", column: "word_of_the_day_id", name: "word_of_the_day_id_refs_id_da29be0f"
  add_foreign_key "CMS_siteconfig", "\"News_section\"", column: "watch_module_category_id", name: "watch_module_category_id_refs_id_cd02daa7"
  add_foreign_key "CMS_siteconfig", "django_site", column: "site_id", name: "CMS_siteconfig_site_id_fkey"
  add_foreign_key "Caracterization_caracterizationitem", "\"Caracterization_caracterizationcategory\"", column: "category_id", name: "category_id_refs_id_b679219a"
  add_foreign_key "Caracterization_citemclientrelation", "\"Caracterization_caracterizationitem\"", column: "citem_id", name: "citem_id_refs_id_4794c2f8"
  add_foreign_key "Caracterization_citemclientrelation", "auth_users", column: "user_id", name: "user_id_refs_id_2c36e84c"
  add_foreign_key "LMS_activity", "\"LMS_course\"", column: "course_id", name: "course_id_refs_id_531b1d84"
  add_foreign_key "LMS_activity", "\"LMS_level\"", column: "difficulty_id", name: "difficulty_id_refs_id_9eef6f33"
  add_foreign_key "LMS_activity", "\"News_article\"", column: "article_id", name: "article_id_refs_id_79ffcd00"
  add_foreign_key "LMS_activity", "auth_users", column: "created_by_id", name: "created_by_id_refs_id_efed8cf0"
  add_foreign_key "LMS_activity_vocabulary", "\"LMS_activity\"", column: "activity_id", name: "activity_id_refs_id_aa0c58ef"
  add_foreign_key "LMS_activity_vocabulary", "\"LMS_word\"", column: "word_id", name: "word_id_refs_id_6d18699b"
  add_foreign_key "LMS_answer", "\"LMS_question\"", column: "question_id", name: "question_id_refs_id_209a4c3c"
  add_foreign_key "LMS_answer", "\"Media_media\"", column: "Media_media_id"
  add_foreign_key "LMS_definition", "\"LMS_word\"", column: "word_id", name: "word_id_refs_id_d3c61f90"
  add_foreign_key "LMS_question", "\"LMS_activity\"", column: "activity_id", name: "activity_id_refs_id_b44b64de"
  add_foreign_key "LMS_question", "\"LMS_category\"", column: "category_id", name: "category_id_refs_id_516b7a39"
  add_foreign_key "LMS_question", "\"LMS_question\"", column: "parent", name: "id_palabra"
  add_foreign_key "LMS_question", "\"Media_media\"", column: "media_id", name: "media_id_refs_id_04cd8848"
  add_foreign_key "LMS_question", "question_type", column: "type_id", name: "question_type_refs_id"
  add_foreign_key "LMS_slide", "\"Media_media\"", column: "image_id", name: "image_id_refs_id_9aeaf33d"
  add_foreign_key "Media_gallery", "django_content_type", column: "content_type_id", name: "Media_gallery_content_type_id_fkey"
  add_foreign_key "Media_gallery_media", "\"Media_gallery\"", column: "gallery_id", name: "gallery_id_refs_id_115a126a"
  add_foreign_key "Media_gallery_media", "\"Media_media\"", column: "media_id", name: "Media_gallery_media_media_id_fkey"
  add_foreign_key "News_article_section", "\"News_article\"", column: "article_id", name: "article_id_refs_id_4b6c3bb7"
  add_foreign_key "News_article_section", "\"News_section\"", column: "section_id", name: "News_article_section_section_id_fkey"
  add_foreign_key "News_section", "\"News_section\"", column: "parent_id", name: "News_section_parent_id_fkey"
  add_foreign_key "News_section_sites", "\"News_section\"", column: "section_id", name: "section_id_refs_id_5c772ae2"
  add_foreign_key "News_section_sites", "django_site", column: "site_id", name: "News_section_sites_site_id_fkey"
  add_foreign_key "News_sectionsidebarcontent", "\"News_section\"", column: "section_to_show_id", name: "section_to_show_id_refs_id_5d0309f5"
  add_foreign_key "News_sectionsidebarcontent_sections", "\"News_section\"", column: "section_id", name: "section_id_refs_id_ac445159"
  add_foreign_key "News_sectionsidebarcontent_sections", "\"News_sectionsidebarcontent\"", column: "sectionsidebarcontent_id", name: "sectionsidebarcontent_id_refs_id_349472ac"
  add_foreign_key "PaymentPasarel_coupon_categories", "\"PaymentPasarel_coupon\"", column: "coupon_id", name: "coupon_id_refs_id_f297b4e3"
  add_foreign_key "PaymentPasarel_coupon_categories", "\"PaymentPasarel_subscriptioncategory\"", column: "subscriptioncategory_id", name: "subscriptioncategory_id_refs_id_6874bc6e"
  add_foreign_key "PaymentPasarel_subscriptioninformation", "auth_users", column: "user_id", name: "PaymentPasarel_subscriptioninformation_user_id_fkey"
  add_foreign_key "activity_institutions", "\"LMS_activity\"", column: "activity_id", name: "activity_id_foreign_key"
  add_foreign_key "activity_institutions", "latamusers_intitutions", column: "institution_id", name: "institution_id_foreign_key"
  add_foreign_key "admin_user_groups", "auth_users", column: "auth_users_id"
  add_foreign_key "auth_group_permissions", "auth_group", column: "group_id", name: "group_id_refs_id_f4b32aac"
  add_foreign_key "auth_group_permissions", "auth_permission", column: "permission_id", name: "auth_group_permissions_permission_id_fkey"
  add_foreign_key "auth_permission", "django_content_type", column: "content_type_id", name: "content_type_id_refs_id_d043b34a"
  add_foreign_key "auth_user_groups", "auth_group", column: "group_id", name: "auth_user_groups_group_id_fkey"
  add_foreign_key "auth_user_groups", "auth_users", column: "user_id", name: "user_id_refs_id_40c41112"
  add_foreign_key "auth_user_user_permissions", "auth_permission", column: "permission_id", name: "auth_user_user_permissions_permission_id_fkey"
  add_foreign_key "auth_user_user_permissions", "auth_users", column: "user_id", name: "user_id_refs_id_4dc23c39"
  add_foreign_key "category_institution", "\"News_section\"", column: "section_id", name: "section_id_foreing_key"
  add_foreign_key "category_institution", "latamusers_intitutions", column: "institution_id", name: "institution_id_foreign_key"
  add_foreign_key "django_admin_log", "auth_users", column: "user_id", name: "user_id_refs_id_c0d12874"
  add_foreign_key "django_admin_log", "django_content_type", column: "content_type_id", name: "content_type_id_refs_id_93d2d1f8"
  add_foreign_key "easy_thumbnails_thumbnail", "easy_thumbnails_source", column: "source_id", name: "source_id_refs_id_0df57a91"
  add_foreign_key "easy_thumbnails_thumbnaildimensions", "easy_thumbnails_thumbnail", column: "thumbnail_id", name: "thumbnail_id_refs_id_ef901436"
  add_foreign_key "groups_masives", "latamusers_usergroups", primary_key: "code"
  add_foreign_key "groups_masives", "masives", column: "masive_id"
  add_foreign_key "instant_phrases_parte", "instant_phrases_frase", column: "frase_id", name: "identificador frase"
  add_foreign_key "instant_phrases_parte", "instant_phrases_palabra", column: "palabra_id", name: "identificador_palabra_correspondiente"
  add_foreign_key "institutions_masives", "latamusers_intitutions"
  add_foreign_key "institutions_masives", "masives", column: "masive_id"
  add_foreign_key "latamusers_assignedactivity", "\"LMS_activity\"", column: "activity_id", name: "activity_id_refs_id_6e98f351"
  add_foreign_key "latamusers_assignedactivity", "auth_users", column: "teacher_id", name: "teacher_id_refs_id_9483ba22"
  add_foreign_key "latamusers_student", "auth_users", column: "teacher_id", name: "teacher_id_refs_id_a0f45e2e"
  add_foreign_key "latamusers_student", "auth_users", column: "user_id", name: "user_id_refs_id_a0f45e2e"
  add_foreign_key "latamusers_student", "latamusers_intitutions", column: "institution_id", name: "institution_id_refs_id_324c6abf"
  add_foreign_key "latamusers_student", "latamusers_usergroups", column: "group_id", primary_key: "code", name: "group_id_refs_code_d6e0c18f"
  add_foreign_key "latamusers_useractivity", "\"LMS_activity\"", column: "activity_id", name: "activity_id_refs_id_500cc2bd"
  add_foreign_key "latamusers_useractivity", "auth_users", column: "user_id", name: "user_id_refs_id_04ac7839"
  add_foreign_key "latamusers_useractivity", "latamusers_assignedactivity", column: "assigned_activity_id", name: "assigned_activity_id_refs_id_2f637da6"
  add_foreign_key "latamusers_usergroups", "auth_users", column: "teacher_id", name: "teacher_id_refs_id_bba655fe"
  add_foreign_key "latamusers_userprofile", "auth_users", column: "user_id", name: "user_id_refs_id_97299955"
  add_foreign_key "latamusers_userquestion", "\"LMS_category\"", column: "category_id", name: "category_id_refs_id_37f7b799"
  add_foreign_key "latamusers_userquestion", "\"LMS_question\"", column: "question_id", name: "question_id_refs_id_cf9865c5"
  add_foreign_key "latamusers_userquestion", "auth_users", column: "user_id", name: "user_id_refs_id_2900df3b"
  add_foreign_key "masives", "templates"
  add_foreign_key "nested_admin_item", "nested_admin_section", column: "section_id", name: "nested_admin_item_section_id_fkey"
  add_foreign_key "nested_admin_section", "nested_admin_group", column: "group_id", name: "nested_admin_section_group_id_fkey"
  add_foreign_key "open_questions", "\"LMS_question\"", column: "lms_question_id"
  add_foreign_key "open_questions", "auth_users"
  add_foreign_key "registration_registrationprofile", "auth_users", column: "user_id", name: "registration_registrationprofile_user_id_fkey"
  add_foreign_key "report_builder_displayfield", "report_builder_format", column: "display_format_id", name: "report_builder_displayfield_display_format_id_fkey"
  add_foreign_key "report_builder_displayfield", "report_builder_report", column: "report_id", name: "report_builder_displayfield_report_id_fkey"
  add_foreign_key "report_builder_filterfield", "report_builder_report", column: "report_id", name: "report_builder_filterfield_report_id_fkey"
  add_foreign_key "report_builder_report", "auth_users", column: "user_created_id", name: "report_builder_report_user_created_id_fkey"
  add_foreign_key "report_builder_report", "auth_users", column: "user_modified_id", name: "report_builder_report_user_modified_id_fkey"
  add_foreign_key "report_builder_report", "django_content_type", column: "root_model_id", name: "report_builder_report_root_model_id_fkey"
  add_foreign_key "report_builder_report_starred", "auth_users", column: "user_id", name: "report_builder_report_starred_user_id_fkey"
  add_foreign_key "report_builder_report_starred", "report_builder_report", column: "report_id", name: "report_id_refs_id_205f4f68"
  add_foreign_key "sequence_lms_activities", "\"LMS_activity\"", column: "lms_activity_id"
  add_foreign_key "sequence_lms_activities", "sequences"
  add_foreign_key "taggit_taggeditem", "django_content_type", column: "content_type_id", name: "content_type_id_refs_id_01d42cdf"
  add_foreign_key "taggit_taggeditem", "taggit_tag", column: "tag_id", name: "tag_id_refs_id_c23fda9d"
  add_foreign_key "users_masives", "auth_users"
  add_foreign_key "users_masives", "masives", column: "masive_id"
end
