class CreateSequenceLmsActivity < ActiveRecord::Migration[5.1]
  def change
    create_table :sequence_lms_activities do |t|
    	t.string :percentage
    	t.string :type
    	t.integer :order
      t.references :lms_activity, foreign_key: { to_table: 'LMS_activity' }, index: true
      t.references :sequence, foreign_key: { to_table: :sequences }, index: true
    	t.timestamps
    end
  end
end
