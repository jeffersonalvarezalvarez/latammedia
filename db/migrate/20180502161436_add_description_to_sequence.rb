class AddDescriptionToSequence < ActiveRecord::Migration[5.1]
  def change
  	add_column :sequences, :description, :text
  end
end
