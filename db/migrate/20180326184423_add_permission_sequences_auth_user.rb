class AddPermissionSequencesAuthUser < ActiveRecord::Migration[5.1]
  def change

  	DjangoContentType.create(:name => "sequence_user", 
  		:app_label => "sequence_user", 
  		:model => "sequenceuser")

	AuthPermission.create(:name => "Can add sequence user",
		:content_type_id => 61,
		:codename => "add_sequence_user")

	AuthPermission.create(:name => "Can change sequence user",
		:content_type_id => 61,
		:codename => "change_sequence_user")

	AuthPermission.create(:name => "Can delete sequence user",
		:content_type_id => 61,
		:codename => "delete_sequence_user")
	
  end
end
