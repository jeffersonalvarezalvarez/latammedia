class ChangeNameColumnType < ActiveRecord::Migration[5.1]
  def change
  	rename_column :sequence_lms_activities, :type, :type_activity
  end
end
