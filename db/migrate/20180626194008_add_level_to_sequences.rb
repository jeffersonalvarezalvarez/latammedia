class AddLevelToSequences < ActiveRecord::Migration[5.1]
  def change
    add_column :sequences, :level, :integer
  end
end
