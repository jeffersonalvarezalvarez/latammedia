class AddPermissionsToOpenQuestions < ActiveRecord::Migration[5.1]
  def change

  	DjangoContentType.create(:name => "open_question", 
  		:app_label => "open_questions", 
  		:model => "openquestion")

	AuthPermission.create(:name => "Can add open question",
		:content_type_id => 60,
		:codename => "add_open_question")

	AuthPermission.create(:name => "Can change open question",
		:content_type_id => 60,
		:codename => "change_open_question")

	AuthPermission.create(:name => "Can delete open question",
		:content_type_id => 60,
		:codename => "delete_open_question")
	
  end
end
