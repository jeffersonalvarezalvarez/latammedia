class AddDefinitionToLmsQuestion < ActiveRecord::Migration[5.1]
  def change
    add_column :LMS_question, :definition, :text
  end
end
