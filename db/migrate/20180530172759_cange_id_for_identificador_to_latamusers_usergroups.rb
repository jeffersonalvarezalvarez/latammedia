class CangeIdForIdentificadorToLatamusersUsergroups < ActiveRecord::Migration[5.1]
  def change
  	rename_column :latamusers_usergroups, :id, :identificador
  	execute "ALTER SEQUENCE latamusers_usergroups_id_seq OWNED BY latamusers_usergroups.identificador;"
    
  end
end
