class AddMediaIdToLmsAnswer < ActiveRecord::Migration[5.1]
  def change
  	add_reference :LMS_answer, :Media_media, foreign_key: true
  end
end
