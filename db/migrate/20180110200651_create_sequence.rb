class CreateSequence < ActiveRecord::Migration[5.1]
  def change
    create_table :sequences do |t|
    	t.string :name
    	t.timestamps
    end
  end
end
