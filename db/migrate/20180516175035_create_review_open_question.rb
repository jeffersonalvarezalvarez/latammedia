class CreateReviewOpenQuestion < ActiveRecord::Migration[5.1]
  def change
    create_table :review_open_questions do |t|
      t.text :text
      t.integer :teacher_id
      t.integer :student_id
      t.integer :open_question_id

      t.timestamps
    end
  end
end
