class AddContentCorrection < ActiveRecord::Migration[5.1]
  def change
  	add_column :open_questions, :content_errors, :text 
  end
end
