class CreateOpenQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :open_questions do |t|
      t.text :content
      t.references :auth_user, foreign_key: { to_table: 'auth_users' }, index: true
      t.references :lms_question, foreign_key: { to_table: 'LMS_question' }, index: true

      t.timestamps
    end
  end
end
