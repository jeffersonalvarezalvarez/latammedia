class CreateSequenceUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :sequence_users do |t|
    	t.integer :auth_user_id
    	t.integer :sequence_id
    	
      	t.timestamps
    end
  end
end
