class AddForeignKeyToGroups < ActiveRecord::Migration[5.1]
  def change
  	add_reference :latamusers_usergroups, :sequence, foreign_key: true
  end
end
