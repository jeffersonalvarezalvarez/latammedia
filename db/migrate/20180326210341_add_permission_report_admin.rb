class AddPermissionReportAdmin < ActiveRecord::Migration[5.1]
  def change
    DjangoContentType.create(:name => "reports_and_statistics", 
      :app_label => "reports_and_statistics", 
      :model => "reports_statistics")

    AuthPermission.create(:name => "Can add generate report",
      :content_type_id => 62,
      :codename => "generate_report")
  end
end