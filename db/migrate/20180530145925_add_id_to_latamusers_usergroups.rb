class AddIdToLatamusersUsergroups < ActiveRecord::Migration[5.1]
  def self.up
    add_column :latamusers_usergroups, :id, :integer

    i = 0
    LatamusersUsergroup.all.each do |ug|
    	i = i + 1
    	ug.update_column(:id, i)
  	end
  	
    execute "CREATE SEQUENCE latamusers_usergroups_id_seq OWNED BY latamusers_usergroups.id INCREMENT BY 1 START WITH 115;"
    execute "ALTER TABLE latamusers_usergroups ALTER COLUMN id SET NOT NULL;
    ALTER TABLE latamusers_usergroups ALTER COLUMN id SET DEFAULT nextval('latamusers_usergroups_id_seq'::regclass);"
    add_index :latamusers_usergroups, :id, unique: true

  end

  def self.down
  	remove_column :latamusers_usergroups, :id
  end
end
