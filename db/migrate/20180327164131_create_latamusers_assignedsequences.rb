class CreateLatamusersAssignedsequences < ActiveRecord::Migration[5.1]
  def change
    create_table :latamusers_assignedsequences do |t|
		t.integer :sequence_id
		t.integer :maximum_tries
		t.date :start_date
		t.date :end_date
		t.integer :teacher_id
		t.date :creation_date
	 	t.boolean :active
      	t.timestamps
    end

    add_column :sequence_users, :assigned, :boolean
    add_column :sequence_users, :assigned_sequence_id, :integer
    add_column :sequence_users, :score, :integer
    add_column :sequence_users, :date, :date
    add_column :sequence_users, :successfull, :boolean
    add_column :sequence_users, :current_try, :integer
    
  end
end
