class AddDefaultToMedia < ActiveRecord::Migration[5.1]
  def change
  	change_column :Media_gallery, :style, :string, :default => ""
  	change_column :Media_gallery, :position, :string, :default => ""
  end
end
