class RemoveUniqueConstraintInSlug < ActiveRecord::Migration[5.1]
  def change
  	execute "ALTER TABLE taggit_tag DROP CONSTRAINT taggit_tag_slug_key;"
  end
end
